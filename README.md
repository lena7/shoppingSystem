# 仿天猫购物商城+后台+沙箱支付

使用技术：SSM+分模块开发+elementUI+vue+mysql+zookeeper
支付功能使用了支付宝的沙箱支付
数据库sql文件在shoppingSystem_common模块里。

## 项目启动
1. 执行sql文件
2. 修改shoppingSystem_service_shop模块下resources>db.properties的数据库配置信息以及dubbo.properties端口号的设置（本项目采用的是本机地址）
3. 启动zkServer.cmd（本项目使用了zookeeper）
4. 启动shoppingSystem_service_shop的tomcat
5. 启动shoppingSystem_web的tomcat
6. 即可访问
