/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.40 : Database - shopping_system
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shopping_system` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shopping_system`;

/*Table structure for table `bank_account` */

DROP TABLE IF EXISTS `bank_account`;

CREATE TABLE `bank_account` (
  `id` varchar(100) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户id',
  `bank_account` varchar(19) DEFAULT NULL COMMENT '银行卡号',
  `belong_bank` varchar(100) DEFAULT NULL COMMENT '所属银行',
  `type` int(1) DEFAULT NULL COMMENT '卡的类型（储蓄卡1 信用卡0）',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `bank_account_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `bank_account` */

insert  into `bank_account`(`id`,`user_id`,`bank_account`,`belong_bank`,`type`) values ('7001','1001','6532555555555555','中国建设银行',1),('7002','1001','6532555555555555','中国人民银行',1),('7003','1001','6532555555555555','中国工商银行',1),('7004','1002','6532555555555555','中国农业银行',1),('7005','1002','6532555555555555','中国建设银行',2);

/*Table structure for table `business` */

DROP TABLE IF EXISTS `business`;

CREATE TABLE `business` (
  `id` varchar(100) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `sex` char(2) DEFAULT NULL COMMENT '性别',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `real_name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `brief` varchar(100) DEFAULT NULL COMMENT '个签',
  `regist_date` datetime NOT NULL COMMENT '注册时间',
  `face_img` varchar(100) DEFAULT NULL COMMENT '头像',
  `alipay_account` varchar(11) DEFAULT NULL COMMENT '支付宝账号',
  `phone_number` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  `bond` int(11) DEFAULT NULL COMMENT '缴纳保证金',
  `credit_rating` int(2) DEFAULT NULL COMMENT '信誉等级（5心->5皇冠）',
  `living` varchar(200) DEFAULT NULL COMMENT '现居住地',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `business` */

insert  into `business`(`id`,`name`,`sex`,`email`,`real_name`,`id_card`,`brief`,`regist_date`,`face_img`,`alipay_account`,`phone_number`,`age`,`bond`,`credit_rating`,`living`) values ('2001','jazh','男','123456789@qq.com','嘉志','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 21:18:55',NULL,'13111111111','111111',31,1000000,3,'广东省广州市滨江街道仲恺路500号'),('2002','商家2','女','123456789@qq.com','商家2','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 21:18:58',NULL,'13111111111','13111111111',32,1000000,3,'广东省广州市滨江街道仲恺路500号'),('2003','商家3','男','123456789@qq.com','商家3','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 21:19:00',NULL,'13111111111','13111111111',33,1000000,3,'广东省广州市滨江街道仲恺路500号'),('2004','商家4','女','123456789@qq.com','商家4','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 21:19:01',NULL,'13111111111','13111111111',34,1000000,3,'广东省广州市滨江街道仲恺路500号'),('2005','商家5','男','123456789@qq.com','商家5','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 21:19:10',NULL,'13111111111','13111111111',35,1000000,3,'广东省广州市滨江街道仲恺路500号');

/*Table structure for table `business_bill` */

DROP TABLE IF EXISTS `business_bill`;

CREATE TABLE `business_bill` (
  `id` varchar(100) NOT NULL COMMENT '账单id',
  `business_id` varchar(100) DEFAULT '2001' COMMENT '商家id',
  `user_id` varchar(100) DEFAULT '1001' COMMENT '用户id',
  `order_id` varchar(100) DEFAULT NULL COMMENT '订单id',
  `receive_amount` decimal(8,2) DEFAULT NULL COMMENT '收入金额',
  `receive_mode` varchar(50) DEFAULT '支付宝' COMMENT '收入方式',
  `status` int(1) DEFAULT '1' COMMENT '交易状态（1成功2退款）',
  `remarks` varchar(100) DEFAULT NULL COMMENT '备注信息',
  `time` datetime DEFAULT NULL COMMENT '交易时间',
  PRIMARY KEY (`id`),
  KEY `business_id` (`business_id`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `business_bill_ibfk_1` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`),
  CONSTRAINT `business_bill_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `business_bill_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `business_bill` */

insert  into `business_bill`(`id`,`business_id`,`user_id`,`order_id`,`receive_amount`,`receive_mode`,`status`,`remarks`,`time`) values ('7001','2001','1001','8001','287.00','支付宝',1,NULL,'2020-12-02 22:02:40'),('7002','2001','1001','8002','246.00','支付宝',1,NULL,'2020-12-03 22:02:50'),('7003','2001','1001','8003','546.00','支付宝',1,NULL,'2020-12-04 22:02:54'),('7004','2001','1001','8004','276.00','支付宝',1,NULL,'2020-12-05 22:02:57'),('7005','2001','1001','8005','127.00','支付宝',1,NULL,'2020-12-06 22:03:00'),('7006','2001','1001','8010','424.00','支付宝',1,NULL,'2020-12-07 22:03:08'),('7007','2001','1001','8011','142.00','支付宝',1,NULL,'2020-12-08 22:03:08'),('7008','2001','1001','8012','199.00','支付宝',1,NULL,'2020-12-09 22:03:08'),('7009','2001','1001','8013','427.00','支付宝',1,NULL,'2020-12-10 22:03:08'),('7010','2001','1001','8014','199.00','支付宝',1,NULL,'2020-12-11 22:03:15'),('7011','2001','1001','8015','212.00','支付宝',1,NULL,'2020-12-12 22:03:15'),('7012','2001','1001','8016','787.00','支付宝',1,NULL,'2020-12-13 22:03:15'),('7013','2001','1001','8017','444.00','支付宝',1,NULL,'2020-12-14 22:03:15'),('7014','2001','1001','8018','28.00','支付宝',1,NULL,'2020-12-15 22:03:27'),('7015','2001','1001','8019','282.00','支付宝',1,NULL,'2020-12-17 22:03:27'),('7016','2001','1001','8020','222.00','支付宝',1,NULL,'2020-12-18 22:03:27'),('7017','2001','1001','8021','285.00','支付宝',1,NULL,'2020-12-19 22:03:27');

/*Table structure for table `catagory` */

DROP TABLE IF EXISTS `catagory`;

CREATE TABLE `catagory` (
  `id` varchar(100) NOT NULL COMMENT '类别ID',
  `name` varchar(100) DEFAULT NULL COMMENT '类别名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `catagory` */

insert  into `catagory`(`id`,`name`) values ('1','女运动鞋'),('10','帽子'),('11','礼品'),('12','生活用品'),('13','女外套'),('14','女羊毛'),('15','女衬衫'),('16','女平底鞋'),('2','女上衣'),('3','女长裤'),('4','女长裙'),('5','男上衣'),('6','男长裤'),('7','男卫衣'),('8','男外套'),('9','高跟鞋');

/*Table structure for table `catagory_show` */

DROP TABLE IF EXISTS `catagory_show`;

CREATE TABLE `catagory_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一索引id',
  `name` varchar(255) NOT NULL COMMENT '分类的名字',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `catagory_show` */

insert  into `catagory_show`(`id`,`name`) values (1,'女装 /内衣'),(2,'男装 /运动户外'),(3,'女鞋 /男鞋 /箱包'),(4,'美妆 /个人护理'),(5,'腕表 /眼镜 /珠宝饰品'),(6,'手机 /数码 /电脑办公'),(7,'母婴玩具'),(8,'零食 /茶酒 /进口食品'),(9,'生鲜水果'),(10,'大家电 /生活电器'),(11,'家具建材'),(12,'汽车 /配件 /用品'),(13,'家纺 /家饰 /鲜花'),(14,'医药保健'),(15,'厨具 /收纳 /宠物'),(16,'图书音像');

/*Table structure for table `evaluate` */

DROP TABLE IF EXISTS `evaluate`;

CREATE TABLE `evaluate` (
  `id` varchar(100) NOT NULL COMMENT '评价ID',
  `order_id` varchar(100) DEFAULT NULL COMMENT '订单ID',
  `goods_id` varchar(100) DEFAULT NULL COMMENT '商品ID',
  `content` varchar(255) DEFAULT NULL COMMENT '评价内容',
  `is_anonymouns` int(1) DEFAULT NULL COMMENT '是否匿名',
  `grade` int(1) DEFAULT NULL COMMENT '评价等级（好评1 中评2 差评3）',
  `describe_fit` int(1) DEFAULT NULL COMMENT '描述相符（1-5）',
  `logistics_service` int(1) DEFAULT NULL COMMENT '物流服务（1-5）',
  `service_attitude` int(1) DEFAULT NULL COMMENT '服务态度（1-5）',
  `is_img` int(1) DEFAULT '0' COMMENT '是否有图片（0无 1有）',
  `date` datetime DEFAULT NULL COMMENT '评价日期',
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `goods_id` (`goods_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `evaluate_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`),
  CONSTRAINT `evaluate_ibfk_2` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`),
  CONSTRAINT `evaluate_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `evaluate` */

insert  into `evaluate`(`id`,`order_id`,`goods_id`,`content`,`is_anonymouns`,`grade`,`describe_fit`,`logistics_service`,`service_attitude`,`is_img`,`date`,`user_id`) values ('1',NULL,'2J8100010','1',1,1,1,1,1,0,'2020-12-18 20:14:18',NULL);

/*Table structure for table `evaluate_img` */

DROP TABLE IF EXISTS `evaluate_img`;

CREATE TABLE `evaluate_img` (
  `id` varchar(100) NOT NULL COMMENT '评价图片id',
  `evaluate_id` varchar(100) DEFAULT NULL COMMENT '评价ID',
  `img1` varchar(200) DEFAULT NULL COMMENT '图片1',
  `img2` varchar(200) DEFAULT NULL COMMENT '图片2',
  `img3` varchar(200) DEFAULT NULL COMMENT '图片3',
  `img4` varchar(200) DEFAULT NULL COMMENT '图片4',
  `img5` varchar(200) DEFAULT NULL COMMENT '图片5',
  `number` int(1) DEFAULT NULL COMMENT '图片总数',
  PRIMARY KEY (`id`),
  KEY `evaluate_id` (`evaluate_id`),
  CONSTRAINT `evaluate_img_ibfk_1` FOREIGN KEY (`evaluate_id`) REFERENCES `evaluate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `evaluate_img` */

/*Table structure for table `goods` */

DROP TABLE IF EXISTS `goods`;

CREATE TABLE `goods` (
  `id` varchar(100) NOT NULL COMMENT '商品id',
  `main_img_id` varchar(100) DEFAULT NULL COMMENT '商品主图id',
  `details_id` varchar(100) DEFAULT NULL COMMENT '商品详情id',
  `details_img_id` varchar(100) DEFAULT NULL COMMENT '商品详情页图片id',
  `catagory_id` varchar(100) DEFAULT NULL COMMENT '商品所属类别id',
  `component_id` varchar(100) DEFAULT NULL COMMENT '商品成分id',
  `type_select_id` varchar(100) DEFAULT NULL COMMENT '商品类型选择id',
  `shop_id` varchar(100) DEFAULT NULL COMMENT '所属店铺id',
  PRIMARY KEY (`id`),
  KEY `main_img_id` (`main_img_id`),
  KEY `details_id` (`details_id`),
  KEY `details_img_id` (`details_img_id`),
  KEY `component_id` (`component_id`),
  KEY `type_select_id` (`type_select_id`),
  KEY `shop_id` (`shop_id`),
  KEY `goods_ibfk_4` (`catagory_id`),
  CONSTRAINT `goods_ibfk_1` FOREIGN KEY (`main_img_id`) REFERENCES `goods_main_img` (`id`),
  CONSTRAINT `goods_ibfk_2` FOREIGN KEY (`details_id`) REFERENCES `goods_details` (`id`),
  CONSTRAINT `goods_ibfk_3` FOREIGN KEY (`details_img_id`) REFERENCES `goods_details_img` (`id`),
  CONSTRAINT `goods_ibfk_5` FOREIGN KEY (`component_id`) REFERENCES `goods_component` (`id`),
  CONSTRAINT `goods_ibfk_7` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods` */

insert  into `goods`(`id`,`main_img_id`,`details_id`,`details_img_id`,`catagory_id`,`component_id`,`type_select_id`,`shop_id`) values ('2J8100010','2J8100010','2J8100010','2J8100010','1','2J8100010','5005','3003'),('368024','368024','368024','368024','3','368024','5001','3001'),('7B8160231-1','7B8160231-1','7B8160231-1','7B8160231-1','1','7B8160231-1','5003','3002'),('AV9370','AV9370','AV9370','AV9370','3','AV9370','5007','3004'),('CGU2809TA','CGU2809TA','CGU2809TA','CGU2809TA','1','CGU2809TA','5004','3003'),('test1','test1','test1','test1','3','test1','5001','3006'),('test2','test2','test2','test2','3','test2','5001','3006'),('test3','test3','test3','test3','3','test3','5001','3006'),('test4','test4','test4','test4','3','test4','5001','3006'),('test5','test5','test5','test5','3','test5','5001','3006'),('test6','test6','test6','test6','3','test6','5001','3006'),('test7','test7','test7','test7','3','test7','5001','3006'),('X-19D0087','X-19D0087','X-19D0087','X-19D0087','1','X-19D0087','5002','3002'),('YLMH-845236','YLMH-845236','YLMH-845236','YLMH-845236','3','YLMH-845236','5006','3001');

/*Table structure for table `goods_color` */

DROP TABLE IF EXISTS `goods_color`;

CREATE TABLE `goods_color` (
  `id` varchar(100) NOT NULL COMMENT '颜色分类ID',
  `name` varchar(20) DEFAULT NULL COMMENT '分类名字',
  `img` varchar(200) DEFAULT NULL COMMENT '分类图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods_color` */

insert  into `goods_color`(`id`,`name`,`img`) values ('1','蓝色',NULL),('10','米色',NULL),('1001','白/白/白',NULL),('1002','白/深藏青/深藏青/白色',NULL),('1003','山峰白/石英粉-亮花粉黄',NULL),('1004','白色/帆布/白色/白',NULL),('11','黄色',NULL),('2','藏蓝色',NULL),('3','红色',NULL),('4','玫红色',NULL),('5','灰色',NULL),('6','淡紫色',NULL),('7','黑色',NULL),('8','白色',NULL),('9','粉红色',NULL);

/*Table structure for table `goods_component` */

DROP TABLE IF EXISTS `goods_component`;

CREATE TABLE `goods_component` (
  `id` varchar(100) NOT NULL COMMENT '商品成分id',
  `brand` varchar(50) DEFAULT NULL COMMENT '品牌',
  `fit_age` varchar(40) DEFAULT NULL COMMENT '适用年龄',
  `item_no` int(11) DEFAULT NULL COMMENT '货号',
  `sent_season` varchar(50) DEFAULT NULL COMMENT '上市年份季节',
  `sales_channel_type` varchar(50) DEFAULT NULL COMMENT '销售渠道类型',
  `material_composition` varchar(20) DEFAULT NULL COMMENT '材质成分',
  `style` varchar(20) DEFAULT NULL COMMENT '基础风格',
  `pattern` varchar(20) DEFAULT NULL COMMENT '图案',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods_component` */

insert  into `goods_component`(`id`,`brand`,`fit_age`,`item_no`,`sent_season`,`sales_channel_type`,`material_composition`,`style`,`pattern`) values ('2J8100010','品牌f','所有女性',100010,'2020年夏季','仅线上销售','衬衫','时尚','纯色'),('368024','品牌a','所有人群',36802402,'2020年秋冬','仅淘宝线上销售','橡胶','运动休闲','无'),('7B8160231-1','品牌c','年轻女性',81602311,'2020年秋季','线上线下销售','毛绒','时尚','无'),('AV9370','nike','女子',9370,'2020年春季','线上线下销售','低帮','运动','无'),('CGU2809TA','品牌e','所有女性',2809,'2020年冬季','仅线上销售','羊毛','保暖','无'),('test1','安踏','所有女性',123456,'2020年春秋','线上线下销售','网面','运动休闲','白色'),('test2','安踏','所有女性',123456,'2020年春秋','线上线下销售','网面','运动休闲','白色'),('test3','安踏','所有女性',123456,'2020年春秋','线上线下销售','网面','运动休闲','白色'),('test4','安踏','所有女性',123456,'2020年春秋','线上线下销售','网面','运动休闲','白色'),('test5','安踏','所有女性',123456,'2020年春秋','线上线下销售','网面','运动休闲','白色'),('test6','安踏','所有女性',123456,'2020年春秋','线上线下销售','网面','运动休闲','白色'),('test7','安踏','所有女性',123456,'2020年春秋','线上线下销售','网面','运动休闲','白色'),('X-19D0087','品牌b','中老年人',190087,'2020年冬季','线上线下销售','绒毛','保暖','无'),('YLMH-845236','品牌a','所有女性',845236,'2020年春秋','线上线下销售','羊皮','欧美','纯色');

/*Table structure for table `goods_details` */

DROP TABLE IF EXISTS `goods_details`;

CREATE TABLE `goods_details` (
  `id` varchar(100) NOT NULL COMMENT '商品详情页id',
  `describegoods` varchar(200) DEFAULT NULL COMMENT '商品描述',
  `title` varchar(200) DEFAULT NULL COMMENT '商品标题',
  `recommend_number` int(11) DEFAULT NULL COMMENT '推荐人数',
  `sales_month` int(11) DEFAULT NULL COMMENT '月销量',
  `low_price` decimal(8,2) DEFAULT NULL COMMENT '最低价格',
  `high_price` decimal(8,2) DEFAULT NULL COMMENT '最高价格',
  `delivery_place` varchar(100) DEFAULT NULL COMMENT '发货地',
  `express` int(1) DEFAULT '0' COMMENT '快递（0为免运费）',
  `guarantee` varchar(100) DEFAULT NULL COMMENT '保障',
  `title_show` varchar(50) DEFAULT NULL COMMENT '小标题（显示标题）',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods_details` */

insert  into `goods_details`(`id`,`describegoods`,`title`,`recommend_number`,`sales_month`,`low_price`,`high_price`,`delivery_place`,`express`,`guarantee`,`title_show`,`stock`) values ('2J8100010',NULL,'less衬衣秋冬折扣新品时尚渐变下摆开衩设计波点衬衫女2J8100010',50,100,'1990.00','1990.00','广西桂林',0,'正品保证,极速退款','新品时尚渐变下摆开衩设计波点衬衫女',99),('368024','368024','Puma彪马泫雅网红同款粉绿色全麂皮黑白运动紫亮面老爹鞋女368024',22,30,'529.00','799.00','广东广州',0,'假一赔十,7天无理由退货','Puma彪马泫雅网红同款粉绿色',9999),('7B8160231-1',NULL,'李沁同款诗凡黎套装女2020新款秋冬中长款工装风韩版英伦风风衣女',84,207,'458.00','1180.00','广东广州',0,'正品保证,极速退款','李沁同款诗凡黎套装女2020新款',999),('AV9370',NULL,'Nike官方耐克BLAZER LOW LE 女子运动鞋休闲板鞋经典小白鞋AV9370',1824,4400,'299.00','599.00','上海',0,'正品保证,极速退款,七天无理由退换','Nike官方耐克BLAZER LOW LE 女子运动鞋',16807),('CGU2809TA',NULL,'羊毛针织套装2020年秋冬新款女休闲显瘦减龄时尚气质百褶裙两件套',20,77,'823.00','823.00','广东深圳',0,'7天无理由退货','2020年秋冬新款女休闲显瘦气质百褶裙两件套',100),('test1',NULL,'安踏官网旗舰女鞋跑步鞋2020年秋冬季新款全掌气垫鞋子女士运动鞋',299,300,'309.00','469.00','广东广州',0,'增保价险,正品保证','2020年秋冬季新款全掌气垫鞋子女士运动鞋',99),('test2',NULL,'安踏官网旗舰女鞋跑步鞋2020年秋冬季新款全掌气垫鞋子女士运动鞋',199,250,'309.00','469.00','广东广州',0,'增保价险,正品保证','2020年秋冬季新款全掌气垫鞋子女士运动鞋',99),('test3',NULL,'安踏官网旗舰女鞋跑步鞋2020年秋冬季新款全掌气垫鞋子女士运动鞋',256,290,'309.00','469.00','广东广州',0,'假一赔十,7天无理由退货','2020年秋冬季新款全掌气垫鞋子女士运动鞋',99),('test4',NULL,'安踏官网旗舰女鞋跑步鞋2020年秋冬季新款全掌气垫鞋子女士运动鞋',2145,2500,'309.00','469.00','广东广州',0,'7天无理由退货','2020年秋冬季新款全掌气垫鞋子女士运动鞋',99),('test5',NULL,'安踏官网旗舰女鞋跑步鞋2020年秋冬季新款全掌气垫鞋子女士运动鞋',21,31,'309.00','469.00','广东广州',0,'假一赔十,7天无理由退货','2020年秋冬季新款全掌气垫鞋子女士运动鞋',99),('test6',NULL,'安踏官网旗舰女鞋跑步鞋2020年秋冬季新款全掌气垫鞋子女士运动鞋',1482,1600,'309.00','469.00','广东广州',0,'7天无理由退货','2020年秋冬季新款全掌气垫鞋子女士运动鞋',99),('test7',NULL,'安踏官网旗舰女鞋跑步鞋2020年秋冬季新款全掌气垫鞋子女士运动鞋',21,25,'309.00','469.00','广东广州',0,'正品保证,极速退款','2020年秋冬季新款全掌气垫鞋子女士运动鞋',99),('X-19D0087',NULL,'妈妈秋冬装仿水貂绒大衣洋气50岁中老年女装秋装加厚时尚毛呢外套',36,50,'920.00','920.00','广东揭阳',0,'增保价险,正品保证','仿水貂绒大衣洋气50岁中老年女装秋装',50),('YLMH-845236',NULL,'一力米灰网红方头单鞋女粗跟2020新款百搭珍珠仙女风时尚浅口女鞋',25,44,'388.00','388.00','广东广州',0,'7天无理由退换','2020新款百搭珍珠仙女风时尚浅口女鞋',2);

/*Table structure for table `goods_details_img` */

DROP TABLE IF EXISTS `goods_details_img`;

CREATE TABLE `goods_details_img` (
  `id` varchar(100) NOT NULL COMMENT '商品详情页图片id',
  `number` int(2) DEFAULT NULL COMMENT '图片数量',
  `img1` varchar(200) DEFAULT NULL,
  `img2` varchar(200) DEFAULT NULL,
  `img3` varchar(200) DEFAULT NULL,
  `img4` varchar(200) DEFAULT NULL,
  `img5` varchar(200) DEFAULT NULL,
  `img6` varchar(200) DEFAULT NULL,
  `img7` varchar(200) DEFAULT NULL,
  `img8` varchar(200) DEFAULT NULL,
  `img9` varchar(200) DEFAULT NULL,
  `img10` varchar(200) DEFAULT NULL,
  `context` varchar(255) DEFAULT NULL COMMENT '商品描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods_details_img` */

insert  into `goods_details_img`(`id`,`number`,`img1`,`img2`,`img3`,`img4`,`img5`,`img6`,`img7`,`img8`,`img9`,`img10`,`context`) values ('2J8100010',7,'https://img.alicdn.com/imgextra/i2/1027574646/O1CN01HyFA9j1kBuix4SK0B_!!1027574646.jpg','https://img.alicdn.com/imgextra/i2/1027574646/O1CN01XxIMOi1kBuisnQQ3w_!!1027574646.jpg','https://img.alicdn.com/imgextra/i2/1027574646/O1CN014a9WAA1kBuiwmOzG7_!!1027574646.png','https://img.alicdn.com/imgextra/i2/1027574646/O1CN01OK10rS1kBuisGEC6J_!!1027574646.jpg','https://img.alicdn.com/imgextra/i2/1027574646/O1CN01A70nAp1kBuisBjf4A_!!1027574646.jpg','https://img.alicdn.com/imgextra/i3/1027574646/O1CN01G8mL891kBuiovdx0R_!!1027574646.jpg','https://img.alicdn.com/imgextra/i3/1027574646/O1CN01y15JxJ1kBuirnp8GA_!!1027574646.jpg',NULL,NULL,NULL,NULL),('368024',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'这个鞋子很好'),('7B8160231-1',4,'https://img.alicdn.com/imgextra/i4/1573475524/O1CN01d9s2001qg2V90WuA6_!!1573475524.jpg','https://img.alicdn.com/imgextra/i3/1573475524/O1CN01Azu84S1qg2V2in5HF_!!1573475524.png','https://img.alicdn.com/imgextra/i1/1573475524/O1CN019fqona1qg2V7P5TQv_!!1573475524.jpg','https://img.alicdn.com/imgextra/i1/1573475524/O1CN01kkmwsQ1qg2V7P3nWE_!!1573475524.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL),('AV9370',9,'https://img.alicdn.com/imgextra/i3/890482188/O1CN01zQZ5iO1S29DRrAqwL_!!890482188.jpg','https://img.alicdn.com/imgextra/i4/890482188/O1CN01H64Ati1S29Dqklx0k_!!890482188.jpg','https://img.alicdn.com/imgextra/i1/890482188/O1CN01Qy2xcJ1S298B3SeoF_!!890482188.jpg','https://img.alicdn.com/imgextra/i4/890482188/O1CN01tI0RWB1S298OgJlwN_!!890482188.jpg','https://img.alicdn.com/imgextra/i1/890482188/O1CN01HEC45D1S29D2XkQ54_!!890482188.jpg','https://img.alicdn.com/imgextra/i3/890482188/O1CN01v5dL8k1S29D35vjMB_!!890482188.jpg','https://img.alicdn.com/imgextra/i1/890482188/O1CN01bDXIHN1S29CQ9h0f7_!!890482188.jpg','https://img.alicdn.com/imgextra/i3/890482188/O1CN01Ipgu5Y1S29CQ9it2I_!!890482188.jpg','https://img.alicdn.com/imgextra/i2/890482188/O1CN011S292t2p233lrgQ_!!890482188.jpg',NULL,NULL),('CGU2809TA',7,'https://img.alicdn.com/imgextra/i4/3325938437/O1CN01OeYrLQ2CCCEWSjUR5_!!3325938437.jpg','https://img.alicdn.com/imgextra/i3/3325938437/O1CN01BZtfR52CCCEbCshnI_!!3325938437.jpg','https://img.alicdn.com/imgextra/i1/3325938437/O1CN01mWRgoH2CCCFE0fZfq_!!3325938437.jpg','https://img.alicdn.com/imgextra/i1/3325938437/O1CN01FnSvuy2CCCEVl4M36_!!3325938437.jpg','https://img.alicdn.com/imgextra/i3/3325938437/O1CN01BGih9N2CCCES7CExW_!!3325938437.jpg','https://img.alicdn.com/imgextra/i1/3325938437/O1CN019VUxTK2CCCEVfaZwy_!!3325938437.jpg','https://img.alicdn.com/imgextra/i3/3325938437/O1CN01QkLCQB2CCCEWSkA0k_!!3325938437.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('test1',9,'https://gdp.alicdn.com/imgextra/i2/385132127/TB2zN42bVXXXXXCXXXXXXXXXXXX-385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01W172eE1RaD4jJHF3Y_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN012RkdvH1RaD4fCpAVR_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01ak68ix1RaD4jxFyvT_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01aVAfwk1RaD4bWp8jI_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01mzI33p1RaD4i0hpgZ_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01oPHVgU1RaD4WoTC7g_!!385132127.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('test2',9,'https://gdp.alicdn.com/imgextra/i2/385132127/TB2zN42bVXXXXXCXXXXXXXXXXXX-385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01W172eE1RaD4jJHF3Y_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN012RkdvH1RaD4fCpAVR_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01ak68ix1RaD4jxFyvT_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01aVAfwk1RaD4bWp8jI_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01mzI33p1RaD4i0hpgZ_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01oPHVgU1RaD4WoTC7g_!!385132127.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('test3',9,'https://gdp.alicdn.com/imgextra/i2/385132127/TB2zN42bVXXXXXCXXXXXXXXXXXX-385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01W172eE1RaD4jJHF3Y_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN012RkdvH1RaD4fCpAVR_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01ak68ix1RaD4jxFyvT_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01aVAfwk1RaD4bWp8jI_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01mzI33p1RaD4i0hpgZ_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01oPHVgU1RaD4WoTC7g_!!385132127.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('test4',9,'https://gdp.alicdn.com/imgextra/i2/385132127/TB2zN42bVXXXXXCXXXXXXXXXXXX-385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01W172eE1RaD4jJHF3Y_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN012RkdvH1RaD4fCpAVR_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01ak68ix1RaD4jxFyvT_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01aVAfwk1RaD4bWp8jI_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01mzI33p1RaD4i0hpgZ_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01oPHVgU1RaD4WoTC7g_!!385132127.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('test5',9,'https://gdp.alicdn.com/imgextra/i2/385132127/TB2zN42bVXXXXXCXXXXXXXXXXXX-385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01W172eE1RaD4jJHF3Y_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN012RkdvH1RaD4fCpAVR_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01ak68ix1RaD4jxFyvT_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01aVAfwk1RaD4bWp8jI_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01mzI33p1RaD4i0hpgZ_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01oPHVgU1RaD4WoTC7g_!!385132127.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('test6',9,'https://gdp.alicdn.com/imgextra/i2/385132127/TB2zN42bVXXXXXCXXXXXXXXXXXX-385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01W172eE1RaD4jJHF3Y_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN012RkdvH1RaD4fCpAVR_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01ak68ix1RaD4jxFyvT_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01aVAfwk1RaD4bWp8jI_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01mzI33p1RaD4i0hpgZ_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01oPHVgU1RaD4WoTC7g_!!385132127.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('test7',9,'https://gdp.alicdn.com/imgextra/i2/385132127/TB2zN42bVXXXXXCXXXXXXXXXXXX-385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01W172eE1RaD4jJHF3Y_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN012RkdvH1RaD4fCpAVR_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01ak68ix1RaD4jxFyvT_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01aVAfwk1RaD4bWp8jI_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01mzI33p1RaD4i0hpgZ_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01oPHVgU1RaD4WoTC7g_!!385132127.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('X-19D0087',7,'https://img.alicdn.com/imgextra/i2/2096952851/O1CN01qpIWKt1WvnlTbBGBW_!!2096952851.jpg','https://img.alicdn.com/imgextra/i2/2096952851/O1CN01Qf3cs31WvnlT3EBjo_!!2096952851.jpg','https://img.alicdn.com/imgextra/i1/2096952851/O1CN01NneUGm1WvnlN7d6Ag_!!2096952851.jpg','https://img.alicdn.com/imgextra/i1/2096952851/O1CN01lMCVnK1WvnlQf5zRa_!!2096952851.jpg','https://img.alicdn.com/imgextra/i1/2096952851/O1CN01fRf7iq1WvnlTbCjeb_!!2096952851.jpg','https://img.alicdn.com/imgextra/i3/2096952851/O1CN01Dx5WfO1WvnlO5KQ4K_!!2096952851.jpg','https://img.alicdn.com/imgextra/i1/2096952851/O1CN01SnPD6u1WvnlOOnQy6_!!2096952851.jpg','https://img.alicdn.com/imgextra/i4/385132127/O1CN01wxMzsc1RaD4ewzrxU_!!385132127.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg',NULL,NULL),('YLMH-845236',9,'https://img.alicdn.com/imgextra/i3/2081125364/O1CN010i287u1pUl71oQneN_!!2081125364.jpg','https://img.alicdn.com/imgextra/i2/2081125364/O1CN01rwUCB51pUl7FiwsnQ_!!2081125364.jpg','https://img.alicdn.com/imgextra/i2/2081125364/O1CN01OCoQLA1pUl7FjOCzl_!!2081125364.jpg','https://img.alicdn.com/imgextra/i4/2081125364/O1CN01DEg01v1pUl7HU3M47_!!2081125364.jpg','https://img.alicdn.com/imgextra/i3/2081125364/O1CN01TH8Yir1pUl7JsC5oL_!!2081125364.jpg','https://img.alicdn.com/imgextra/i3/2081125364/O1CN01IPfYla1pUl7NesRtL_!!2081125364.jpg','https://img.alicdn.com/imgextra/i3/2081125364/O1CN01cSflCE1pUl7IlTV1f_!!2081125364.jpg','https://img.alicdn.com/imgextra/i2/2081125364/O1CN01d82GJr1pUl7JbS4Br_!!2081125364.jpg','https://img.alicdn.com/imgextra/i1/2081125364/O1CN01gUZzY91pUl7FBkXzw_!!2081125364.jpg',NULL,NULL);

/*Table structure for table `goods_main_img` */

DROP TABLE IF EXISTS `goods_main_img`;

CREATE TABLE `goods_main_img` (
  `id` varchar(100) NOT NULL,
  `goods_id` varchar(100) DEFAULT NULL COMMENT '商品id（有点多余）',
  `img_number` int(1) DEFAULT '5' COMMENT '主图数量',
  `img1` varchar(200) DEFAULT NULL COMMENT '主图1',
  `img2` varchar(200) DEFAULT NULL COMMENT '主图2',
  `img3` varchar(200) DEFAULT NULL COMMENT '主图3',
  `img4` varchar(200) DEFAULT NULL COMMENT '主图4',
  `img5` varchar(200) DEFAULT NULL COMMENT '主图5',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods_main_img` */

insert  into `goods_main_img`(`id`,`goods_id`,`img_number`,`img1`,`img2`,`img3`,`img4`,`img5`) values ('2J8100010','2J8100010',5,'https://img.alicdn.com/imgextra/i2/1027574646/O1CN01HuFzln1kBuiwmORyE_!!1027574646.jpg','https://img.alicdn.com/imgextra/i2/1027574646/O1CN01HuFzln1kBuiwmORyE_!!1027574646.jpg','https://img.alicdn.com/imgextra/i2/1027574646/O1CN01HuFzln1kBuiwmORyE_!!1027574646.jpg','https://img.alicdn.com/imgextra/i2/1027574646/O1CN01HuFzln1kBuiwmORyE_!!1027574646.jpg','https://img.alicdn.com/imgextra/i2/1027574646/O1CN01HuFzln1kBuiwmORyE_!!1027574646.jpg'),('368024','368024',5,'https://gd4.alicdn.com/imgextra/i4/2329922645/O1CN01P9qivz1VPS5gWY6wx_!!2329922645.jpg','https://gd2.alicdn.com/imgextra/i2/2329922645/O1CN012Jdvcx1VPS5fsuWku_!!2329922645.jpg','https://gd4.alicdn.com/imgextra/i4/2329922645/O1CN011VPS2rQloDCugs4_!!2329922645.jpg','https://gd4.alicdn.com/imgextra/i4/2329922645/O1CN011VPS2rQloDCugs4_!!2329922645.jpg','https://gd4.alicdn.com/imgextra/i4/2329922645/O1CN011VPS2rQloDCugs4_!!2329922645.jpg'),('7B8160231-1','7B8160231-1',5,'https://img.alicdn.com/imgextra/i1/1573475524/O1CN011xXRHQ1qg2VB8Su1q_!!1573475524.jpg','https://img.alicdn.com/imgextra/i3/1573475524/O1CN01pVCBng1qg2V90ZJp5_!!1573475524.jpg','https://img.alicdn.com/imgextra/i3/1573475524/O1CN01PK6FPZ1qg2V2ipITn_!!1573475524.jpg','https://img.alicdn.com/imgextra/i4/1573475524/O1CN01sL1Q0T1qg2VAkppGW_!!1573475524.jpg','https://img.alicdn.com/imgextra/i4/1573475524/O1CN01YfRkjv1qg2VDi5Ode_!!1573475524.jpg'),('AV9370','AV9370',5,'https://img.alicdn.com/imgextra/i2/890482188/O1CN01dg4D391S298Z6Bjeo_!!890482188.jpg','https://img.alicdn.com/imgextra/i1/890482188/O1CN01l845Hf1S298OgFkHr_!!890482188.jpg','https://img.alicdn.com/imgextra/i1/890482188/O1CN01iHIsLe1S29D5K3zVI_!!890482188.jpg','https://img.alicdn.com/imgextra/i1/890482188/O1CN01iHIsLe1S29D5K3zVI_!!890482188.jpg','https://img.alicdn.com/imgextra/i1/890482188/O1CN01iHIsLe1S29D5K3zVI_!!890482188.jpg'),('CGU2809TA','CGU2809TA',5,'https://img.alicdn.com/imgextra/i2/3325938437/O1CN01ICtdfa2CCCEVNWiXw_!!3325938437.jpg','https://img.alicdn.com/imgextra/i2/3325938437/O1CN01OPOPlR2CCCEbCtJE0_!!3325938437.jpg','https://img.alicdn.com/imgextra/i2/3325938437/O1CN01OPOPlR2CCCEbCtJE0_!!3325938437.jpg','https://img.alicdn.com/imgextra/i2/3325938437/O1CN01OPOPlR2CCCEbCtJE0_!!3325938437.jpg','https://img.alicdn.com/imgextra/i2/3325938437/O1CN01OPOPlR2CCCEbCtJE0_!!3325938437.jpg'),('test1','test1',5,'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/385132127/O1CN011W9qyG1RaD4zOpL6Q-385132127.jpg_430x430q90.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01Gyy9041RaD3ntFQWU_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01EKDadA1RaD3iQ6MiY_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01JvQZqf1RaD4APWzGC_!!385132127.jpg'),('test2','test2',5,'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/385132127/O1CN011W9qyG1RaD4zOpL6Q-385132127.jpg_430x430q90.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01Gyy9041RaD3ntFQWU_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01EKDadA1RaD3iQ6MiY_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01JvQZqf1RaD4APWzGC_!!385132127.jpg'),('test3','test3',5,'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/385132127/O1CN011W9qyG1RaD4zOpL6Q-385132127.jpg_430x430q90.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01Gyy9041RaD3ntFQWU_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01EKDadA1RaD3iQ6MiY_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01JvQZqf1RaD4APWzGC_!!385132127.jpg'),('test4','test4',5,'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/385132127/O1CN011W9qyG1RaD4zOpL6Q-385132127.jpg_430x430q90.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01Gyy9041RaD3ntFQWU_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01EKDadA1RaD3iQ6MiY_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01JvQZqf1RaD4APWzGC_!!385132127.jpg'),('test5','test5',5,'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/385132127/O1CN011W9qyG1RaD4zOpL6Q-385132127.jpg_430x430q90.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01Gyy9041RaD3ntFQWU_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01EKDadA1RaD3iQ6MiY_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01JvQZqf1RaD4APWzGC_!!385132127.jpg'),('test6','test6',5,'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/385132127/O1CN011W9qyG1RaD4zOpL6Q-385132127.jpg_430x430q90.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01Gyy9041RaD3ntFQWU_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01EKDadA1RaD3iQ6MiY_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01JvQZqf1RaD4APWzGC_!!385132127.jpg'),('test7','test7',5,'https://img.alicdn.com/imgextra/https://img.alicdn.com/imgextra/i3/385132127/O1CN011W9qyG1RaD4zOpL6Q-385132127.jpg_430x430q90.jpg','https://img.alicdn.com/imgextra/i2/385132127/O1CN01Qk4IIi1RaD3iQ6tw5_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01Gyy9041RaD3ntFQWU_!!385132127.jpg','https://img.alicdn.com/imgextra/i1/385132127/O1CN01EKDadA1RaD3iQ6MiY_!!385132127.jpg','https://img.alicdn.com/imgextra/i3/385132127/O1CN01JvQZqf1RaD4APWzGC_!!385132127.jpg'),('X-19D0087','X-19D0087',5,'https://img.alicdn.com/imgextra/i2/2096952851/O1CN01qpIWKt1WvnlTbBGBW_!!2096952851.jpg','https://img.alicdn.com/imgextra/i2/2096952851/O1CN01Qf3cs31WvnlT3EBjo_!!2096952851.jpg','https://img.alicdn.com/imgextra/i2/2096952851/O1CN01Qf3cs31WvnlT3EBjo_!!2096952851.jpg','https://img.alicdn.com/imgextra/i2/2096952851/O1CN01Qf3cs31WvnlT3EBjo_!!2096952851.jpg','https://img.alicdn.com/imgextra/i2/2096952851/O1CN01Qf3cs31WvnlT3EBjo_!!2096952851.jpg'),('YLMH-845236','YLMH-845236',5,'https://img.alicdn.com/imgextra/i2/2081125364/O1CN01d82GJr1pUl7JbS4Br_!!2081125364.jpg','https://img.alicdn.com/imgextra/i1/2081125364/O1CN01gUZzY91pUl7FBkXzw_!!2081125364.jpg','https://img.alicdn.com/imgextra/i1/2081125364/O1CN01it2ver1pUl7IoS1qC_!!2081125364.jpg','https://img.alicdn.com/imgextra/i3/2081125364/O1CN015UDqRR1pUl7FKArP2_!!2081125364.jpg','https://img.alicdn.com/imgextra/i1/2081125364/O1CN01x0OQRq1pUl7IoPHNS_!!2081125364.jpg');

/*Table structure for table `goods_size` */

DROP TABLE IF EXISTS `goods_size`;

CREATE TABLE `goods_size` (
  `id` varchar(100) NOT NULL COMMENT '尺码ID',
  `name` varchar(20) DEFAULT NULL COMMENT '尺码名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods_size` */

insert  into `goods_size`(`id`,`name`) values ('1','S'),('2','M'),('201','35'),('202','36'),('203','37'),('204','38'),('205','39'),('206','40'),('207','41'),('208','42'),('209','34'),('3','L'),('4','XL'),('5','XXL'),('6','XS'),('7','XXS');

/*Table structure for table `goods_type_select` */

DROP TABLE IF EXISTS `goods_type_select`;

CREATE TABLE `goods_type_select` (
  `id` varchar(100) DEFAULT NULL COMMENT '分类尺码关联表ID',
  `color_id` varchar(100) DEFAULT NULL COMMENT '颜色分类ID',
  `size_id` varchar(100) DEFAULT NULL COMMENT '尺码ID',
  `money` decimal(8,2) DEFAULT NULL COMMENT '当前颜色尺码的价格',
  `stock` int(11) DEFAULT NULL COMMENT '库存',
  KEY `color_id` (`color_id`),
  KEY `size_id` (`size_id`),
  CONSTRAINT `goods_type_select_ibfk_1` FOREIGN KEY (`color_id`) REFERENCES `goods_color` (`id`),
  CONSTRAINT `goods_type_select_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `goods_size` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `goods_type_select` */

insert  into `goods_type_select`(`id`,`color_id`,`size_id`,`money`,`stock`) values ('5001','5','202','529.00',9999),('5001','5','203','529.00',9999),('5001','5','204','529.00',9999),('5001','5','205','529.00',9999),('5001','5','206','529.00',9999),('5002','9','1','920.00',50),('5002','9','2','920.00',49),('5002','9','6','920.00',50),('5003','10','2','458.00',400),('5003','10','3','558.00',999),('5003','10','4','758.00',999),('5003','10','5','1180.00',999),('5004','10','1','823.00',100),('5004','10','2','823.00',100),('5004','10','3','823.00',100),('5005','7','1','1990.00',99),('5005','7','2','1990.00',99),('5005','7','3','1990.00',99),('5006','7','201','388.00',2),('5006','7','202','388.00',2),('5006','7','203','388.00',2),('5006','7','204','388.00',2),('5006','7','205','388.00',2),('5006','8','201','388.00',2),('5006','8','201','388.00',2),('5006','8','201','388.00',2),('5006','8','204','388.00',2),('5006','8','204','388.00',2),('5006','8','204','388.00',2),('5006','7','206','388.00',2),('5007','1001','201','299.00',16807),('5007','1001','201','299.00',16807),('5007','1001','201','299.00',16807),('5007','1001','201','299.00',16807),('5007','1001','201','299.00',16807),('5007','1001','201','299.00',16807),('5007','1002','204','399.00',16807),('5007','1002','204','399.00',16807),('5007','1002','204','399.00',16807),('5007','1002','204','399.00',16807),('5007','1002','204','399.00',16807),('5007','1002','204','399.00',16807),('5007','1002','204','399.00',16807),('5007','1003','201','499.00',16807),('5007','1003','205','499.00',16807),('5007','1004','204','599.00',16807),('5007','1004','204','599.00',16807),('5007','1004','204','599.00',16807),('5007','1004','204','599.00',16807),('5007','1004','204','599.00',16807);

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id` varchar(100) NOT NULL COMMENT '登录账号',
  `password` varchar(20) DEFAULT '123456' COMMENT '登录密码',
  `status` int(1) DEFAULT NULL COMMENT '状态（用户1 商家2 店铺3 管理员4）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `login` */

insert  into `login`(`id`,`password`,`status`) values ('1001','123',1),('1002','123',1),('1003','123',1),('1004','123',1),('1005','123',1),('2001','123',2),('2002','123',2),('2003','123',2),('2004','123',2),('2005','123',2),('3001','123',3),('3002','123',3),('3003','123',3),('3004','123',3),('3005','123',3),('4001','123',4),('4002','123',4),('4003','123',4);

/*Table structure for table `logistics` */

DROP TABLE IF EXISTS `logistics`;

CREATE TABLE `logistics` (
  `id` varchar(100) NOT NULL COMMENT '物流号ID',
  `sent_address` varchar(200) DEFAULT '6006' COMMENT '发货地址',
  `receive_address` varchar(200) DEFAULT '6001' COMMENT '收获地址',
  `sent_time` datetime DEFAULT NULL COMMENT '发货时间',
  `pickup_time` datetime DEFAULT NULL COMMENT '揽件时间',
  `pickup_phone` varchar(11) DEFAULT NULL COMMENT '揽件快递员电话',
  `now_position` varchar(100) DEFAULT NULL COMMENT '当前运输位置',
  `deliverer` varchar(20) DEFAULT NULL COMMENT '派送人',
  `delivery_phone` varchar(11) DEFAULT NULL COMMENT '派送人联系方式',
  `signer` varchar(20) DEFAULT NULL COMMENT '签收人',
  `status` int(1) DEFAULT '0' COMMENT '当前状态（未发货0 发货1 揽件2 运输3 派送4 签收5）',
  `express_company` varchar(20) DEFAULT '申通快递' COMMENT '快递公司（如：顺丰快递）',
  `delivery_evaluate` int(1) DEFAULT NULL COMMENT '快递员评价等级（1-5）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `logistics` */

insert  into `logistics`(`id`,`sent_address`,`receive_address`,`sent_time`,`pickup_time`,`pickup_phone`,`now_position`,`deliverer`,`delivery_phone`,`signer`,`status`,`express_company`,`delivery_evaluate`) values ('10001','6006','6001','2020-12-21 16:53:20',NULL,NULL,NULL,NULL,NULL,NULL,0,'申通快递',1),('10002','6006','6001','2020-12-14 17:07:57','2020-12-15 17:08:08','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',5,'圆通快递',5),('10003','6006','6001','2020-12-08 17:13:00','2020-12-08 23:13:04','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',5,'顺丰快递',5),('10004','6006','6001','2020-12-15 17:13:46','2020-12-16 17:13:57','13111111111',NULL,NULL,NULL,NULL,0,'韵达快递',3),('10005','6006','6001','2020-12-17 17:17:04','2020-12-18 17:17:08','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111',NULL,4,'顺丰快递',0),('10006','6006','6001','2020-12-13 16:21:27','2020-12-15 16:21:38','13111111111','广州市海珠区仲恺农业工程学院学生公寓',NULL,NULL,NULL,0,'申通快递',NULL),('10007','6006','6001','2020-12-13 16:21:30','2020-12-15 16:21:42','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',0,'申通快递',5),('10008','6006','6001','2020-12-13 16:21:32','2020-12-15 16:21:51','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',0,'申通快递',5),('10009','6006','6001','2020-12-13 16:21:35','2020-12-15 16:21:53','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',0,'申通快递',5),('10010','6006','6001','2020-12-11 19:24:37',NULL,NULL,NULL,'快递员a','13111111111','收件人1',0,'申通快递',5),('10011','6006','6001','2020-12-11 19:24:37','2020-12-12 19:28:28','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',0,'申通快递',5),('10012','6006','6001','2020-12-11 19:24:37','2020-12-12 19:28:28','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',0,'申通快递',5),('10013','6006','6001','2020-12-11 19:24:37','2020-12-12 19:28:28','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',0,'申通快递',5),('10014','6006','6001','2020-12-11 19:24:37','2020-12-12 19:28:28','13111111111','广州市海珠区仲恺农业工程学院学生公寓','快递员a','13111111111','收件人1',0,'申通快递',5),('1111','6006','6001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'111',NULL),('202020202020','6006','6001',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'申通',NULL);

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id` varchar(100) NOT NULL COMMENT '订单号id',
  `goods_id` varchar(100) DEFAULT NULL COMMENT '商品ID',
  `order_details_id` varchar(100) DEFAULT NULL COMMENT '订单详情id',
  `user_id` varchar(100) DEFAULT '1001' COMMENT '用户id',
  `time` datetime DEFAULT NULL COMMENT '时间',
  `number` int(11) DEFAULT '1' COMMENT '数量',
  `shop_id` varchar(100) DEFAULT '3001' COMMENT '店铺ID',
  PRIMARY KEY (`id`),
  KEY `goods_id` (`goods_id`),
  KEY `order_details_id` (`order_details_id`),
  KEY `user_id` (`user_id`),
  KEY `shop_id` (`shop_id`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`),
  CONSTRAINT `order_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `order_ibfk_4` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order` */

insert  into `order`(`id`,`goods_id`,`order_details_id`,`user_id`,`time`,`number`,`shop_id`) values ('8001','2J8100010','8001','1001','2020-12-20 16:48:55',1,'3001'),('8002','368024','8002','1001','2020-12-12 17:05:32',1,'3001'),('8003','7B8160231-1','8003','1001','2020-12-04 17:05:45',1,'3001'),('8004','AV9370','8004','1001','2020-12-15 17:05:56',1,'3001'),('8005','CGU2809TA','8005','1001','2020-12-17 17:17:51',1,'3001'),('8006','2J8100010','8006','1001','2020-12-22 16:03:30',1,'3001'),('8007','2J8100010','8007','1001','2020-12-21 16:03:32',1,'3001'),('8008','2J8100010','8008','1001','2020-12-22 16:03:35',1,'3001'),('8009','2J8100010','8009','1001','2020-12-22 16:03:37',1,'3001'),('8010','368024','8010','1001','2020-12-12 16:10:06',1,'3001'),('8011','368024','8011','1001','2020-12-12 16:10:24',1,'3001'),('8012','368024','8012','1001','2020-12-12 16:10:26',1,'3001'),('8013','368024','8013','1001','2020-12-12 16:10:28',1,'3001'),('8014','CGU2809TA','8014','1001','2020-12-11 19:25:49',1,'3001'),('8015','CGU2809TA','8015','1001','2020-12-11 19:25:51',1,'3001'),('8016','CGU2809TA','8016','1001','2020-12-11 19:25:53',1,'3001'),('8017','CGU2809TA','8017','1001','2020-12-11 19:25:55',1,'3001'),('8018','AV9370','8018','1001','2020-12-10 19:31:51',1,'3001'),('8019','AV9370','8019','1001','2020-12-10 19:31:51',1,'3001'),('8020','AV9370','8020','1001','2020-12-10 19:31:51',1,'3001'),('8021','AV9370','8021','1001','2020-12-10 19:31:51',1,'3001');

/*Table structure for table `order_commodity` */

DROP TABLE IF EXISTS `order_commodity`;

CREATE TABLE `order_commodity` (
  `id` varchar(100) NOT NULL COMMENT '订单包含商品表ID',
  `order_id` varchar(100) DEFAULT NULL COMMENT '订单ID',
  `color` varchar(20) DEFAULT '黑色' COMMENT '颜色',
  `size` varchar(20) DEFAULT '均码' COMMENT '尺码',
  `goods_id` varchar(100) DEFAULT NULL COMMENT '商品ID',
  `original_money` decimal(8,2) DEFAULT '199.00' COMMENT '原始金额',
  `real_money` decimal(8,2) DEFAULT '199.00' COMMENT '最终金额',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `status` int(1) DEFAULT '0' COMMENT '当前状态（已付款0 申请退款1 退款中2 退款成功3）',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `goods_id` (`goods_id`),
  CONSTRAINT `order_commodity_ibfk_2` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order_commodity` */

insert  into `order_commodity`(`id`,`order_id`,`color`,`size`,`goods_id`,`original_money`,`real_money`,`number`,`status`) values ('8001','8001','黑色','均码','2J8100010','287.00','287.00',1,0),('8002','8002','黑色','均码','368024','246.00','246.00',1,0),('8003','8003','黑色','均码','7B8160231-1','546.00','546.00',1,0),('8004','8004','黑色','均码','AV9370','276.00','276.00',1,0),('8005','8005','黑色','均码','2J8100010','199.00','199.00',1,0),('8006','8006','黑色','均码','2J8100010','199.00','199.00',1,0),('8007','8007','黑色','均码','2J8100010','199.00','199.00',1,0),('8008','8008','黑色','均码','2J8100010','199.00','199.00',1,0),('8009','8009','黑色','均码','2J8100010','199.00','199.00',1,0),('8010','8010','黑色','均码','368024','199.00','199.00',1,0),('8011','8011','黑色','均码','368024','199.00','199.00',1,0),('8012','8012','黑色','均码','368024','199.00','199.00',1,0),('8013','8013','黑色','均码','368024','199.00','199.00',1,0),('8014','8014','黑色','均码','CGU2809TA','199.00','199.00',1,0),('8015','8015','黑色','均码','CGU2809TA','199.00','199.00',1,0),('8016','8016','黑色','均码','CGU2809TA','199.00','199.00',1,0),('8017','8017','黑色','均码','CGU2809TA','199.00','199.00',1,0),('8018','8018','黑色','均码','AV9370','199.00','199.00',1,0),('8019','8019','黑色','均码','AV9370','199.00','199.00',1,0),('8020','8020','黑色','均码','AV9370','199.00','199.00',1,0),('8021','8021','黑色','均码','AV9370','199.00','199.00',1,0);

/*Table structure for table `order_details` */

DROP TABLE IF EXISTS `order_details`;

CREATE TABLE `order_details` (
  `id` varchar(100) NOT NULL COMMENT '订单详情id',
  `order_id` varchar(100) DEFAULT NULL COMMENT '订单号',
  `status` int(1) DEFAULT NULL COMMENT '未付款0 已付款1 已发货2 已收货3 已评价4 已追评5 已过期6 申请退款7 退款中8 退款成功9',
  `goods_id` varchar(100) DEFAULT NULL COMMENT '商品id',
  `real_pay` decimal(8,2) DEFAULT '199.00' COMMENT '实付款',
  `shop_id` varchar(100) DEFAULT '3001' COMMENT '店铺id',
  `total_price` decimal(8,2) DEFAULT '199.00' COMMENT '商品总价',
  `freight` int(3) DEFAULT '0' COMMENT '运费险',
  `freight_insurance` int(1) DEFAULT '0' COMMENT '是否有运费险（1有0无）',
  `shop_discount` decimal(8,2) DEFAULT '0.00' COMMENT '店铺优惠',
  `lucky_money` decimal(8,2) DEFAULT '0.00' COMMENT '红包',
  `remarks` varchar(100) DEFAULT NULL COMMENT '备注信息',
  `logistics_id` varchar(100) DEFAULT NULL COMMENT '物流信息ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `pay_time` datetime DEFAULT NULL COMMENT '付款时间',
  `sent_time` datetime DEFAULT NULL COMMENT '发货时间',
  `deal_time` datetime DEFAULT NULL COMMENT '成交时间（确认收货时间）',
  `user_id` varchar(100) DEFAULT '1001' COMMENT '用户ID',
  `shipping_address_id` varchar(100) DEFAULT '6001' COMMENT '收获地址ID',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `shop_id` (`shop_id`),
  KEY `logistics_id` (`logistics_id`),
  KEY `user_id` (`user_id`),
  KEY `shipping_address_id` (`shipping_address_id`),
  CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`),
  CONSTRAINT `order_details_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `order_details_ibfk_5` FOREIGN KEY (`shipping_address_id`) REFERENCES `shipping_address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order_details` */

insert  into `order_details`(`id`,`order_id`,`status`,`goods_id`,`real_pay`,`shop_id`,`total_price`,`freight`,`freight_insurance`,`shop_discount`,`lucky_money`,`remarks`,`logistics_id`,`create_time`,`pay_time`,`sent_time`,`deal_time`,`user_id`,`shipping_address_id`) values ('8001','8001',1,'2J8100010','287.00','3001','287.00',0,1,'0.00','0.00',NULL,'10001','2020-12-31 16:10:45','2020-12-31 16:10:54','2020-12-31 16:19:03',NULL,'1001','6001'),('8002','8002',1,'368024','246.00','3001','246.00',0,1,'0.00','0.00',NULL,'10002','2020-12-12 16:58:35','2020-12-31 16:58:37','2020-12-31 16:19:03',NULL,'1001','6001'),('8003','8003',3,'7B8160231-1','546.00','3001','546.00',0,1,'0.00','0.00',NULL,'10003','2020-12-04 17:00:49','2020-12-04 17:00:52','2020-12-08 17:01:00','2020-12-17 17:01:05','1001','6001'),('8004','8004',6,'AV9370','276.00','3001','276.00',0,1,'0.00','0.00',NULL,'10004','2020-12-15 17:01:46','2020-12-15 17:01:50','2020-12-16 17:01:52','2020-12-23 20:18:19','1001','6001'),('8005','8005',2,'CGU2809TA','127.00','3001','127.00',0,1,'0.00','0.00',NULL,'10005','2020-12-17 17:15:59','2020-12-17 17:16:03','2020-12-18 17:16:05',NULL,'1001','6001'),('8006','8006',0,'2J8100010','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-22 16:08:43','2020-12-31 16:19:03','2020-12-31 16:19:03',NULL,'1001','6001'),('8007','8007',0,'2J8100010','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-21 16:08:47','2020-12-31 16:19:03','2020-12-31 16:19:03',NULL,'1001','6001'),('8008','8008',1,'2J8100010','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-22 16:09:04','2020-12-22 16:09:04','2020-12-31 16:19:03',NULL,'1001','6001'),('8009','8009',2,'2J8100010','199.00','3001','199.00',0,0,'0.00','0.00',NULL,'202020202020','2020-12-22 16:09:06','2020-12-22 16:09:06','2020-12-31 16:19:03',NULL,'1001','6001'),('8010','8010',3,'368024','199.00','3001','199.00',0,0,'0.00','0.00',NULL,'10006','2020-12-12 16:18:40','2020-12-12 16:18:53','2020-12-13 16:19:05',NULL,'1001','6001'),('8011','8011',3,'368024','199.00','3001','199.00',0,0,'0.00','0.00',NULL,'10007','2020-12-12 16:18:42','2020-12-12 16:18:58','2020-12-13 16:19:12',NULL,'1001','6001'),('8012','8012',3,'368024','199.00','3001','199.00',0,0,'0.00','0.00',NULL,'10008','2020-12-12 16:18:44','2020-12-12 16:19:00','2020-12-13 16:19:14',NULL,'1001','6001'),('8013','8013',2,'368024','199.00','3001','199.00',0,0,'0.00','0.00',NULL,'1111','2020-12-12 16:18:47','2020-12-31 16:19:03','2020-12-31 16:19:03',NULL,'1001','6001'),('8014','8014',6,'CGU2809TA','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-11 19:27:05','2020-12-11 19:27:05','2020-12-12 19:27:19','2020-12-23 20:18:22','1001','6001'),('8015','8015',6,'CGU2809TA','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-11 19:27:05','2020-12-11 19:27:05','2020-12-12 19:27:19','2020-12-23 20:18:23','1001','6001'),('8016','8016',6,'CGU2809TA','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-11 19:27:05','2020-12-11 19:27:05','2020-12-12 19:27:19','2020-12-23 20:18:25','1001','6001'),('8017','8017',6,'CGU2809TA','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-11 19:27:05','2020-12-11 19:27:05','2020-12-12 19:27:19','2020-12-23 20:18:26','1001','6001'),('8018','8018',7,'AV9370','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-10 19:30:06','2020-12-10 19:30:06','2020-12-16 19:30:20',NULL,'1001','6001'),('8019','8019',7,'AV9370','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-10 19:30:06','2020-12-10 19:30:06','2020-12-16 19:30:20',NULL,'1001','6001'),('8020','8020',7,'AV9370','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-10 19:30:06','2020-12-10 19:30:06','2020-12-16 19:30:20',NULL,'1001','6001'),('8021','8021',7,'AV9370','199.00','3001','199.00',0,0,'0.00','0.00',NULL,NULL,'2020-12-10 19:30:06','2020-12-10 19:30:06','2020-12-16 19:30:20',NULL,'1001','6001');

/*Table structure for table `order_property` */

DROP TABLE IF EXISTS `order_property`;

CREATE TABLE `order_property` (
  `id` varchar(100) NOT NULL COMMENT '商品基本属性id（管理员维护该表）',
  `name` varchar(50) DEFAULT NULL COMMENT '属性名',
  `is_required` int(1) DEFAULT '0' COMMENT '是否必填（0否1是）',
  `catagory_id` varchar(100) DEFAULT NULL COMMENT '所属分类id',
  PRIMARY KEY (`id`),
  KEY `catagory_id` (`catagory_id`),
  CONSTRAINT `order_property_ibfk_1` FOREIGN KEY (`catagory_id`) REFERENCES `catagory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order_property` */

insert  into `order_property`(`id`,`name`,`is_required`,`catagory_id`) values ('1','id',0,NULL),('2','品牌',0,NULL),('3','适用年龄',0,NULL),('4','货号',0,NULL),('5','上市年份季节',0,NULL),('6','销售渠道类型',0,NULL),('7','材质成分',0,NULL),('8','基础风格',0,NULL),('9','图案',0,NULL);

/*Table structure for table `referal_link` */

DROP TABLE IF EXISTS `referal_link`;

CREATE TABLE `referal_link` (
  `id` varchar(100) NOT NULL COMMENT '外部网站ID',
  `name` varchar(20) DEFAULT NULL COMMENT '外部网站名称',
  `url` varchar(200) DEFAULT NULL COMMENT '外部网站访问路径',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `referal_link` */

insert  into `referal_link`(`id`,`name`,`url`) values ('1','天猫会员','https://vip.tmall.com/vip/index.htm?spm=875.7931836/B.2016004.3.7fae4265FR0oPD&acm=lb-zebra-148799-667861.1003.4.2429983&scm=1003.4.lb-zebra-148799-667861.OTHER_14561845383563_2429983'),('2','电器城','https://list.tmall.com/search_product.htm?spm=875.7931836/B.2016004.4.7fae4265FR0oPD&q=%CA%FD%C2%EB'),('3','喵鲜生','https://miao.tmall.com/?spm=875.7931836'),('4','医药馆','https://yao.tmall.com/?spm=875.7931836/B.2016004.6.7fae4265FR0oPD'),('5','营业厅','https://login.taobao.com/'),('6','魅力惠','https://www.tmall.com/wow/mlh/act/timei?spm=875.7931836/B.2016004.8.7fae4265FR0oPD429983'),('7','飞猪旅行','https://www.fliggy.com/?spm=875.7931836/B.2016004.9.7fae4265FR0oPD'),('8','苏宁易购','https://suning.tmall.com/?spm=875.7931836/B.2016004.10.7fae4265FR0oPD');

/*Table structure for table `shipping_address` */

DROP TABLE IF EXISTS `shipping_address`;

CREATE TABLE `shipping_address` (
  `id` varchar(100) NOT NULL COMMENT '收获地址ID',
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户ID',
  `receiver_name` varchar(50) DEFAULT NULL COMMENT '收货人姓名',
  `phone_number` varchar(11) DEFAULT NULL COMMENT '电话',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `is_default` int(1) DEFAULT '0' COMMENT '是否为默认地址（1是，只能有一个）',
  `remarks` varchar(200) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `shipping_address` */

insert  into `shipping_address`(`id`,`user_id`,`receiver_name`,`phone_number`,`address`,`is_default`,`remarks`) values ('6001','1001','收件人1','13111111111','广东省广州市海珠区仲恺路500号',1,'不要打电话，发短信就可以了。'),('6002','1001','收件人2','13111111111','广东省广州市海珠区仲恺路500号',0,NULL),('6003','1001','收件人3','13111111111','广东省广州市海珠区仲恺路500号',0,NULL),('6004','1002','收件人1','13111111111','广东省广州市海珠区仲恺路500号',1,NULL),('6005','1003','收件人1','13111111111','广东省广州市海珠区仲恺路500号',1,NULL),('6006','2001','发货人1','13111111111','广东省广州市海珠区仲恺路500号',1,'这个快递很重要，请尽快送达。');

/*Table structure for table `shop` */

DROP TABLE IF EXISTS `shop`;

CREATE TABLE `shop` (
  `id` varchar(100) NOT NULL COMMENT '店铺ID',
  `business_id` varchar(100) DEFAULT NULL COMMENT '商家ID',
  `name` varchar(50) DEFAULT NULL COMMENT '店铺名',
  `qualification` varchar(100) DEFAULT '已签署消保协议' COMMENT '资质',
  `enterprise_qualification` varchar(100) DEFAULT '营业执照' COMMENT '企业资质',
  `card_img` varchar(100) DEFAULT NULL COMMENT '店铺名片（二维码）',
  `open_date` datetime DEFAULT NULL COMMENT '开店日期',
  `feedback_rate` decimal(5,2) DEFAULT '5.00' COMMENT '店铺好评率',
  `describe_fit` decimal(2,1) DEFAULT '5.0' COMMENT '描述相符',
  `service_attitude` decimal(2,1) DEFAULT '5.0' COMMENT '服务态度',
  `logistics_service` decimal(2,1) DEFAULT '5.0' COMMENT '物流服务',
  `comprehensive_experience` decimal(2,1) DEFAULT '5.0' COMMENT '综合体验',
  `fans_number` int(11) DEFAULT '0' COMMENT '粉丝数',
  `level` int(2) DEFAULT '1' COMMENT '当前店铺等级',
  PRIMARY KEY (`id`),
  KEY `business_id` (`business_id`),
  CONSTRAINT `shop_ibfk_1` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `shop` */

insert  into `shop`(`id`,`business_id`,`name`,`qualification`,`enterprise_qualification`,`card_img`,`open_date`,`feedback_rate`,`describe_fit`,`service_attitude`,`logistics_service`,`comprehensive_experience`,`fans_number`,`level`) values ('05ca913f-7c85-478b-8134-60158e527b63','2001','test1','已签署消保协议','营业执照',NULL,'2020-12-22 15:49:26','5.00','5.0','5.0','5.0','5.0',0,1),('3001','2001','店铺A','已签署消保协议','营业执照',NULL,'2020-12-07 21:25:31','4.70','4.7','4.7','4.7','4.7',3251,2),('3002','2002','店铺B','已签署消保协议','营业执照',NULL,'2020-12-07 21:25:36','4.80','4.8','4.7','4.9','4.8',102353,7),('3003','2003','店铺C','已签署消保协议','营业执照',NULL,'2020-12-07 21:25:37','5.00','4.9','4.7','4.8','4.8',325,1),('3004','2001','NIKE','已签署消保协议','营业执照',NULL,'2020-12-07 21:25:38','4.90','4.7','4.7','4.8','4.7',100,1),('3005','2004','店铺E','已签署消保协议','营业执照',NULL,'2020-12-07 21:25:40','4.70','5.0','4.7','4.7','4.8',25468,3),('3006','2001','安踏','已签署消保协议','营业执照',NULL,'2020-12-27 20:00:09','5.00','5.0','5.0','5.0','5.0',0,1),('4744e109-73cb-438c-a03a-dfcc9beda3d0','2001','1111','已签署消保协议','营业执照',NULL,'2020-12-31 17:14:18','5.00','5.0','5.0','5.0','5.0',0,1),('80a56398-abff-4d4e-b312-6795cc5d35fe','2001','test123','已签署消保协议','营业执照',NULL,'2021-01-02 15:16:54','5.00','5.0','5.0','5.0','5.0',0,1),('a686b4ed-3b8f-43dd-b880-c7f21a65f334','2001','test','已签署消保协议','营业执照',NULL,'2020-12-22 10:39:46','5.00','5.0','5.0','5.0','5.0',0,1),('c05d682e-39a5-451f-943a-f42460b659c0','2001','121312','已签署消保协议','营业执照',NULL,'2020-12-31 16:59:04','5.00','5.0','5.0','5.0','5.0',0,1),('fa4865b2-1c7b-4783-9db1-4e9406db181a','2001','test','已签署消保协议','营业执照',NULL,'2020-12-30 13:06:06','5.00','5.0','5.0','5.0','5.0',0,1);

/*Table structure for table `shopping_cart` */

DROP TABLE IF EXISTS `shopping_cart`;

CREATE TABLE `shopping_cart` (
  `id` varchar(100) NOT NULL COMMENT '用户购物车ID',
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户ID',
  `goods_id` varchar(100) DEFAULT NULL COMMENT '商品ID',
  `color` varchar(20) DEFAULT NULL COMMENT '颜色分类',
  `size` varchar(20) DEFAULT NULL COMMENT '尺码',
  `number` int(11) DEFAULT NULL COMMENT '数量',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `money` int(11) DEFAULT NULL COMMENT '金额',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `goods_id` (`goods_id`),
  CONSTRAINT `shopping_cart_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `shopping_cart_ibfk_2` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `shopping_cart` */

insert  into `shopping_cart`(`id`,`user_id`,`goods_id`,`color`,`size`,`number`,`add_time`,`money`) values ('11b40fir8jr400000','1001','7B8160231-1','1','1',2,'2020-12-31 16:50:56',916),('159ad3eheem80000',NULL,'2J8100010','1','1',1,'2020-12-30 13:31:08',NULL),('15c7zb3yx6kg00000',NULL,'2J8100010','1','1',1,'2020-12-30 13:29:40',NULL),('15ngbn7rry2o','1001','2J8100010','1','1',1,'2020-12-20 12:48:13',1990),('16id0wdt8lxc00000',NULL,'2J8100010','1','1',1,'2020-12-30 13:33:53',NULL),('7e5n8gb11tc00000',NULL,'2J8100010','1','1',1,'2020-12-30 13:32:01',NULL),('9dim5vjp8ug00000','1001','7B8160231-1','1','1',2,'2020-12-30 13:40:46',NULL),('dk6nrsypbo000000','1001','368024','1','1',1,'2020-12-30 13:44:01',529),('epaozfn6cm8','1001','CGU2809TA','1','1',2,'2020-12-21 16:41:56',1646),('evz01p9d3fk','1001','368024','1','1',1,'2020-12-25 10:38:56',529),('h934tutaijc00000','1001','368024','1','1',1,'2020-12-31 17:02:21',529),('jbaxim2iwi800000','1001','test6','1','1',2,'2020-12-31 12:25:21',618),('krrrroh37wg','1005','7B8160231-1','1','1',1,'2020-12-25 10:45:30',458),('kwiaz66dan400000','1001','test1','1','1',1,'2020-12-31 17:13:03',309),('pa950ahe96o00000','1001','CGU2809TA','1','1',2,'2020-12-31 17:08:44',1646),('t6iuc1q6qk000000',NULL,'2J8100010','1','1',2,'2020-12-30 13:31:02',NULL),('vaoz3m3957k00000','1001','368024','1','1',2,'2020-12-31 12:19:03',1058),('wrtmkfqtr0000000','1001','368024','1','1',2,'2020-12-30 13:44:06',1058),('xbzdmz8vl1c00000','1001','368024','1','1',2,'2020-12-31 12:19:09',1058),('xv9jk0tzo9c00000','1001','AV9370','1','1',1,'2020-12-31 12:22:38',299);

/*Table structure for table `system` */

DROP TABLE IF EXISTS `system`;

CREATE TABLE `system` (
  `id` varchar(100) NOT NULL COMMENT '管理员id',
  `real_name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `sex` char(2) DEFAULT NULL COMMENT '性别',
  `phone_number` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `living` varchar(200) DEFAULT NULL COMMENT '居住地',
  `join_date` datetime DEFAULT NULL COMMENT '入职日期',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `system` */

insert  into `system`(`id`,`real_name`,`sex`,`phone_number`,`living`,`join_date`,`id_card`,`email`,`age`) values ('4001','管理员1','男','13111111111','广东省广州市海珠区滨江街道仲恺路500号','2020-12-07 21:36:41','445222222222222222','123456789@qq.com',41),('4002','管理员2','女','13111111111','广东省广州市海珠区滨江街道仲恺路500号','2020-12-07 21:36:42','445222222222222222','123456789@qq.com',42),('4003','管理员3','男','13111111111','广东省广州市海珠区滨江街道仲恺路500号','2020-12-07 21:36:44','445222222222222222','123456789@qq.com',43);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` varchar(100) NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `sex` char(2) DEFAULT NULL COMMENT '性别',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `real_name` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `id_card` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `brief` varchar(100) DEFAULT NULL COMMENT '个性签名',
  `regist_date` datetime DEFAULT NULL COMMENT '注册时间',
  `face_img` varchar(100) DEFAULT NULL COMMENT '头像',
  `alipay_account` varchar(11) DEFAULT NULL COMMENT '支付宝账号',
  `phone_number` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `bank_account` varchar(19) DEFAULT NULL COMMENT '银行卡号',
  `age` int(3) DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`sex`,`email`,`real_name`,`id_card`,`brief`,`regist_date`,`face_img`,`alipay_account`,`phone_number`,`bank_account`,`age`) values ('1001','lena','女','155555555@qq.com','lena','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 20:59:31',NULL,'13111111111','13111111111','6532555555555555',20),('1002','用户2','女','155555555@qq.com','零零二','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 20:59:36',NULL,'13111111111','13111111111','6532555555555555',21),('1003','用户3','男','155555555@qq.com','零零三','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 20:59:38',NULL,'13111111111','13111111111','6532555555555555',22),('1004','用户4','女','155555555@qq.com','零零四','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 20:59:39',NULL,'13111111111','13111111111','6532555555555555',23),('1005','用户5','男','155555555@qq.com','零零五','445222222222222222','这个人很懒，什么也没有写...','2020-12-07 20:59:40',NULL,'13111111111','13111111111',NULL,24);

/*Table structure for table `user_bill` */

DROP TABLE IF EXISTS `user_bill`;

CREATE TABLE `user_bill` (
  `id` varchar(100) NOT NULL COMMENT '账单id',
  `user_id` varchar(100) DEFAULT NULL COMMENT '用户id',
  `business_id` varchar(100) DEFAULT NULL COMMENT '商家id',
  `order_id` varchar(100) DEFAULT NULL COMMENT '订单id',
  `pay_amount` decimal(8,2) DEFAULT NULL COMMENT '支付金额',
  `pay_mode` varchar(50) DEFAULT NULL COMMENT '支付方式',
  `bill_type` varchar(20) DEFAULT NULL COMMENT '账单分类',
  `goods_describe` varchar(50) DEFAULT NULL COMMENT '商品说明',
  `business_phone` varchar(11) DEFAULT NULL COMMENT '商家联系电话',
  `status` int(1) DEFAULT '0' COMMENT '交易状态（0正常1异常）',
  `remarks` varchar(100) DEFAULT NULL COMMENT '备注信息',
  `time` datetime DEFAULT NULL COMMENT '交易时间',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `business_id` (`business_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `user_bill_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_bill_ibfk_2` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`),
  CONSTRAINT `user_bill_ibfk_3` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_bill` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
