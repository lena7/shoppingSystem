package cn.web.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2021000116684393";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDFx7c4UaDD+HsvFXSo0FYWLryuZL7thE153WWOSWaQCdCzdkzeZetTxoPTQb8WoHnwJhneBJ4AIuWLa6sFdy1T9SOpbOy4s9XmlyKLZAM19cUtDIvnl8xfHO+J0UTFif3nw/CNGN4kkYplRgSZYlWEInOslboVwA+pe683xS5jmN0vPi7VOqh6Fi4bvzPO19VoZU6qxlAAdNWkrjDPeLDmfvp83M3pkyB2DXt5wjdAYcLImuKlSh+8zgnGQhLDcoRH5wW4FXQU8ZKznGUNTwjdQfMtjo/G0E1YBQDBVrlB0SaDoKiBrMBapxGkhvophmqJfOgDEr/lE0kMOOUfrHwHAgMBAAECggEBAL7Z5vLgyhrxh6U6thVE3R2QwiAjNZ6rYbvsKdiw+diasBrNWIXvobN4x7KvKf9ioehcS+/TUshc5FaychjXFRWEY4JViuT9HLbkNsB9pR+H51p+Ve8Iq31Ixdme6Bca/rmMeW8ehRaSnxL1CvEcU9srB6aN4UCl0jdBFCbkDsypfeZycjrfTsY1neIf7JuGnrx1PJiulIaK5+AIfuuD4VirnwjuUpEpI8lpj0fIzNgx8IFiBvylLg5B6du36zBXQ8AGkm0Y4vGtr++C30CWuY/an1EU/r7z4e1OGex313XBkuhyg+IAKnTk6mhmrNdPvJBghe6q3e+GVzdjSkrK+1ECgYEA8GZVciI4xamjqB+fSzw6KUuzwCsDbpzI1pe+P71NHBBF9RKwC4aj8U2D1bmOi8Awr8KVQFMuBl1hioDUZDnqP41KxKR+g6XBKYdk9AfVF+s+wakVnG4XzrSx8b4sZaXG19caCw4fYVx5eaQGfvbG4aYJi1dfvn6qYi/JQ5mVWB8CgYEA0p1b+Rgez3iE3lqZCb+UiwHVzWGSDGk7aVPAaQFaASaxdWmq2kvMDkpUPDnYF+b/8uvpA151pXK9tCjveCe/LvD/gm7UlgxTfQX5z8FJqf9mAz7zYyUGbGBtThP4NWFx+mcGHinsPxQB6yisiON+dJZW/P7LIhknBozLJDiw/xkCgYA1n+4xfnbBiNzL1Pn4k1+59HwNQssKMokObVzv5O89clfYmimQ+KC7Y/ojg7fLbrytiC844RUkMFJgzeoSzWR2drMV+EeMYD4dYGlhkhmzszMVif0bHufKoT4MWcqXhgo7/Wx+yNCxLCqgurkEO1IT5bC9V2kxGhoxus0fejP5KwKBgH+wYZN6mStlwQBh4wiy+ubv/OSgtAAMSLXJzgybgirSf6JUpv3kGFnNnJZK54VOSnzHpCSjvyXIk9THQLATQOi/udklQoOaA3/4AIWBhRyot/tdzgec6fEI4RwGJWSPqJ+MmHvvJMj1Y1hj936ilgcvIpwOCa+pmixxbbOPH1f5AoGAZAB8wDlipDEwwbVL6othEB1ezyQVRHW9Lqf41AACtybOa/6/G4HaqOjklp9cmukVkoFSCeMlFH9gwcUp3ILCYPK6aK9ocHyitaFoQrkm2XwF06DvVPeUGgUNzsVuEEknxwwNR3A6+uN6XsmcyacxOLRFSKvJsO+oEzknxEJtkW4=";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhauGpN9nXCflAtdyXbFmet/zEs3Wg1xgRiK0cTz7qh5/zlNWpAVdfjb7GcVMa7uzJyo93b0pK3yd9FjbQiWtMFrO9tOGgCc7da1xpPVvGT2Lbgh61Xi+lUaKbBICJCqTwUEItWS9DEWg6JCvLGOWwDjjCiemInkrpTGIRPpUqS5AX0+tFM8LDhvWz+5QEyDHbEcWBBzbVKgvJltNYJykkdzXsblR4VImiFoX0TcOK3IiZYZP3IXEXlkTMMmtEdqMUdOypxe2H8Ig0xw4HpmIeVYL6dFA1dKTHcFzFGfbSNZJvy37T6JJ4KixLrXsMszyHpqGGrrLh93DjzPj3HIK5QIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:9101/pages/alipay/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:9101/pages/user/index.html";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "E:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

