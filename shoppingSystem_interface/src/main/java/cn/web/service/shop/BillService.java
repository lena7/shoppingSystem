package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Bill;

import java.util.*;

/**
 * bill业务逻辑层
 */
public interface BillService {


    public List<Bill> findAll();


    public PageResult<Bill> findPage(int page, int size);


    public List<Bill> findList(Map<String,Object> searchMap);


    public PageResult<Bill> findPage(Map<String,Object> searchMap,int page, int size);


    public Bill findById(String id);

    public void add(Bill bill);


    public void update(Bill bill);


    public void delete(String id);

}
