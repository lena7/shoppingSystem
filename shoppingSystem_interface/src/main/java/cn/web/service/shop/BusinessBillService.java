package cn.web.service.shop;

import cn.web.entity.PageResult;
import cn.web.pojo.shop.BusinessBill;

import java.util.List;
import java.util.Map;

/**
 * BusinessBill业务逻辑层
 */
public interface BusinessBillService {


    public List<BusinessBill> findAll();


    public PageResult<BusinessBill> findPage(int page, int size);


    public List<BusinessBill> findList(Map<String, Object> searchMap);


    public PageResult<BusinessBill> findPage(Map<String, Object> searchMap, int page, int size);


    public BusinessBill findById(String id);

    public void add(BusinessBill BusinessBill);


    public void update(BusinessBill BusinessBill);


    public void delete(String id);

}
