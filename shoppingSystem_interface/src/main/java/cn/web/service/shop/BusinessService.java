package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Business;

import java.util.*;

/**
 * business业务逻辑层
 */
public interface BusinessService {


    public List<Business> findAll();


    public PageResult<Business> findPage(int page, int size);


    public List<Business> findList(Map<String,Object> searchMap);


    public PageResult<Business> findPage(Map<String,Object> searchMap,int page, int size);


    public Business findById(String id);

    public void add(Business business);


    public void update(Business business);


    public void delete(String id);

}
