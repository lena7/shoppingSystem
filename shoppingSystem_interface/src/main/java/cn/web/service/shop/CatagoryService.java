package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Catagory;

import java.util.*;

/**
 * catagory业务逻辑层
 */
public interface CatagoryService {


    public List<Catagory> findAll();


    public PageResult<Catagory> findPage(int page, int size);


    public List<Catagory> findList(Map<String,Object> searchMap);


    public PageResult<Catagory> findPage(Map<String,Object> searchMap,int page, int size);


    public Catagory findById(String id);

    public void add(Catagory catagory);


    public void update(Catagory catagory);


    public void delete(String id);

}
