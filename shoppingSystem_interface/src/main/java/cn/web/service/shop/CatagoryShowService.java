package cn.web.service.shop;

import cn.web.entity.PageResult;
import cn.web.pojo.shop.CatagoryShow;

import java.util.List;
import java.util.Map;

/**
 * CatagoryShow业务逻辑层
 */
public interface CatagoryShowService {


    public List<CatagoryShow> findAll();


    public PageResult<CatagoryShow> findPage(int page, int size);


    public List<CatagoryShow> findList(Map<String, Object> searchMap);


    public PageResult<CatagoryShow> findPage(Map<String, Object> searchMap, int page, int size);


    public CatagoryShow findById(String id);


    public void add(CatagoryShow catagoryShow);


    public void update(CatagoryShow catagoryShow);


    public void delete(String id);

}
