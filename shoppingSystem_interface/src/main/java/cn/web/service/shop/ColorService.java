package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Color;

import java.util.*;

/**
 * color业务逻辑层
 */
public interface ColorService {


    public List<Color> findAll();


    public PageResult<Color> findPage(int page, int size);


    public List<Color> findList(Map<String,Object> searchMap);


    public PageResult<Color> findPage(Map<String,Object> searchMap,int page, int size);


    public Color findById(String id);

    public void add(Color color);


    public void update(Color color);


    public void delete(String id);

}
