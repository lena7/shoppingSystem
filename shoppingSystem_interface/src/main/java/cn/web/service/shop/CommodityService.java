package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Commodity;

import java.util.*;

/**
 * commodity业务逻辑层
 */
public interface CommodityService {


    public List<Commodity> findAll();


    public PageResult<Commodity> findPage(int page, int size);


    public List<Commodity> findList(Map<String,Object> searchMap);


    public PageResult<Commodity> findPage(Map<String,Object> searchMap,int page, int size);


    public Commodity findById(String id);

    public void add(Commodity commodity);


    public void update(Commodity commodity);


    public void delete(String id);

}
