package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Component;

import java.util.*;

/**
 * component业务逻辑层
 */
public interface ComponentService {


    public List<Component> findAll();


    public PageResult<Component> findPage(int page, int size);


    public List<Component> findList(Map<String,Object> searchMap);


    public PageResult<Component> findPage(Map<String,Object> searchMap,int page, int size);


    public Component findById(String id);

    public void add(Component component);


    public void update(Component component);


    public void delete(String id);

}
