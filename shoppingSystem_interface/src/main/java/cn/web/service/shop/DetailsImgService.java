package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.DetailsImg;

import java.util.*;

/**
 * detailsImg业务逻辑层
 */
public interface DetailsImgService {


    public List<DetailsImg> findAll();


    public PageResult<DetailsImg> findPage(int page, int size);


    public List<DetailsImg> findList(Map<String,Object> searchMap);


    public PageResult<DetailsImg> findPage(Map<String,Object> searchMap,int page, int size);


    public DetailsImg findById(String id);

    public void add(DetailsImg detailsImg);


    public void update(DetailsImg detailsImg);


    public void delete(String id);

}
