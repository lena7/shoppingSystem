package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Details;

import java.util.*;

/**
 * details业务逻辑层
 */
public interface DetailsService {


    public List<Details> findAll();


    public PageResult<Details> findPage(int page, int size);


    public List<Details> findList(Map<String,Object> searchMap);


    public PageResult<Details> findPage(Map<String,Object> searchMap,int page, int size);


    public Details findById(String id);

    public void add(Details details);


    public void update(Details details);


    public void delete(String id);

}
