package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Evaluate;

import java.util.*;

/**
 * evaluate业务逻辑层
 */
public interface EvaluateService {


    public List<Evaluate> findAll();


    public PageResult<Evaluate> findPage(int page, int size);


    public List<Evaluate> findList(Map<String,Object> searchMap);


    public PageResult<Evaluate> findPage(Map<String,Object> searchMap,int page, int size);


    public Evaluate findById(String id);

    public void add(Evaluate evaluate);


    public void update(Evaluate evaluate);


    public void delete(String id);

}
