package cn.web.service.shop;

import cn.web.entity.PageResult;
import cn.web.pojo.shop.GoodsDetails;

import java.util.List;
import java.util.Map;

/**
 * GoodsDetails业务逻辑层
 */
public interface GoodsDetailsService {

    public List<GoodsDetails> findAll();

    public PageResult<GoodsDetails> findPage(int page, int size);

    public List<GoodsDetails> findList(Map<String, Object> searchMap);

    public PageResult<GoodsDetails> findPage(Map<String, Object> searchMap, int page, int size);

    public GoodsDetails findById(String id);

    public void add(GoodsDetails GoodsDetails);

    public void update(GoodsDetails GoodsDetails);

    public void delete(String id);

}
