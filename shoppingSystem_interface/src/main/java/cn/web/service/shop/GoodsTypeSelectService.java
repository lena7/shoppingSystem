package cn.web.service.shop;

import cn.web.entity.PageResult;
import cn.web.pojo.shop.GoodsTypeSelect;

import java.util.List;
import java.util.Map;

public interface GoodsTypeSelectService {

	public List<GoodsTypeSelect> findAll();


	public PageResult<GoodsTypeSelect> findPage(int page, int size);


	public List<GoodsTypeSelect> findList(Map<String,Object> searchMap);


	public PageResult<GoodsTypeSelect> findPage(Map<String,Object> searchMap,int page, int size);


	public GoodsTypeSelect findById(String id);

	public void add(GoodsTypeSelect GoodsTypeSelect);


	public void update(GoodsTypeSelect GoodsTypeSelect);


	public void delete(String id);
}
