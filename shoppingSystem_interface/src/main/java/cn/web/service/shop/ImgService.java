package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Img;

import java.util.*;

/**
 * img业务逻辑层
 */
public interface ImgService {


    public List<Img> findAll();


    public PageResult<Img> findPage(int page, int size);


    public List<Img> findList(Map<String,Object> searchMap);


    public PageResult<Img> findPage(Map<String,Object> searchMap,int page, int size);


    public Img findById(String id);

    public void add(Img img);


    public void update(Img img);


    public void delete(String id);

}
