package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Login;

import java.util.*;

/**
 * login业务逻辑层
 */
public interface LoginService {

    public int Login(String id,String password);

    public List<Login> findAll();


    public PageResult<Login> findPage(int page, int size);


    public List<Login> findList(Map<String,Object> searchMap);


    public PageResult<Login> findPage(Map<String,Object> searchMap,int page, int size);


    public Login findById(String id);

    public void add(Login login);


    public void update(Login login);


    public void delete(String id);

}
