package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Logistics;

import java.util.*;

/**
 * logistics业务逻辑层
 */
public interface LogisticsService {


    public List<Logistics> findAll();


    public PageResult<Logistics> findPage(int page, int size);


    public List<Logistics> findList(Map<String,Object> searchMap);


    public PageResult<Logistics> findPage(Map<String,Object> searchMap,int page, int size);


    public Logistics findById(String id);

    public void add(Logistics logistics);


    public void update(Logistics logistics);


    public void delete(String id);

}
