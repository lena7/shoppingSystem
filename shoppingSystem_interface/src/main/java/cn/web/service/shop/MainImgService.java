package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.MainImg;

import java.util.*;

/**
 * mainImg业务逻辑层
 */
public interface MainImgService {


    public List<MainImg> findAll();


    public PageResult<MainImg> findPage(int page, int size);


    public List<MainImg> findList(Map<String,Object> searchMap);


    public PageResult<MainImg> findPage(Map<String,Object> searchMap,int page, int size);


    public MainImg findById(String id);

    public void add(MainImg mainImg);


    public void update(MainImg mainImg);


    public void delete(String id);

}
