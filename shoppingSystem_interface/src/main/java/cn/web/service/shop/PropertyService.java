package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Property;

import java.util.*;

/**
 * property业务逻辑层
 */
public interface PropertyService {


    public List<Property> findAll();


    public PageResult<Property> findPage(int page, int size);


    public List<Property> findList(Map<String,Object> searchMap);


    public PageResult<Property> findPage(Map<String,Object> searchMap,int page, int size);


    public Property findById(String id);

    public void add(Property property);


    public void update(Property property);


    public void delete(String id);

}
