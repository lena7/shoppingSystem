package cn.web.service.shop;

import cn.web.entity.PageResult;
import cn.web.pojo.shop.ReferalLink;

import java.util.List;

public interface ReferalLinkService {

	public List<ReferalLink> findAll();


	public PageResult<ReferalLink> findPage(int page, int size);


	public ReferalLink findById(String id);

	public void add(ReferalLink ReferalLink);


	public void update(ReferalLink ReferalLink);


	public void delete(String id);
}
