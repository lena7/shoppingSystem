package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Shop;

import java.util.*;

/**
 * shop业务逻辑层
 */
public interface ShopService {


    public List<Shop> findAll();


    public PageResult<Shop> findPage(int page, int size);


    public List<Shop> findList(Map<String,Object> searchMap);


    public PageResult<Shop> findPage(Map<String,Object> searchMap,int page, int size);


    public Shop findById(String id);

    public void add(Shop shop);


    public void update(Shop shop);


    public void delete(String id);

}
