package cn.web.service.shop;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Size;

import java.util.*;

/**
 * size业务逻辑层
 */
public interface SizeService {


    public List<Size> findAll();


    public PageResult<Size> findPage(int page, int size);


    public List<Size> findList(Map<String,Object> searchMap);


    public PageResult<Size> findPage(Map<String,Object> searchMap,int page, int size);


    public Size findById(String id);

    public void add(Size size);


    public void update(Size size);


    public void delete(String id);

}
