package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * account实体类
 * @author Administrator
 *
 */
@Table(name="bank_account")
public class Account implements Serializable{

	@Id
	private String id;//id


	

	private String userId;//用户id

	private String bankAccount;//银行卡号

	private String belongBank;//所属银行

	private Integer type;//卡的类型（储蓄卡1 信用卡0）

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBankAccount() {
		return bankAccount;
	}
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getBelongBank() {
		return belongBank;
	}
	public void setBelongBank(String belongBank) {
		this.belongBank = belongBank;
	}

	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}


	
}
