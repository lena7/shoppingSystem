package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * address实体类
 * @author Administrator
 *
 */
@Table(name="shipping_address")
public class Address implements Serializable{

	@Id
	private String id;//收获地址ID


	

	private String userId;//用户ID

	private String receiverName;//收货人姓名

	private String phoneNumber;//电话

	private String address;//地址

	private Integer isDefault;//是否为默认地址（1是，只能有一个）

	private String remarks;//备注信息

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getReceiverName() {
		return receiverName;
	}
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}


	
}
