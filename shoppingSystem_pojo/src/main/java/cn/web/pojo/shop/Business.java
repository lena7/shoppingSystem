package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * business实体类
 * @author Administrator
 *
 */
@Table(name="business")
public class Business implements Serializable{

	@Id
	private String id;//id

	private String name;//昵称

	private String sex;//性别

	private String email;//邮箱

	private String realName;//真实姓名

	private String idCard;//身份证号

	private String brief;//个签

	private java.util.Date registDate;//注册时间

	private String faceImg;//头像

	private String alipayAccount;//支付宝账号

	private String phoneNumber;//手机号码

	private Integer age;//年龄

	private Integer bond;//缴纳保证金

	private Integer creditRating;//信誉等级（5心->5皇冠）

	private String living;//现居住地

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}

	public java.util.Date getRegistDate() {
		return registDate;
	}
	public void setRegistDate(java.util.Date registDate) {
		this.registDate = registDate;
	}

	public String getFaceImg() {
		return faceImg;
	}
	public void setFaceImg(String faceImg) {
		this.faceImg = faceImg;
	}

	public String getAlipayAccount() {
		return alipayAccount;
	}
	public void setAlipayAccount(String alipayAccount) {
		this.alipayAccount = alipayAccount;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getBond() {
		return bond;
	}
	public void setBond(Integer bond) {
		this.bond = bond;
	}

	public Integer getCreditRating() {
		return creditRating;
	}
	public void setCreditRating(Integer creditRating) {
		this.creditRating = creditRating;
	}

	public String getLiving() {
		return living;
	}
	public void setLiving(String living) {
		this.living = living;
	}


	
}
