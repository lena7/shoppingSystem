package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * catagory实体类
 * @author Administrator
 *
 */
@Table(name="catagory")
public class Catagory implements Serializable{

	@Id
	private String id;//类别ID


	

	private String name;//类别名称

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	
}
