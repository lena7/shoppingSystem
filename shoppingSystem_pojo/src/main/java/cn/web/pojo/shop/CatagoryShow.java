package cn.web.pojo.shop;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="catagory_show")
public class CatagoryShow implements Serializable {

	@Id
	private Integer id;		//id主键自增

	private String name;	//分类名称

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
