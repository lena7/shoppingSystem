package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * color实体类
 * @author Administrator
 *
 */
@Table(name="goods_color")
public class Color implements Serializable{

	@Id
	private String id;//颜色分类ID


	

	private String name;//分类名字

	private String img;//分类图片

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}


	
}
