package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * commodity实体类
 * @author Administrator
 *
 */
@Table(name="order_commodity")
public class Commodity implements Serializable{

	@Id
	private String id;//订单包含商品表ID


	

	private String orderId;//订单ID

	private String color;//颜色

	private String size;//尺码

	private String goodsId;//商品ID

	private Double originalMoney;//原始金额

	private Double realMoney;//最终金额

	private Integer number;//数量

	private Integer status;//当前状态（已付款0 申请退款1 退款中2 退款成功3）

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}

	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public Double getOriginalMoney() {
		return originalMoney;
	}
	public void setOriginalMoney(Double originalMoney) {
		this.originalMoney = originalMoney;
	}

	public Double getRealMoney() {
		return realMoney;
	}
	public void setRealMoney(Double realMoney) {
		this.realMoney = realMoney;
	}

	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}


	
}
