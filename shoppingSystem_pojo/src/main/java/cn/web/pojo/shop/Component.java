package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * component实体类
 * @author Administrator
 *
 */
@Table(name="goods_component")
public class Component implements Serializable{

	@Id
	private String id;//商品成分id

	private String brand;//品牌

	private String fitAge;//适用年龄

	private Integer itemNo;//货号

	private String sentSeason;//上市年份季节

	private String salesChannelType;//销售渠道类型

	private String materialComposition;//材质成分

	private String style;//基础风格

	private String pattern;//图案

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getFitAge() {
		return fitAge;
	}
	public void setFitAge(String fitAge) {
		this.fitAge = fitAge;
	}

	public Integer getItemNo() {
		return itemNo;
	}
	public void setItemNo(Integer itemNo) {
		this.itemNo = itemNo;
	}

	public String getSentSeason() {
		return sentSeason;
	}
	public void setSentSeason(String sentSeason) {
		this.sentSeason = sentSeason;
	}

	public String getSalesChannelType() {
		return salesChannelType;
	}
	public void setSalesChannelType(String salesChannelType) {
		this.salesChannelType = salesChannelType;
	}

	public String getMaterialComposition() {
		return materialComposition;
	}
	public void setMaterialComposition(String materialComposition) {
		this.materialComposition = materialComposition;
	}

	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}

	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}


	
}
