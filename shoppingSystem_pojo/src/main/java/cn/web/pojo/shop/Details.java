package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * details实体类
 * @author Administrator
 *
 */
@Table(name="order_details")
public class Details implements Serializable{

	@Id
	private String id;//订单详情id

	private String orderId;//订单号

	private String goodsId;	//商品id

	private String shopId;//店铺id

	private Double totalPrice;//商品总价

	private Integer freight;//运费险

	private Integer freightInsurance;//是否有运费险（1有0无）

	private Double shopDiscount;//店铺优惠

	private Double luckyMoney;//红包

	private Double realPay;//实付款

	private String remarks;//备注信息

	private String logisticsId;//物流信息ID

	private java.util.Date createTime;//创建时间

	private java.util.Date payTime;//付款时间

	private java.util.Date sentTime;//发货时间

	private java.util.Date dealTime;//成交时间（确认收货时间）

	private String userId;//用户ID

	private String shippingAddressId;//收获地址ID

	private Integer status;//未付款0 已付款1 已发货2 已收货3 已评价4 已追评5 已过期6 申请退款7 退款中8 退款成功9

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getFreight() {
		return freight;
	}
	public void setFreight(Integer freight) {
		this.freight = freight;
	}

	public Integer getFreightInsurance() {
		return freightInsurance;
	}
	public void setFreightInsurance(Integer freightInsurance) {
		this.freightInsurance = freightInsurance;
	}

	public Double getShopDiscount() {
		return shopDiscount;
	}
	public void setShopDiscount(Double shopDiscount) {
		this.shopDiscount = shopDiscount;
	}

	public Double getLuckyMoney() {
		return luckyMoney;
	}
	public void setLuckyMoney(Double luckyMoney) {
		this.luckyMoney = luckyMoney;
	}

	public Double getRealPay() {
		return realPay;
	}
	public void setRealPay(Double realPay) {
		this.realPay = realPay;
	}

	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getLogisticsId() {
		return logisticsId;
	}
	public void setLogisticsId(String logisticsId) {
		this.logisticsId = logisticsId;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}

	public java.util.Date getPayTime() {
		return payTime;
	}
	public void setPayTime(java.util.Date payTime) {
		this.payTime = payTime;
	}

	public java.util.Date getSentTime() {
		return sentTime;
	}
	public void setSentTime(java.util.Date sentTime) {
		this.sentTime = sentTime;
	}

	public java.util.Date getDealTime() {
		return dealTime;
	}
	public void setDealTime(java.util.Date dealTime) {
		this.dealTime = dealTime;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(String shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}


	
}
