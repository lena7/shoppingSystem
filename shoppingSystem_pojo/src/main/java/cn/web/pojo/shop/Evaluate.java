package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * evaluate实体类
 * @author Administrator
 *
 */
@Table(name="evaluate")
public class Evaluate implements Serializable{

	@Id
	private String id;//评价ID


	

	private String orderId;//订单ID

	private String goodsId;//商品ID

	private String content;//评价内容

	private Integer isAnonymouns;//是否匿名

	private Integer grade;//评价等级（好评1 中评2 差评3）

	private Integer describeFit;//描述相符（1-5）

	private Integer logisticsService;//物流服务（1-5）

	private Integer serviceAttitude;//服务态度（1-5）

	private Integer isImg;//是否有图片（0无 1有）

	private java.util.Date date;//评价日期

	private String userId;//用户id

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public Integer getIsAnonymouns() {
		return isAnonymouns;
	}
	public void setIsAnonymouns(Integer isAnonymouns) {
		this.isAnonymouns = isAnonymouns;
	}

	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Integer getDescribeFit() {
		return describeFit;
	}
	public void setDescribeFit(Integer describeFit) {
		this.describeFit = describeFit;
	}

	public Integer getLogisticsService() {
		return logisticsService;
	}
	public void setLogisticsService(Integer logisticsService) {
		this.logisticsService = logisticsService;
	}

	public Integer getServiceAttitude() {
		return serviceAttitude;
	}
	public void setServiceAttitude(Integer serviceAttitude) {
		this.serviceAttitude = serviceAttitude;
	}

	public Integer getIsImg() {
		return isImg;
	}
	public void setIsImg(Integer isImg) {
		this.isImg = isImg;
	}

	public java.util.Date getDate() {
		return date;
	}
	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}


	
}
