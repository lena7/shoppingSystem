package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * goods实体类
 * @author Administrator
 *
 */
@Table(name="goods")
public class Goods implements Serializable{

	@Id
	private String id;//商品id

	private String mainImgId;//商品主图id

	private String detailsId;//商品详情id

	private String detailsImgId;//商品详情页图片id

	private String catagoryId;//商品所属类别id

	private String componentId;//商品成分id

	private String typeSelectId;//商品类型选择id

	private String shopId;//所属店铺id

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getMainImgId() {
		return mainImgId;
	}
	public void setMainImgId(String mainImgId) {
		this.mainImgId = mainImgId;
	}

	public String getDetailsId() {
		return detailsId;
	}
	public void setDetailsId(String detailsId) {
		this.detailsId = detailsId;
	}

	public String getDetailsImgId() {
		return detailsImgId;
	}
	public void setDetailsImgId(String detailsImgId) {
		this.detailsImgId = detailsImgId;
	}

	public String getCatagoryId() {
		return catagoryId;
	}
	public void setCatagoryId(String catagoryId) {
		this.catagoryId = catagoryId;
	}

	public String getComponentId() {
		return componentId;
	}
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getTypeSelectId() {
		return typeSelectId;
	}
	public void setTypeSelectId(String typeSelectId) {
		this.typeSelectId = typeSelectId;
	}

	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}


	
}
