package cn.web.pojo.shop;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="goods_details")
public class GoodsDetails  implements Serializable {

	@Id
	private String id;//商品详情页id

	private String describegoods;//商品描述

	private String title;//商品标题

	private Integer recommendNumber;//推荐人数

	private Integer salesMonth;//月销量

	private Double lowPrice;//最低价格

	private Double highPrice;//最高价格

	private String deliveryPlace;//发货地

	private Integer express;//快递（0为免运费）

	private String guarantee;//保障

	private String titleShow;//小标题（显示标题）

	private Integer stock;//库存

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescribegoods() {
		return describegoods;
	}

	public void setDescribegoods(String describegoods) {
		this.describegoods = describegoods;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getRecommendNumber() {
		return recommendNumber;
	}

	public void setRecommendNumber(Integer recommendNumber) {
		this.recommendNumber = recommendNumber;
	}

	public Integer getSalesMonth() {
		return salesMonth;
	}

	public void setSalesMonth(Integer salesMonth) {
		this.salesMonth = salesMonth;
	}

	public Double getLowPrice() {
		return lowPrice;
	}

	public void setLowPrice(Double lowPrice) {
		this.lowPrice = lowPrice;
	}

	public Double getHighPrice() {
		return highPrice;
	}

	public void setHighPrice(Double highPrice) {
		this.highPrice = highPrice;
	}

	public String getDeliveryPlace() {
		return deliveryPlace;
	}

	public void setDeliveryPlace(String deliveryPlace) {
		this.deliveryPlace = deliveryPlace;
	}

	public Integer getExpress() {
		return express;
	}

	public void setExpress(Integer express) {
		this.express = express;
	}

	public String getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(String guarantee) {
		this.guarantee = guarantee;
	}

	public String getTitleShow() {
		return titleShow;
	}

	public void setTitleShow(String titleShow) {
		this.titleShow = titleShow;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}
}
