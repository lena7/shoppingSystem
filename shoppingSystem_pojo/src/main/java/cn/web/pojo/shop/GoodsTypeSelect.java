package cn.web.pojo.shop;

import javax.persistence.Table;
import java.io.Serializable;

@Table(name="goods_type_select")
public class GoodsTypeSelect  implements Serializable {

	private String id;		//不是主键！分类尺码关联表ID

	private String colorId;	//颜色分类ID

	private String sizeId;	//尺码ID

	private Double money;	//当前颜色尺码的价格

	private Integer stock;	//库存

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getColorId() {
		return colorId;
	}

	public void setColorId(String colorId) {
		this.colorId = colorId;
	}

	public String getSizeId() {
		return sizeId;
	}

	public void setSizeId(String sizeId) {
		this.sizeId = sizeId;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}
}
