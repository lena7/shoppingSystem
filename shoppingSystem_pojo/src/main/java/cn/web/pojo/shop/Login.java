package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * login实体类
 * @author Administrator
 *
 */
@Table(name="login")
public class Login implements Serializable{

	@Id
	private String id;//登录账号

	private String password;//登录密码

	private Integer status;//状态（用户1 商家2 店铺3 管理员4）

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
