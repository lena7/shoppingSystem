package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * logistics实体类
 * @author Administrator
 *
 */
@Table(name="logistics")
public class Logistics implements Serializable{

	@Id
	private String id;//物流号ID


	

	private String sentAddress;//发货地址

	private String receiveAddress;//收获地址

	private java.util.Date sentTime;//发货时间

	private java.util.Date pickupTime;//揽件时间

	private String pickupPhone;//揽件快递员电话

	private String nowPosition;//当前运输位置

	private String deliverer;//派送人

	private String deliveryPhone;//派送人联系方式

	private String signer;//签收人

	private Integer status;//当前状态（未发货0 发货1 揽件2 运输3 派送4 签收5）

	private String expressCompany;//快递公司（如：顺丰快递）

	private Integer deliveryEvaluate;//快递员评价等级（1-5）

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getSentAddress() {
		return sentAddress;
	}
	public void setSentAddress(String sentAddress) {
		this.sentAddress = sentAddress;
	}

	public String getReceiveAddress() {
		return receiveAddress;
	}
	public void setReceiveAddress(String receiveAddress) {
		this.receiveAddress = receiveAddress;
	}

	public java.util.Date getSentTime() {
		return sentTime;
	}
	public void setSentTime(java.util.Date sentTime) {
		this.sentTime = sentTime;
	}

	public java.util.Date getPickupTime() {
		return pickupTime;
	}
	public void setPickupTime(java.util.Date pickupTime) {
		this.pickupTime = pickupTime;
	}

	public String getPickupPhone() {
		return pickupPhone;
	}
	public void setPickupPhone(String pickupPhone) {
		this.pickupPhone = pickupPhone;
	}

	public String getNowPosition() {
		return nowPosition;
	}
	public void setNowPosition(String nowPosition) {
		this.nowPosition = nowPosition;
	}

	public String getDeliverer() {
		return deliverer;
	}
	public void setDeliverer(String deliverer) {
		this.deliverer = deliverer;
	}

	public String getDeliveryPhone() {
		return deliveryPhone;
	}
	public void setDeliveryPhone(String deliveryPhone) {
		this.deliveryPhone = deliveryPhone;
	}

	public String getSigner() {
		return signer;
	}
	public void setSigner(String signer) {
		this.signer = signer;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getExpressCompany() {
		return expressCompany;
	}
	public void setExpressCompany(String expressCompany) {
		this.expressCompany = expressCompany;
	}

	public Integer getDeliveryEvaluate() {
		return deliveryEvaluate;
	}
	public void setDeliveryEvaluate(Integer deliveryEvaluate) {
		this.deliveryEvaluate = deliveryEvaluate;
	}


	
}
