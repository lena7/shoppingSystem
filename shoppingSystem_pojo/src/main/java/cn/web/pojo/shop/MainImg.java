package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * mainImg实体类
 * @author Administrator
 *
 */
@Table(name="goods_main_img")
public class MainImg implements Serializable{

	@Id
	private String id;//id

	private String goodsId;//商品id（有点多余）

	private Integer imgNumber;//主图数量

	private String img1;//主图1

	private String img2;//主图2

	private String img3;//主图3

	private String img4;//主图4

	private String img5;//主图5

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public Integer getImgNumber() {
		return imgNumber;
	}
	public void setImgNumber(Integer imgNumber) {
		this.imgNumber = imgNumber;
	}

	public String getImg1() {
		return img1;
	}
	public void setImg1(String img1) {
		this.img1 = img1;
	}

	public String getImg2() {
		return img2;
	}
	public void setImg2(String img2) {
		this.img2 = img2;
	}

	public String getImg3() {
		return img3;
	}
	public void setImg3(String img3) {
		this.img3 = img3;
	}

	public String getImg4() {
		return img4;
	}
	public void setImg4(String img4) {
		this.img4 = img4;
	}

	public String getImg5() {
		return img5;
	}
	public void setImg5(String img5) {
		this.img5 = img5;
	}


	
}
