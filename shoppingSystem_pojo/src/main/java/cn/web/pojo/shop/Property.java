package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * property实体类
 * @author Administrator
 *
 */
@Table(name="order_property")
public class Property implements Serializable{

	@Id
	private String id;//商品基本属性id（管理员维护该表）

	private String name;//属性名

	private Integer isRequired;//是否必填（0否1是）

	private String catagoryId;//所属分类id

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsRequired() {
		return isRequired;
	}
	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}

	public String getCatagoryId() {
		return catagoryId;
	}
	public void setCatagoryId(String catagoryId) {
		this.catagoryId = catagoryId;
	}


	
}
