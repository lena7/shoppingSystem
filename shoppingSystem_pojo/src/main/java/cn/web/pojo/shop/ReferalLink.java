package cn.web.pojo.shop;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Table(name="referal_link")
public class ReferalLink  implements Serializable {
	@Id
	private String id;//外部网站ID




	private String name;//外部网站名称

	private String url;//外部网站访问路径


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
