package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * shop实体类
 * @author Administrator
 *
 */
@Table(name="shop")
public class Shop implements Serializable{

	@Id
	private String id;//店铺ID

	private String businessId;//商家ID

	private String name;//店铺名

	private String qualification;//资质

	private String enterpriseQualification;//企业资质

	private String cardImg;//店铺名片（二维码）

	private java.util.Date openDate;//开店日期

	private Double feedbackRate;//店铺好评率

	private Double describeFit;//描述相符

	private Double serviceAttitude;//服务态度

	private Double logisticsService;//物流服务

	private Double comprehensiveExperience;//综合体验

	private Integer fansNumber;//粉丝数

	private Integer level;//当前店铺等级

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getQualification() {
		return qualification;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getEnterpriseQualification() {
		return enterpriseQualification;
	}
	public void setEnterpriseQualification(String enterpriseQualification) {
		this.enterpriseQualification = enterpriseQualification;
	}

	public String getCardImg() {
		return cardImg;
	}
	public void setCardImg(String cardImg) {
		this.cardImg = cardImg;
	}

	public java.util.Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(java.util.Date openDate) {
		this.openDate = openDate;
	}

	public Double getFeedbackRate() {
		return feedbackRate;
	}
	public void setFeedbackRate(Double feedbackRate) {
		this.feedbackRate = feedbackRate;
	}

	public Double getDescribeFit() {
		return describeFit;
	}
	public void setDescribeFit(Double describeFit) {
		this.describeFit = describeFit;
	}

	public Double getServiceAttitude() {
		return serviceAttitude;
	}
	public void setServiceAttitude(Double serviceAttitude) {
		this.serviceAttitude = serviceAttitude;
	}

	public Double getLogisticsService() {
		return logisticsService;
	}
	public void setLogisticsService(Double logisticsService) {
		this.logisticsService = logisticsService;
	}

	public Double getComprehensiveExperience() {
		return comprehensiveExperience;
	}
	public void setComprehensiveExperience(Double comprehensiveExperience) {
		this.comprehensiveExperience = comprehensiveExperience;
	}

	public Integer getFansNumber() {
		return fansNumber;
	}
	public void setFansNumber(Integer fansNumber) {
		this.fansNumber = fansNumber;
	}

	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}


	
}
