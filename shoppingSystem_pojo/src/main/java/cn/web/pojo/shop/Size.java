package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * size实体类
 * @author Administrator
 *
 */
@Table(name="goods_size")
public class Size implements Serializable{

	@Id
	private String id;//尺码ID

	private String name;//尺码名
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	
}
