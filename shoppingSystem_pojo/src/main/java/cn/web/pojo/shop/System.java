package cn.web.pojo.shop;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
/**
 * system实体类
 * @author Administrator
 *
 */
@Table(name="system")
public class System implements Serializable{

	@Id
	private String id;//管理员id


	

	private String realName;//真实姓名

	private String sex;//性别

	private String phoneNumber;//电话号码

	private String living;//居住地

	private java.util.Date joinDate;//入职日期

	private String idCard;//身份证

	private String email;//邮箱

	private Integer age;//年龄

	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLiving() {
		return living;
	}
	public void setLiving(String living) {
		this.living = living;
	}

	public java.util.Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(java.util.Date joinDate) {
		this.joinDate = joinDate;
	}

	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}


	
}
