package cn.web.pojo.vo;

import cn.web.pojo.shop.GoodsDetails;

public class GoodsDetailsImg {

	private GoodsDetails goodsDetails;		//商品详情
	private String img1;	//主图

	public GoodsDetails getGoodsDetails() {
		return goodsDetails;
	}

	public void setGoodsDetails(GoodsDetails goodsDetails) {
		this.goodsDetails = goodsDetails;
	}

	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}
}
