package cn.web.pojo.vo;

public class PropertyValue {

	private String name;//属性名

	private String brand;//品牌

	private String fitAge;//适用年龄

	private Integer itemNo;//货号

	private String sentSeason;//上市年份季节

	private String salesChannelType;//销售渠道类型

	private String materialComposition;//材质成分

	private String style;//基础风格

	private String pattern;//图案



}
