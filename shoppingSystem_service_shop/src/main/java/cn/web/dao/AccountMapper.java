package cn.web.dao;

import cn.web.pojo.shop.Account;
import tk.mybatis.mapper.common.Mapper;

public interface AccountMapper extends Mapper<Account> {

}
