package cn.web.dao;

import cn.web.pojo.shop.Address;
import tk.mybatis.mapper.common.Mapper;

public interface AddressMapper extends Mapper<Address> {

}
