package cn.web.dao;

import cn.web.pojo.shop.Bill;
import tk.mybatis.mapper.common.Mapper;

public interface BillMapper extends Mapper<Bill> {

}
