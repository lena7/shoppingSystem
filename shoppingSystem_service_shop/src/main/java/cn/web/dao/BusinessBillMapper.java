package cn.web.dao;

import cn.web.pojo.shop.BusinessBill;
import tk.mybatis.mapper.common.Mapper;

public interface BusinessBillMapper  extends Mapper<BusinessBill> {
}
