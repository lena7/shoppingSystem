package cn.web.dao;

import cn.web.pojo.shop.Business;
import tk.mybatis.mapper.common.Mapper;

public interface BusinessMapper extends Mapper<Business> {

}
