package cn.web.dao;

import cn.web.pojo.shop.Cart;
import tk.mybatis.mapper.common.Mapper;

public interface CartMapper extends Mapper<Cart> {

}
