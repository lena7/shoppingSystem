package cn.web.dao;

import cn.web.pojo.shop.Color;
import tk.mybatis.mapper.common.Mapper;

public interface ColorMapper extends Mapper<Color> {

}
