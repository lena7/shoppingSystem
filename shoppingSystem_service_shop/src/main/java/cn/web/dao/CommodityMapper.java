package cn.web.dao;

import cn.web.pojo.shop.Commodity;
import tk.mybatis.mapper.common.Mapper;

public interface CommodityMapper extends Mapper<Commodity> {

}
