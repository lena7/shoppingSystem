package cn.web.dao;

import cn.web.pojo.shop.Component;
import tk.mybatis.mapper.common.Mapper;

public interface ComponentMapper extends Mapper<Component> {

}
