package cn.web.dao;

import cn.web.pojo.shop.DetailsImg;
import tk.mybatis.mapper.common.Mapper;

public interface DetailsImgMapper extends Mapper<DetailsImg> {

}
