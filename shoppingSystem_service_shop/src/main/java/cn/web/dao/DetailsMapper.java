package cn.web.dao;

import cn.web.pojo.shop.Details;
import tk.mybatis.mapper.common.Mapper;

public interface DetailsMapper extends Mapper<Details> {

}
