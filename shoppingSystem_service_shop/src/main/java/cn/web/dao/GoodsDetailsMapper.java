package cn.web.dao;

import cn.web.pojo.shop.GoodsDetails;
import tk.mybatis.mapper.common.Mapper;

public interface GoodsDetailsMapper  extends Mapper<GoodsDetails> {
}
