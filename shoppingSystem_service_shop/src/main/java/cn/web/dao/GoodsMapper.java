package cn.web.dao;

import cn.web.pojo.shop.Goods;
import tk.mybatis.mapper.common.Mapper;

public interface GoodsMapper extends Mapper<Goods> {

}
