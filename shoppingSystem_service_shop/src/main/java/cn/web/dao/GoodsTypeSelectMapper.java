package cn.web.dao;

import cn.web.pojo.shop.GoodsTypeSelect;
import tk.mybatis.mapper.common.Mapper;

public interface GoodsTypeSelectMapper extends Mapper<GoodsTypeSelect> {
}
