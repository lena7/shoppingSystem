package cn.web.dao;

import cn.web.pojo.shop.Login;
import tk.mybatis.mapper.common.Mapper;

public interface LoginMapper extends Mapper<Login> {

}
