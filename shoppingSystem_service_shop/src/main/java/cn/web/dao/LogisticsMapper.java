package cn.web.dao;

import cn.web.pojo.shop.Logistics;
import tk.mybatis.mapper.common.Mapper;

public interface LogisticsMapper extends Mapper<Logistics> {

}
