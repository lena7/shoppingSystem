package cn.web.dao;

import cn.web.pojo.shop.ReferalLink;
import tk.mybatis.mapper.common.Mapper;

public interface ReferalLinkMapper extends Mapper<ReferalLink> {
}
