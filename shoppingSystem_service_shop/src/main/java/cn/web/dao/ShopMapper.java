package cn.web.dao;

import cn.web.pojo.shop.Shop;
import tk.mybatis.mapper.common.Mapper;

public interface ShopMapper extends Mapper<Shop> {

}
