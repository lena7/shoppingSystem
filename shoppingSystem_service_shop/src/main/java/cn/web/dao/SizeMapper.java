package cn.web.dao;

import cn.web.pojo.shop.Size;
import tk.mybatis.mapper.common.Mapper;

public interface SizeMapper extends Mapper<Size> {

}
