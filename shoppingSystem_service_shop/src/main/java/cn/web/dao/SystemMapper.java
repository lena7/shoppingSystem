package cn.web.dao;

import cn.web.pojo.shop.System;
import tk.mybatis.mapper.common.Mapper;

public interface SystemMapper extends Mapper<System> {

}
