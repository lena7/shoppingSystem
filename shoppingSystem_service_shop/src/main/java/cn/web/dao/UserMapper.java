package cn.web.dao;

import cn.web.pojo.shop.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {

}
