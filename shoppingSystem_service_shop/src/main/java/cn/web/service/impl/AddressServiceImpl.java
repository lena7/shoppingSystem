package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.AddressMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Address;
import cn.web.service.shop.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressMapper addressMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Address> findAll() {
        return addressMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Address> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Address> addresss = (Page<Address>) addressMapper.selectAll();
        return new PageResult<Address>(addresss.getTotal(),addresss.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Address> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return addressMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Address> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Address> addresss = (Page<Address>) addressMapper.selectByExample(example);
        return new PageResult<Address>(addresss.getTotal(),addresss.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Address findById(String id) {
        return addressMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param address
     */
    public void add(Address address) {
        addressMapper.insert(address);
    }

    /**
     * 修改
     * @param address
     */
    public void update(Address address) {
        addressMapper.updateByPrimaryKeySelective(address);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        addressMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Address.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 收获地址ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 用户ID
            if(searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))){
                criteria.andLike("userId","%"+searchMap.get("userId")+"%");
            }
            // 收货人姓名
            if(searchMap.get("receiverName")!=null && !"".equals(searchMap.get("receiverName"))){
                criteria.andLike("receiverName","%"+searchMap.get("receiverName")+"%");
            }
            // 电话
            if(searchMap.get("phoneNumber")!=null && !"".equals(searchMap.get("phoneNumber"))){
                criteria.andLike("phoneNumber","%"+searchMap.get("phoneNumber")+"%");
            }
            // 地址
            if(searchMap.get("address")!=null && !"".equals(searchMap.get("address"))){
                criteria.andLike("address","%"+searchMap.get("address")+"%");
            }
            // 备注信息
            if(searchMap.get("remarks")!=null && !"".equals(searchMap.get("remarks"))){
                criteria.andLike("remarks","%"+searchMap.get("remarks")+"%");
            }

            // 是否为默认地址（1是，只能有一个）
            if(searchMap.get("isDefault")!=null ){
                criteria.andEqualTo("isDefault",searchMap.get("isDefault"));
            }

        }
        return example;
    }

}
