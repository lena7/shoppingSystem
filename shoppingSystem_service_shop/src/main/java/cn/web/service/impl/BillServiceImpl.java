package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.BillMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Bill;
import cn.web.service.shop.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class BillServiceImpl implements BillService {

    @Autowired
    private BillMapper billMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Bill> findAll() {
        return billMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Bill> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Bill> bills = (Page<Bill>) billMapper.selectAll();
        return new PageResult<Bill>(bills.getTotal(),bills.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Bill> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return billMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Bill> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Bill> bills = (Page<Bill>) billMapper.selectByExample(example);
        return new PageResult<Bill>(bills.getTotal(),bills.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Bill findById(String id) {
        return billMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param bill
     */
    public void add(Bill bill) {
        billMapper.insert(bill);
    }

    /**
     * 修改
     * @param bill
     */
    public void update(Bill bill) {
        billMapper.updateByPrimaryKeySelective(bill);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        billMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Bill.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 账单id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 用户id
            if(searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))){
                criteria.andLike("userId","%"+searchMap.get("userId")+"%");
            }
            // 商家id
            if(searchMap.get("businessId")!=null && !"".equals(searchMap.get("businessId"))){
                criteria.andLike("businessId","%"+searchMap.get("businessId")+"%");
            }
            // 订单id
            if(searchMap.get("orderId")!=null && !"".equals(searchMap.get("orderId"))){
                criteria.andLike("orderId","%"+searchMap.get("orderId")+"%");
            }
            // 支付方式
            if(searchMap.get("payMode")!=null && !"".equals(searchMap.get("payMode"))){
                criteria.andLike("payMode","%"+searchMap.get("payMode")+"%");
            }
            // 账单分类
            if(searchMap.get("billType")!=null && !"".equals(searchMap.get("billType"))){
                criteria.andLike("billType","%"+searchMap.get("billType")+"%");
            }
            // 商品说明
            if(searchMap.get("goodsDescribe")!=null && !"".equals(searchMap.get("goodsDescribe"))){
                criteria.andLike("goodsDescribe","%"+searchMap.get("goodsDescribe")+"%");
            }
            // 商家联系电话
            if(searchMap.get("businessPhone")!=null && !"".equals(searchMap.get("businessPhone"))){
                criteria.andLike("businessPhone","%"+searchMap.get("businessPhone")+"%");
            }
            // 备注信息
            if(searchMap.get("remarks")!=null && !"".equals(searchMap.get("remarks"))){
                criteria.andLike("remarks","%"+searchMap.get("remarks")+"%");
            }

            // 交易状态（0正常1异常）
            if(searchMap.get("status")!=null ){
                criteria.andEqualTo("status",searchMap.get("status"));
            }

        }
        return example;
    }

}
