package cn.web.service.impl;

import cn.web.dao.BusinessBillMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Account;
import cn.web.pojo.shop.BusinessBill;
import cn.web.service.shop.BusinessBillService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
@Service
public class BusinessBillServiceImpl implements BusinessBillService {

	@Autowired
	private BusinessBillMapper businessBillMapper;

	/**
	 * 返回全部记录
	 * @return
	 */
	public List<BusinessBill> findAll() {
		return businessBillMapper.selectAll();
	}

	/**
	 * 分页查询
	 * @param page 页码
	 * @param size 每页记录数
	 * @return 分页结果
	 */
	public PageResult<BusinessBill> findPage(int page, int size) {
		PageHelper.startPage(page,size);
		Page<BusinessBill> accounts = (Page<BusinessBill>) businessBillMapper.selectAll();
		return new PageResult<BusinessBill>(accounts.getTotal(),accounts.getResult());
	}

	/**
	 * 条件查询
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<BusinessBill> findList(Map<String, Object> searchMap) {
		Example example = createExample(searchMap);
		return businessBillMapper.selectByExample(example);
	}

	/**
	 * 分页+条件查询
	 * @param searchMap
	 * @param page
	 * @param size
	 * @return
	 */
	public PageResult<BusinessBill> findPage(Map<String, Object> searchMap, int page, int size) {
		PageHelper.startPage(page,size);
		Example example = createExample(searchMap);
		Page<BusinessBill> accounts = (Page<BusinessBill>) businessBillMapper.selectByExample(example);
		return new PageResult<BusinessBill>(accounts.getTotal(),accounts.getResult());
	}

	/**
	 * 根据Id查询
	 * @param id
	 * @return
	 */
	public BusinessBill findById(String id) {
		return businessBillMapper.selectByPrimaryKey(id);
	}


	public void add(BusinessBill businessBill) {
		businessBillMapper.insert(businessBill);
	}

	/**
	 * 修改
	 * @param businessBill
	 */
	public void update(BusinessBill businessBill) {
		businessBillMapper.updateByPrimaryKeySelective(businessBill);
	}

	/**
	 *  删除
	 * @param id
	 */
	public void delete(String id) {
		businessBillMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 构建查询条件
	 * @param searchMap
	 * @return
	 */
	private Example createExample(Map<String, Object> searchMap){
		Example example=new Example(Account.class);
		Example.Criteria criteria = example.createCriteria();
		if(searchMap!=null){
			// id
			if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
				criteria.andLike("id","%"+searchMap.get("id")+"%");
			}
			// 用户id
			if(searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))){
				criteria.andLike("userId","%"+searchMap.get("userId")+"%");
			}
			// 商家id
			if(searchMap.get("businessId")!=null && !"".equals(searchMap.get("businessId"))){
				criteria.andLike("businessId","%"+searchMap.get("businessId")+"%");
			}
			// 订单ID
			if(searchMap.get("orderId")!=null && !"".equals(searchMap.get("orderId"))){
				criteria.andLike("orderId","%"+searchMap.get("orderId")+"%");
			}
			// 交易状态
			if(searchMap.get("status")!=null ){
				criteria.andEqualTo("status",searchMap.get("status"));
			}
			// 收款方式
			if(searchMap.get("receiveMode")!=null && !"".equals(searchMap.get("receiveMode"))){
				criteria.andLike("receiveMode","%"+searchMap.get("receiveMode")+"%");
			}
		}
		return example;
	}
}
