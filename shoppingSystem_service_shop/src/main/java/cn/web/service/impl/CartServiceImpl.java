package cn.web.service.impl;
import cn.web.dao.CartMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Cart;
import cn.web.service.shop.CartService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartMapper cartMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Cart> findAll() {
        return cartMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Cart> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Cart> carts = (Page<Cart>) cartMapper.selectAll();
        return new PageResult<Cart>(carts.getTotal(),carts.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Cart> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return cartMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Cart> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Cart> carts = (Page<Cart>) cartMapper.selectByExample(example);
        return new PageResult<Cart>(carts.getTotal(),carts.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Cart findById(String id) {
        return cartMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param cart
     */
    public void add(Cart cart) {
        cartMapper.insert(cart);
    }

    /**
     * 修改
     * @param cart
     */
    public void update(Cart cart) {
        cartMapper.updateByPrimaryKeySelective(cart);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        cartMapper.deleteByPrimaryKey(id);
    }

    /**
     * 查询购物车列表
     */
    public List<Cart> selectList(List<String> goodsId){
        Example example=new Example(Cart.class);
        Example.Criteria criteria = example.createCriteria();
        List<Cart> carts = cartMapper.selectByExample(criteria.andIn("goods_id", goodsId));
        return carts;
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Cart.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 用户购物车ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 用户ID
            if(searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))){
                criteria.andLike("userId","%"+searchMap.get("userId")+"%");
            }
            // 商品ID
            if(searchMap.get("goodsId")!=null && !"".equals(searchMap.get("goodsId"))){
                criteria.andLike("goodsId","%"+searchMap.get("goodsId")+"%");
            }
            // 颜色分类
            if(searchMap.get("color")!=null && !"".equals(searchMap.get("color"))){
                criteria.andLike("color","%"+searchMap.get("color")+"%");
            }
            // 尺码
            if(searchMap.get("size")!=null && !"".equals(searchMap.get("size"))){
                criteria.andLike("size","%"+searchMap.get("size")+"%");
            }

            // 数量
            if(searchMap.get("number")!=null ){
                criteria.andEqualTo("number",searchMap.get("number"));
            }

        }
        return example;
    }

}
