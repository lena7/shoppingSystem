package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.CatagoryMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Catagory;
import cn.web.service.shop.CatagoryService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CatagoryServiceImpl implements CatagoryService {

    @Autowired
    private CatagoryMapper catagoryMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Catagory> findAll() {
        return catagoryMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Catagory> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Catagory> catagorys = (Page<Catagory>) catagoryMapper.selectAll();
        return new PageResult<Catagory>(catagorys.getTotal(),catagorys.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Catagory> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return catagoryMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Catagory> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Catagory> catagorys = (Page<Catagory>) catagoryMapper.selectByExample(example);
        return new PageResult<Catagory>(catagorys.getTotal(),catagorys.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Catagory findById(String id) {
        return catagoryMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param catagory
     */
    public void add(Catagory catagory) {
        catagoryMapper.insert(catagory);
    }

    /**
     * 修改
     * @param catagory
     */
    public void update(Catagory catagory) {
        catagoryMapper.updateByPrimaryKeySelective(catagory);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        catagoryMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Catagory.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 类别ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 类别名称
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }


        }
        return example;
    }

}
