package cn.web.service.impl;

import cn.web.dao.CatagoryShowMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.CatagoryShow;
import cn.web.service.shop.CatagoryShowService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CatagoryShowServiceImpl implements CatagoryShowService {

    @Autowired
    private CatagoryShowMapper catagoryShowMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<CatagoryShow> findAll() {
        return catagoryShowMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<CatagoryShow> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<CatagoryShow> CatagoryShows = (Page<CatagoryShow>) catagoryShowMapper.selectAll();
        return new PageResult<CatagoryShow>(CatagoryShows.getTotal(),CatagoryShows.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<CatagoryShow> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return catagoryShowMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<CatagoryShow> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<CatagoryShow> CatagoryShows = (Page<CatagoryShow>) catagoryShowMapper.selectByExample(example);
        return new PageResult<CatagoryShow>(CatagoryShows.getTotal(),CatagoryShows.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public CatagoryShow findById(String id) {
        return catagoryShowMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param catagoryShow
     */
    public void add(CatagoryShow catagoryShow) {
        catagoryShowMapper.insert(catagoryShow);
    }

    /**
     * 修改
     * @param catagoryShow
     */
    public void update(CatagoryShow catagoryShow) {
        catagoryShowMapper.updateByPrimaryKeySelective(catagoryShow);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        catagoryShowMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(CatagoryShow.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // id
            if(searchMap.get("id")!=null ){
                criteria.andEqualTo("id",searchMap.get("id"));
            }
            // 分类名
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
        }
        return example;
    }

}
