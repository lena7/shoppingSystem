package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.ColorMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Color;
import cn.web.service.shop.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ColorServiceImpl implements ColorService {

    @Autowired
    private ColorMapper colorMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Color> findAll() {
        return colorMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Color> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Color> colors = (Page<Color>) colorMapper.selectAll();
        return new PageResult<Color>(colors.getTotal(),colors.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Color> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return colorMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Color> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Color> colors = (Page<Color>) colorMapper.selectByExample(example);
        return new PageResult<Color>(colors.getTotal(),colors.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Color findById(String id) {
        return colorMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param color
     */
    public void add(Color color) {
        colorMapper.insert(color);
    }

    /**
     * 修改
     * @param color
     */
    public void update(Color color) {
        colorMapper.updateByPrimaryKeySelective(color);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        colorMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Color.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 颜色分类ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 分类名字
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 分类图片
            if(searchMap.get("img")!=null && !"".equals(searchMap.get("img"))){
                criteria.andLike("img","%"+searchMap.get("img")+"%");
            }


        }
        return example;
    }

}
