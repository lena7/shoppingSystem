package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.CommodityMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Commodity;
import cn.web.service.shop.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class CommodityServiceImpl implements CommodityService {

    @Autowired
    private CommodityMapper commodityMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Commodity> findAll() {
        return commodityMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Commodity> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Commodity> commoditys = (Page<Commodity>) commodityMapper.selectAll();
        return new PageResult<Commodity>(commoditys.getTotal(),commoditys.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Commodity> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return commodityMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Commodity> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Commodity> commoditys = (Page<Commodity>) commodityMapper.selectByExample(example);
        return new PageResult<Commodity>(commoditys.getTotal(),commoditys.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Commodity findById(String id) {
        return commodityMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param commodity
     */
    public void add(Commodity commodity) {
        commodityMapper.insert(commodity);
    }

    /**
     * 修改
     * @param commodity
     */
    public void update(Commodity commodity) {
        commodityMapper.updateByPrimaryKeySelective(commodity);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        commodityMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Commodity.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 订单包含商品表ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 订单ID
            if(searchMap.get("orderId")!=null && !"".equals(searchMap.get("orderId"))){
                criteria.andLike("orderId","%"+searchMap.get("orderId")+"%");
            }
            // 颜色
            if(searchMap.get("color")!=null && !"".equals(searchMap.get("color"))){
                criteria.andLike("color","%"+searchMap.get("color")+"%");
            }
            // 尺码
            if(searchMap.get("size")!=null && !"".equals(searchMap.get("size"))){
                criteria.andLike("size","%"+searchMap.get("size")+"%");
            }
            // 商品ID
            if(searchMap.get("goodsId")!=null && !"".equals(searchMap.get("goodsId"))){
                criteria.andLike("goodsId","%"+searchMap.get("goodsId")+"%");
            }

            // 数量
            if(searchMap.get("number")!=null ){
                criteria.andEqualTo("number",searchMap.get("number"));
            }
            // 当前状态（已付款0 申请退款1 退款中2 退款成功3）
            if(searchMap.get("status")!=null ){
                criteria.andEqualTo("status",searchMap.get("status"));
            }

        }
        return example;
    }

}
