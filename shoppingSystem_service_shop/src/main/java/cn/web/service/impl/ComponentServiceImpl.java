package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.ComponentMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Component;
import cn.web.service.shop.ComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ComponentServiceImpl implements ComponentService {

    @Autowired
    private ComponentMapper componentMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Component> findAll() {
        return componentMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Component> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Component> components = (Page<Component>) componentMapper.selectAll();
        return new PageResult<Component>(components.getTotal(),components.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Component> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return componentMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Component> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Component> components = (Page<Component>) componentMapper.selectByExample(example);
        return new PageResult<Component>(components.getTotal(),components.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Component findById(String id) {
        return componentMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param component
     */
    public void add(Component component) {
        componentMapper.insert(component);
    }

    /**
     * 修改
     * @param component
     */
    public void update(Component component) {
        componentMapper.updateByPrimaryKeySelective(component);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        componentMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Component.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 商品成分id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 品牌
            if(searchMap.get("brand")!=null && !"".equals(searchMap.get("brand"))){
                criteria.andLike("brand","%"+searchMap.get("brand")+"%");
            }
            // 适用年龄
            if(searchMap.get("fitAge")!=null && !"".equals(searchMap.get("fitAge"))){
                criteria.andLike("fitAge","%"+searchMap.get("fitAge")+"%");
            }
            // 上市年份季节
            if(searchMap.get("sentSeason")!=null && !"".equals(searchMap.get("sentSeason"))){
                criteria.andLike("sentSeason","%"+searchMap.get("sentSeason")+"%");
            }
            // 销售渠道类型
            if(searchMap.get("salesChannelType")!=null && !"".equals(searchMap.get("salesChannelType"))){
                criteria.andLike("salesChannelType","%"+searchMap.get("salesChannelType")+"%");
            }
            // 材质成分
            if(searchMap.get("materialComposition")!=null && !"".equals(searchMap.get("materialComposition"))){
                criteria.andLike("materialComposition","%"+searchMap.get("materialComposition")+"%");
            }
            // 基础风格
            if(searchMap.get("style")!=null && !"".equals(searchMap.get("style"))){
                criteria.andLike("style","%"+searchMap.get("style")+"%");
            }
            // 图案
            if(searchMap.get("pattern")!=null && !"".equals(searchMap.get("pattern"))){
                criteria.andLike("pattern","%"+searchMap.get("pattern")+"%");
            }

            // 货号
            if(searchMap.get("itemNo")!=null ){
                criteria.andEqualTo("itemNo",searchMap.get("itemNo"));
            }

        }
        return example;
    }

}
