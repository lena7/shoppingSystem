package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.DetailsImgMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.DetailsImg;
import cn.web.service.shop.DetailsImgService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DetailsImgServiceImpl implements DetailsImgService {

    @Autowired
    private DetailsImgMapper detailsImgMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<DetailsImg> findAll() {
        return detailsImgMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<DetailsImg> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<DetailsImg> detailsImgs = (Page<DetailsImg>) detailsImgMapper.selectAll();
        return new PageResult<DetailsImg>(detailsImgs.getTotal(),detailsImgs.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<DetailsImg> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return detailsImgMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<DetailsImg> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<DetailsImg> detailsImgs = (Page<DetailsImg>) detailsImgMapper.selectByExample(example);
        return new PageResult<DetailsImg>(detailsImgs.getTotal(),detailsImgs.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public DetailsImg findById(String id) {
        return detailsImgMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param detailsImg
     */
    public void add(DetailsImg detailsImg) {
        detailsImgMapper.insert(detailsImg);
    }

    /**
     * 修改
     * @param detailsImg
     */
    public void update(DetailsImg detailsImg) {
        detailsImgMapper.updateByPrimaryKeySelective(detailsImg);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        detailsImgMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(DetailsImg.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 商品详情页图片id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // img1
            if(searchMap.get("img1")!=null && !"".equals(searchMap.get("img1"))){
                criteria.andLike("img1","%"+searchMap.get("img1")+"%");
            }
            // img2
            if(searchMap.get("img2")!=null && !"".equals(searchMap.get("img2"))){
                criteria.andLike("img2","%"+searchMap.get("img2")+"%");
            }
            // img3
            if(searchMap.get("img3")!=null && !"".equals(searchMap.get("img3"))){
                criteria.andLike("img3","%"+searchMap.get("img3")+"%");
            }
            // img4
            if(searchMap.get("img4")!=null && !"".equals(searchMap.get("img4"))){
                criteria.andLike("img4","%"+searchMap.get("img4")+"%");
            }
            // img5
            if(searchMap.get("img5")!=null && !"".equals(searchMap.get("img5"))){
                criteria.andLike("img5","%"+searchMap.get("img5")+"%");
            }
            // img6
            if(searchMap.get("img6")!=null && !"".equals(searchMap.get("img6"))){
                criteria.andLike("img6","%"+searchMap.get("img6")+"%");
            }
            // img7
            if(searchMap.get("img7")!=null && !"".equals(searchMap.get("img7"))){
                criteria.andLike("img7","%"+searchMap.get("img7")+"%");
            }
            // img8
            if(searchMap.get("img8")!=null && !"".equals(searchMap.get("img8"))){
                criteria.andLike("img8","%"+searchMap.get("img8")+"%");
            }
            // img9
            if(searchMap.get("img9")!=null && !"".equals(searchMap.get("img9"))){
                criteria.andLike("img9","%"+searchMap.get("img9")+"%");
            }
            // img10
            if(searchMap.get("img10")!=null && !"".equals(searchMap.get("img10"))){
                criteria.andLike("img10","%"+searchMap.get("img10")+"%");
            }
            // 商品描述
            if(searchMap.get("context")!=null && !"".equals(searchMap.get("context"))){
                criteria.andLike("context","%"+searchMap.get("context")+"%");
            }

            // 图片数量
            if(searchMap.get("number")!=null ){
                criteria.andEqualTo("number",searchMap.get("number"));
            }

        }
        return example;
    }

}
