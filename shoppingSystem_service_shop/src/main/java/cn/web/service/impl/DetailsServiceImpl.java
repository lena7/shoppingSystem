package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.DetailsMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Details;
import cn.web.service.shop.DetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class DetailsServiceImpl implements DetailsService {

    @Autowired
    private DetailsMapper detailsMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Details> findAll() {
        return detailsMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Details> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Details> detailss = (Page<Details>) detailsMapper.selectAll();
        return new PageResult<Details>(detailss.getTotal(),detailss.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Details> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return detailsMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Details> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Details> detailss = (Page<Details>) detailsMapper.selectByExample(example);
        return new PageResult<Details>(detailss.getTotal(),detailss.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Details findById(String id) {
        return detailsMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param details
     */
    public void add(Details details) {
        detailsMapper.insert(details);
    }

    /**
     * 修改
     * @param details
     */
    public void update(Details details) {
        detailsMapper.updateByPrimaryKeySelective(details);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        detailsMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Details.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 订单详情id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 订单号
            if(searchMap.get("orderId")!=null && !"".equals(searchMap.get("orderId"))){
                criteria.andLike("orderId","%"+searchMap.get("orderId")+"%");
            }
            // 店铺id
            if(searchMap.get("shopId")!=null && !"".equals(searchMap.get("shopId"))){
                criteria.andLike("shopId","%"+searchMap.get("shopId")+"%");
            }
            // 备注信息
            if(searchMap.get("remarks")!=null && !"".equals(searchMap.get("remarks"))){
                criteria.andLike("remarks","%"+searchMap.get("remarks")+"%");
            }
            // 物流信息ID
            if(searchMap.get("logisticsId")!=null && !"".equals(searchMap.get("logisticsId"))){
                criteria.andLike("logisticsId","%"+searchMap.get("logisticsId")+"%");
            }
            // 用户ID
            if(searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))){
                criteria.andLike("userId","%"+searchMap.get("userId")+"%");
            }
            // 收获地址ID
            if(searchMap.get("shippingAddressId")!=null && !"".equals(searchMap.get("shippingAddressId"))){
                criteria.andLike("shippingAddressId","%"+searchMap.get("shippingAddressId")+"%");
            }

            // 运费险
            if(searchMap.get("freight")!=null ){
                criteria.andEqualTo("freight",searchMap.get("freight"));
            }
            // 是否有运费险（1有0无）
            if(searchMap.get("freightInsurance")!=null ){
                criteria.andEqualTo("freightInsurance",searchMap.get("freightInsurance"));
            }
            // 当前状态（未付款0 已付款1 已收货2 已评价3 已追评4 已过期5）
            if(searchMap.get("status")!=null ){
                criteria.andEqualTo("status",searchMap.get("status"));
            }

        }
        return example;
    }

}
