package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.EvaluateMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Evaluate;
import cn.web.service.shop.EvaluateService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class EvaluateServiceImpl implements EvaluateService {

    @Autowired
    private EvaluateMapper evaluateMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Evaluate> findAll() {
        return evaluateMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Evaluate> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Evaluate> evaluates = (Page<Evaluate>) evaluateMapper.selectAll();
        return new PageResult<Evaluate>(evaluates.getTotal(),evaluates.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Evaluate> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return evaluateMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Evaluate> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Evaluate> evaluates = (Page<Evaluate>) evaluateMapper.selectByExample(example);
        return new PageResult<Evaluate>(evaluates.getTotal(),evaluates.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Evaluate findById(String id) {
        return evaluateMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param evaluate
     */
    public void add(Evaluate evaluate) {
        evaluateMapper.insert(evaluate);
    }

    /**
     * 修改
     * @param evaluate
     */
    public void update(Evaluate evaluate) {
        evaluateMapper.updateByPrimaryKeySelective(evaluate);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        evaluateMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Evaluate.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 评价ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 订单ID
            if(searchMap.get("orderId")!=null && !"".equals(searchMap.get("orderId"))){
                criteria.andLike("orderId","%"+searchMap.get("orderId")+"%");
            }
            // 商品ID
            if(searchMap.get("goodsId")!=null && !"".equals(searchMap.get("goodsId"))){
                criteria.andLike("goodsId","%"+searchMap.get("goodsId")+"%");
            }
            // 评价内容
            if(searchMap.get("content")!=null && !"".equals(searchMap.get("content"))){
                criteria.andLike("content","%"+searchMap.get("content")+"%");
            }
            // 用户id
            if(searchMap.get("userId")!=null && !"".equals(searchMap.get("userId"))){
                criteria.andLike("userId","%"+searchMap.get("userId")+"%");
            }

            // 是否匿名
            if(searchMap.get("isAnonymouns")!=null ){
                criteria.andEqualTo("isAnonymouns",searchMap.get("isAnonymouns"));
            }
            // 评价等级（好评1 中评2 差评3）
            if(searchMap.get("grade")!=null ){
                criteria.andEqualTo("grade",searchMap.get("grade"));
            }
            // 描述相符（1-5）
            if(searchMap.get("describeFit")!=null ){
                criteria.andEqualTo("describeFit",searchMap.get("describeFit"));
            }
            // 物流服务（1-5）
            if(searchMap.get("logisticsService")!=null ){
                criteria.andEqualTo("logisticsService",searchMap.get("logisticsService"));
            }
            // 服务态度（1-5）
            if(searchMap.get("serviceAttitude")!=null ){
                criteria.andEqualTo("serviceAttitude",searchMap.get("serviceAttitude"));
            }
            // 是否有图片（0无 1有）
            if(searchMap.get("isImg")!=null ){
                criteria.andEqualTo("isImg",searchMap.get("isImg"));
            }

        }
        return example;
    }

}
