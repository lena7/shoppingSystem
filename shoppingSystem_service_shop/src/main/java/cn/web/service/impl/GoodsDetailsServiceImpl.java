package cn.web.service.impl;

import cn.web.dao.GoodsDetailsMapper;
import cn.web.dao.MainImgMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.GoodsDetails;
import cn.web.service.shop.GoodsDetailsService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class GoodsDetailsServiceImpl implements GoodsDetailsService {

	@Autowired
	private GoodsDetailsMapper goodsDetailsMapper;

	@Autowired
	private MainImgMapper mainImgMapper;

	/**
	 * 返回全部记录
	 * @return
	 */
	public List<GoodsDetails> findAll() {
		return goodsDetailsMapper.selectAll();
	}

	/**
	 * 分页查询
	 * @param page 页码
	 * @param size 每页记录数
	 * @return 分页结果
	 */
	public PageResult<GoodsDetails> findPage(int page, int size) {
		PageHelper.startPage(page,size);
		Page<GoodsDetails> accounts = (Page<GoodsDetails>) goodsDetailsMapper.selectAll();
		//System.out.println(accounts.get(0));
		return new PageResult<GoodsDetails>(accounts.getTotal(),accounts.getResult());
	}

	/**
	 * 条件查询
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<GoodsDetails> findList(Map<String, Object> searchMap) {
		Example example = createExample(searchMap);
		return goodsDetailsMapper.selectByExample(example);
	}

	/**
	 * 分页+条件查询
	 * @param searchMap
	 * @param page
	 * @param size
	 * @return
	 */
	public PageResult<GoodsDetails> findPage(Map<String, Object> searchMap, int page, int size) {
		PageHelper.startPage(page,size);
		Example example = createExample(searchMap);
		Page<GoodsDetails> accounts = (Page<GoodsDetails>) goodsDetailsMapper.selectByExample(example);
		return new PageResult<GoodsDetails>(accounts.getTotal(),accounts.getResult());
	}

	/**
	 * 根据Id查询
	 * @param id
	 * @return
	 */
	public GoodsDetails findById(String id) {
		return goodsDetailsMapper.selectByPrimaryKey(id);
	}

	/**
	 * 新增
	 * @param goodsDetails
	 */
	public void add(GoodsDetails goodsDetails) {
		goodsDetailsMapper.insert(goodsDetails);
	}

	/**
	 * 修改
	 * @param goodsDetails
	 */
	public void update(GoodsDetails goodsDetails) {
		goodsDetailsMapper.updateByPrimaryKeySelective(goodsDetails);
	}

	/**
	 *  删除
	 * @param id
	 */
	public void delete(String id) {
		goodsDetailsMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 构建查询条件
	 * @param searchMap
	 * @return
	 */
	private Example createExample(Map<String, Object> searchMap){
		Example example=new Example(GoodsDetails.class);
		Example.Criteria criteria = example.createCriteria();
		if(searchMap!=null){
			System.out.println("1");
			// 商品标题
			if(searchMap.get("title")!=null && !"".equals(searchMap.get("title"))){
				System.out.println("2");
				criteria.andLike("title","%"+searchMap.get("title")+"%");
			}
			// 发货地
			if(searchMap.get("deliveryPlace")!=null && !"".equals(searchMap.get("deliveryPlace"))){
				criteria.andLike("deliveryPlace","%"+searchMap.get("deliveryPlace")+"%");
			}
			// 保障
			if(searchMap.get("guarantee")!=null && !"".equals(searchMap.get("guarantee"))){
				criteria.andLike("guarantee","%"+searchMap.get("guarantee")+"%");
			}
			// 快递
			if(searchMap.get("express")!=null ){
				criteria.andEqualTo("express",searchMap.get("express"));
			}
			// 库存
			if(searchMap.get("stock")!=null ){
				criteria.andEqualTo("stock",searchMap.get("stock"));
			}

		}
		return example;
	}
}
