package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.GoodsMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Goods;
import cn.web.service.shop.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Goods> findAll() {
        return goodsMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Goods> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Goods> goodss = (Page<Goods>) goodsMapper.selectAll();
        return new PageResult<Goods>(goodss.getTotal(),goodss.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Goods> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return goodsMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Goods> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Goods> goodss = (Page<Goods>) goodsMapper.selectByExample(example);
        return new PageResult<Goods>(goodss.getTotal(),goodss.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Goods findById(String id) {
        return goodsMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param goods
     */
    public void add(Goods goods) {
        goodsMapper.insert(goods);
    }

    /**
     * 修改
     * @param goods
     */
    public void update(Goods goods) {
        goodsMapper.updateByPrimaryKeySelective(goods);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        goodsMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Goods.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 商品id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 商品主图id
            if(searchMap.get("mainImgId")!=null && !"".equals(searchMap.get("mainImgId"))){
                criteria.andLike("mainImgId","%"+searchMap.get("mainImgId")+"%");
            }
            // 商品详情id
            if(searchMap.get("detailsId")!=null && !"".equals(searchMap.get("detailsId"))){
                criteria.andLike("detailsId","%"+searchMap.get("detailsId")+"%");
            }
            // 商品详情页图片id
            if(searchMap.get("detailsImgId")!=null && !"".equals(searchMap.get("detailsImgId"))){
                criteria.andLike("detailsImgId","%"+searchMap.get("detailsImgId")+"%");
            }
            // 商品所属类别id
            if(searchMap.get("catagoryId")!=null && !"".equals(searchMap.get("catagoryId"))){
                criteria.andLike("catagoryId","%"+searchMap.get("catagoryId")+"%");
            }
            // 商品成分id
            if(searchMap.get("componentId")!=null && !"".equals(searchMap.get("componentId"))){
                criteria.andLike("componentId","%"+searchMap.get("componentId")+"%");
            }
            // 商品类型选择id
            if(searchMap.get("typeSelectId")!=null && !"".equals(searchMap.get("typeSelectId"))){
                criteria.andLike("typeSelectId","%"+searchMap.get("typeSelectId")+"%");
            }
            // 所属店铺id
            if(searchMap.get("shopId")!=null && !"".equals(searchMap.get("shopId"))){
                criteria.andLike("shopId","%"+searchMap.get("shopId")+"%");
            }


        }
        return example;
    }

}
