package cn.web.service.impl;

import cn.web.dao.GoodsTypeSelectMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Account;
import cn.web.pojo.shop.GoodsTypeSelect;
import cn.web.service.shop.GoodsTypeSelectService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
@Service
public class GoodsTypeSelectServiceImpl implements GoodsTypeSelectService {

	@Autowired
	private GoodsTypeSelectMapper goodsTypeSelectMapper;

	/**
	 * 返回全部记录
	 * @return
	 */
	public List<GoodsTypeSelect> findAll() {
		return goodsTypeSelectMapper.selectAll();
	}

	/**
	 * 分页查询
	 * @param page 页码
	 * @param size 每页记录数
	 * @return 分页结果
	 */
	public PageResult<GoodsTypeSelect> findPage(int page, int size) {
		PageHelper.startPage(page,size);
		Page<GoodsTypeSelect> accounts = (Page<GoodsTypeSelect>) goodsTypeSelectMapper.selectAll();
		return new PageResult<GoodsTypeSelect>(accounts.getTotal(),accounts.getResult());
	}

	/**
	 * 条件查询
	 * @param searchMap 查询条件
	 * @return
	 */
	public List<GoodsTypeSelect> findList(Map<String, Object> searchMap) {
		Example example = createExample(searchMap);
		return goodsTypeSelectMapper.selectByExample(example);
	}

	/**
	 * 分页+条件查询
	 * @param searchMap
	 * @param page
	 * @param size
	 * @return
	 */
	public PageResult<GoodsTypeSelect> findPage(Map<String, Object> searchMap, int page, int size) {
		PageHelper.startPage(page,size);
		Example example = createExample(searchMap);
		Page<GoodsTypeSelect> accounts = (Page<GoodsTypeSelect>) goodsTypeSelectMapper.selectByExample(example);
		return new PageResult<GoodsTypeSelect>(accounts.getTotal(),accounts.getResult());
	}

	/**
	 * 根据Id查询
	 * @param id
	 * @return
	 */
	public GoodsTypeSelect findById(String id) {
		return goodsTypeSelectMapper.selectByPrimaryKey(id);
	}

	/**
	 * 新增
	 * @param goodsTypeSelect
	 */
	public void add(GoodsTypeSelect goodsTypeSelect) {
		goodsTypeSelectMapper.insert(goodsTypeSelect);
	}

	/**
	 * 修改
	 * @param goodsTypeSelect
	 */
	public void update(GoodsTypeSelect goodsTypeSelect) {
		goodsTypeSelectMapper.updateByPrimaryKeySelective(goodsTypeSelect);
	}

	/**
	 *  删除
	 * @param id
	 */
	public void delete(String id) {
		goodsTypeSelectMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 构建查询条件
	 * @param searchMap
	 * @return
	 */
	private Example createExample(Map<String, Object> searchMap){
		Example example=new Example(Account.class);
		Example.Criteria criteria = example.createCriteria();
		if(searchMap!=null){
			// id
			if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
				criteria.andLike("id","%"+searchMap.get("id")+"%");
			}
			//颜色id
			if(searchMap.get("colorId")!=null && !"".equals(searchMap.get("colorId"))){
				criteria.andLike("colorId","%"+searchMap.get("colorId")+"%");
			}
			// 尺码ID
			if(searchMap.get("sizeId")!=null && !"".equals(searchMap.get("sizeId"))){
				criteria.andLike("sizeId","%"+searchMap.get("sizeId")+"%");
			}
			// 库存
			if(searchMap.get("stock")!=null ){
				criteria.andEqualTo("stock",searchMap.get("stock"));
			}

		}
		return example;
	}
}
