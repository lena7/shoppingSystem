package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.ImgMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Img;
import cn.web.service.shop.ImgService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ImgServiceImpl implements ImgService {

    @Autowired
    private ImgMapper imgMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Img> findAll() {
        return imgMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Img> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Img> imgs = (Page<Img>) imgMapper.selectAll();
        return new PageResult<Img>(imgs.getTotal(),imgs.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Img> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return imgMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Img> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Img> imgs = (Page<Img>) imgMapper.selectByExample(example);
        return new PageResult<Img>(imgs.getTotal(),imgs.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Img findById(String id) {
        return imgMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param img
     */
    public void add(Img img) {
        imgMapper.insert(img);
    }

    /**
     * 修改
     * @param img
     */
    public void update(Img img) {
        imgMapper.updateByPrimaryKeySelective(img);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        imgMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Img.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 评价图片id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 评价ID
            if(searchMap.get("evaluateId")!=null && !"".equals(searchMap.get("evaluateId"))){
                criteria.andLike("evaluateId","%"+searchMap.get("evaluateId")+"%");
            }
            // 图片1
            if(searchMap.get("img1")!=null && !"".equals(searchMap.get("img1"))){
                criteria.andLike("img1","%"+searchMap.get("img1")+"%");
            }
            // 图片2
            if(searchMap.get("img2")!=null && !"".equals(searchMap.get("img2"))){
                criteria.andLike("img2","%"+searchMap.get("img2")+"%");
            }
            // 图片3
            if(searchMap.get("img3")!=null && !"".equals(searchMap.get("img3"))){
                criteria.andLike("img3","%"+searchMap.get("img3")+"%");
            }
            // 图片4
            if(searchMap.get("img4")!=null && !"".equals(searchMap.get("img4"))){
                criteria.andLike("img4","%"+searchMap.get("img4")+"%");
            }
            // 图片5
            if(searchMap.get("img5")!=null && !"".equals(searchMap.get("img5"))){
                criteria.andLike("img5","%"+searchMap.get("img5")+"%");
            }

            // 图片总数
            if(searchMap.get("number")!=null ){
                criteria.andEqualTo("number",searchMap.get("number"));
            }

        }
        return example;
    }

}
