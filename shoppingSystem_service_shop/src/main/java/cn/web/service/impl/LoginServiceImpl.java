package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.LoginMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Login;
import cn.web.service.shop.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    /**
     * 登录
     * @return
     */
    @Override
    public int Login(String id,String password) {
        Example example=new Example(Login.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id",id).andEqualTo("password",password);
        List<Login> logins = loginMapper.selectByExample(criteria);
        if (logins.isEmpty()){
            System.out.println("账号密码错误!");
            return 0;
        }else{
            System.out.println("账号密码正确！");
            return logins.get(0).getStatus();
        }
    }

    /**
     * 返回全部记录
     * @return
     */
    public List<Login> findAll() {
        return loginMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Login> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Login> logins = (Page<Login>) loginMapper.selectAll();
        return new PageResult<Login>(logins.getTotal(),logins.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Login> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return loginMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Login> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Login> logins = (Page<Login>) loginMapper.selectByExample(example);
        return new PageResult<Login>(logins.getTotal(),logins.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Login findById(String id) {
        return loginMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param login
     */
    public void add(Login login) {
        loginMapper.insert(login);
    }

    /**
     * 修改
     * @param login
     */
    public void update(Login login) {
        loginMapper.updateByPrimaryKeySelective(login);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        loginMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Login.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 账号
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            //密码
            if(searchMap.get("password")!=null && !"".equals(searchMap.get("password"))){
                criteria.andEqualTo("password",searchMap.get("password"));
            }
        }
        return example;
    }

}
