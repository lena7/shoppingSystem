package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.LogisticsMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Logistics;
import cn.web.service.shop.LogisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class LogisticsServiceImpl implements LogisticsService {

    @Autowired
    private LogisticsMapper logisticsMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Logistics> findAll() {
        return logisticsMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Logistics> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Logistics> logisticss = (Page<Logistics>) logisticsMapper.selectAll();
        return new PageResult<Logistics>(logisticss.getTotal(),logisticss.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Logistics> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return logisticsMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Logistics> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Logistics> logisticss = (Page<Logistics>) logisticsMapper.selectByExample(example);
        return new PageResult<Logistics>(logisticss.getTotal(),logisticss.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Logistics findById(String id) {
        return logisticsMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param logistics
     */
    public void add(Logistics logistics) {
        logisticsMapper.insert(logistics);
    }

    /**
     * 修改
     * @param logistics
     */
    public void update(Logistics logistics) {
        logisticsMapper.updateByPrimaryKeySelective(logistics);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        logisticsMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Logistics.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 物流号ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 发货地址
            if(searchMap.get("sentAddress")!=null && !"".equals(searchMap.get("sentAddress"))){
                criteria.andLike("sentAddress","%"+searchMap.get("sentAddress")+"%");
            }
            // 收获地址
            if(searchMap.get("receiveAddress")!=null && !"".equals(searchMap.get("receiveAddress"))){
                criteria.andLike("receiveAddress","%"+searchMap.get("receiveAddress")+"%");
            }
            // 揽件快递员电话
            if(searchMap.get("pickupPhone")!=null && !"".equals(searchMap.get("pickupPhone"))){
                criteria.andLike("pickupPhone","%"+searchMap.get("pickupPhone")+"%");
            }
            // 当前运输位置
            if(searchMap.get("nowPosition")!=null && !"".equals(searchMap.get("nowPosition"))){
                criteria.andLike("nowPosition","%"+searchMap.get("nowPosition")+"%");
            }
            // 派送人
            if(searchMap.get("deliverer")!=null && !"".equals(searchMap.get("deliverer"))){
                criteria.andLike("deliverer","%"+searchMap.get("deliverer")+"%");
            }
            // 派送人联系方式
            if(searchMap.get("deliveryPhone")!=null && !"".equals(searchMap.get("deliveryPhone"))){
                criteria.andLike("deliveryPhone","%"+searchMap.get("deliveryPhone")+"%");
            }
            // 签收人
            if(searchMap.get("signer")!=null && !"".equals(searchMap.get("signer"))){
                criteria.andLike("signer","%"+searchMap.get("signer")+"%");
            }
            // 快递公司（如：顺丰快递）
            if(searchMap.get("expressCompany")!=null && !"".equals(searchMap.get("expressCompany"))){
                criteria.andLike("expressCompany","%"+searchMap.get("expressCompany")+"%");
            }

            // 当前状态（未发货0 发货1 揽件2 运输3 派送4 签收5）
            if(searchMap.get("status")!=null ){
                criteria.andEqualTo("status",searchMap.get("status"));
            }
            // 快递员评价等级（1-5）
            if(searchMap.get("deliveryEvaluate")!=null ){
                criteria.andEqualTo("deliveryEvaluate",searchMap.get("deliveryEvaluate"));
            }

        }
        return example;
    }

}
