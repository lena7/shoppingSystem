package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.MainImgMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.MainImg;
import cn.web.service.shop.MainImgService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class MainImgServiceImpl implements MainImgService {

    @Autowired
    private MainImgMapper mainImgMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<MainImg> findAll() {
        return mainImgMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<MainImg> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<MainImg> mainImgs = (Page<MainImg>) mainImgMapper.selectAll();
        return new PageResult<MainImg>(mainImgs.getTotal(),mainImgs.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<MainImg> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return mainImgMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<MainImg> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<MainImg> mainImgs = (Page<MainImg>) mainImgMapper.selectByExample(example);
        return new PageResult<MainImg>(mainImgs.getTotal(),mainImgs.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public MainImg findById(String id) {
        return mainImgMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param mainImg
     */
    public void add(MainImg mainImg) {
        mainImgMapper.insert(mainImg);
    }

    /**
     * 修改
     * @param mainImg
     */
    public void update(MainImg mainImg) {
        mainImgMapper.updateByPrimaryKeySelective(mainImg);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        mainImgMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(MainImg.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 商品id（有点多余）
            if(searchMap.get("goodsId")!=null && !"".equals(searchMap.get("goodsId"))){
                criteria.andLike("goodsId","%"+searchMap.get("goodsId")+"%");
            }
            // 主图1
            if(searchMap.get("img1")!=null && !"".equals(searchMap.get("img1"))){
                criteria.andLike("img1","%"+searchMap.get("img1")+"%");
            }
            // 主图2
            if(searchMap.get("img2")!=null && !"".equals(searchMap.get("img2"))){
                criteria.andLike("img2","%"+searchMap.get("img2")+"%");
            }
            // 主图3
            if(searchMap.get("img3")!=null && !"".equals(searchMap.get("img3"))){
                criteria.andLike("img3","%"+searchMap.get("img3")+"%");
            }
            // 主图4
            if(searchMap.get("img4")!=null && !"".equals(searchMap.get("img4"))){
                criteria.andLike("img4","%"+searchMap.get("img4")+"%");
            }
            // 主图5
            if(searchMap.get("img5")!=null && !"".equals(searchMap.get("img5"))){
                criteria.andLike("img5","%"+searchMap.get("img5")+"%");
            }

            // 主图数量
            if(searchMap.get("imgNumber")!=null ){
                criteria.andEqualTo("imgNumber",searchMap.get("imgNumber"));
            }

        }
        return example;
    }

}
