package cn.web.service.impl;

import cn.web.dao.ReferalLinkMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.ReferalLink;
import cn.web.service.shop.ReferalLinkService;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
@Service
public class ReferalLinkServiceImpl implements ReferalLinkService {

	@Autowired
	private ReferalLinkMapper referalLinkMapper;

	/**
	 * 返回全部记录
	 * @return
	 */
	public List<ReferalLink> findAll() {
		return referalLinkMapper.selectAll();
	}

	/**
	 * 分页查询
	 * @param page 页码
	 * @param size 每页记录数
	 * @return 分页结果
	 */
	public PageResult<ReferalLink> findPage(int page, int size) {
		PageHelper.startPage(page,size);
		Page<ReferalLink> referalLinks = (Page<ReferalLink>) referalLinkMapper.selectAll();
		return new PageResult<ReferalLink>(referalLinks.getTotal(),referalLinks.getResult());
	}


	/**
	 * 根据Id查询
	 * @param id
	 * @return
	 */
	public ReferalLink findById(String id) {
		return referalLinkMapper.selectByPrimaryKey(id);
	}

	/**
	 * 新增
	 * @param referalLink
	 */
	public void add(ReferalLink referalLink) {
		referalLinkMapper.insert(referalLink);
	}

	/**
	 * 修改
	 * @param referalLink
	 */
	public void update(ReferalLink referalLink) {
		referalLinkMapper.updateByPrimaryKeySelective(referalLink);
	}

	/**
	 *  删除
	 * @param id
	 */
	public void delete(String id) {
		referalLinkMapper.deleteByPrimaryKey(id);
	}

}
