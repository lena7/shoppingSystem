package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.ShopMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Shop;
import cn.web.service.shop.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ShopMapper shopMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Shop> findAll() {
        return shopMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Shop> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Shop> shops = (Page<Shop>) shopMapper.selectAll();
        return new PageResult<Shop>(shops.getTotal(),shops.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Shop> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return shopMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Shop> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Shop> shops = (Page<Shop>) shopMapper.selectByExample(example);
        return new PageResult<Shop>(shops.getTotal(),shops.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Shop findById(String id) {
        return shopMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param shop
     */
    public void add(Shop shop) {
        shopMapper.insert(shop);
    }

    /**
     * 修改
     * @param shop
     */
    public void update(Shop shop) {
        shopMapper.updateByPrimaryKeySelective(shop);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        shopMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Shop.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 店铺ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 商家ID
            if(searchMap.get("businessId")!=null && !"".equals(searchMap.get("businessId"))){
                criteria.andLike("businessId","%"+searchMap.get("businessId")+"%");
            }
            // 店铺名
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 资质
            if(searchMap.get("qualification")!=null && !"".equals(searchMap.get("qualification"))){
                criteria.andLike("qualification","%"+searchMap.get("qualification")+"%");
            }
            // 店铺名片（二维码）
            if(searchMap.get("cardImg")!=null && !"".equals(searchMap.get("cardImg"))){
                criteria.andLike("cardImg","%"+searchMap.get("cardImg")+"%");
            }

            // 粉丝数
            if(searchMap.get("fansNumber")!=null ){
                criteria.andEqualTo("fansNumber",searchMap.get("fansNumber"));
            }
            // 当前店铺等级
            if(searchMap.get("level")!=null ){
                criteria.andEqualTo("level",searchMap.get("level"));
            }

        }
        return example;
    }

}
