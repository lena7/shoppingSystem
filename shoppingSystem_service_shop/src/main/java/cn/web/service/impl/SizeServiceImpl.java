package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.SizeMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.Size;
import cn.web.service.shop.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class SizeServiceImpl implements SizeService {

    @Autowired
    private SizeMapper sizeMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<Size> findAll() {
        return sizeMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<Size> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<Size> sizes = (Page<Size>) sizeMapper.selectAll();
        return new PageResult<Size>(sizes.getTotal(),sizes.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<Size> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return sizeMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<Size> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<Size> sizes = (Page<Size>) sizeMapper.selectByExample(example);
        return new PageResult<Size>(sizes.getTotal(),sizes.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public Size findById(String id) {
        return sizeMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param size
     */
    public void add(Size size) {
        sizeMapper.insert(size);
    }

    /**
     * 修改
     * @param size
     */
    public void update(Size size) {
        sizeMapper.updateByPrimaryKeySelective(size);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        sizeMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(Size.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // 尺码ID
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 尺码名
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }


        }
        return example;
    }

}
