package cn.web.service.impl;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import cn.web.dao.UserMapper;
import cn.web.entity.PageResult;
import cn.web.pojo.shop.User;
import cn.web.service.shop.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 返回全部记录
     * @return
     */
    public List<User> findAll() {
        return userMapper.selectAll();
    }

    /**
     * 分页查询
     * @param page 页码
     * @param size 每页记录数
     * @return 分页结果
     */
    public PageResult<User> findPage(int page, int size) {
        PageHelper.startPage(page,size);
        Page<User> users = (Page<User>) userMapper.selectAll();
        return new PageResult<User>(users.getTotal(),users.getResult());
    }

    /**
     * 条件查询
     * @param searchMap 查询条件
     * @return
     */
    public List<User> findList(Map<String, Object> searchMap) {
        Example example = createExample(searchMap);
        return userMapper.selectByExample(example);
    }

    /**
     * 分页+条件查询
     * @param searchMap
     * @param page
     * @param size
     * @return
     */
    public PageResult<User> findPage(Map<String, Object> searchMap, int page, int size) {
        PageHelper.startPage(page,size);
        Example example = createExample(searchMap);
        Page<User> users = (Page<User>) userMapper.selectByExample(example);
        return new PageResult<User>(users.getTotal(),users.getResult());
    }

    /**
     * 根据Id查询
     * @param id
     * @return
     */
    public User findById(String id) {
        return userMapper.selectByPrimaryKey(id);
    }

    /**
     * 新增
     * @param user
     */
    public void add(User user) {
        userMapper.insert(user);
    }

    /**
     * 修改
     * @param user
     */
    public void update(User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     *  删除
     * @param id
     */
    public void delete(String id) {
        userMapper.deleteByPrimaryKey(id);
    }

    /**
     * 构建查询条件
     * @param searchMap
     * @return
     */
    private Example createExample(Map<String, Object> searchMap){
        Example example=new Example(User.class);
        Example.Criteria criteria = example.createCriteria();
        if(searchMap!=null){
            // id
            if(searchMap.get("id")!=null && !"".equals(searchMap.get("id"))){
                criteria.andLike("id","%"+searchMap.get("id")+"%");
            }
            // 昵称
            if(searchMap.get("name")!=null && !"".equals(searchMap.get("name"))){
                criteria.andLike("name","%"+searchMap.get("name")+"%");
            }
            // 性别
            if(searchMap.get("sex")!=null && !"".equals(searchMap.get("sex"))){
                criteria.andLike("sex","%"+searchMap.get("sex")+"%");
            }
            // 邮箱
            if(searchMap.get("email")!=null && !"".equals(searchMap.get("email"))){
                criteria.andLike("email","%"+searchMap.get("email")+"%");
            }
            // 真实姓名
            if(searchMap.get("realName")!=null && !"".equals(searchMap.get("realName"))){
                criteria.andLike("realName","%"+searchMap.get("realName")+"%");
            }
            // 身份证号
            if(searchMap.get("idCard")!=null && !"".equals(searchMap.get("idCard"))){
                criteria.andLike("idCard","%"+searchMap.get("idCard")+"%");
            }
            // 个性签名
            if(searchMap.get("brief")!=null && !"".equals(searchMap.get("brief"))){
                criteria.andLike("brief","%"+searchMap.get("brief")+"%");
            }
            // 头像
            if(searchMap.get("faceImg")!=null && !"".equals(searchMap.get("faceImg"))){
                criteria.andLike("faceImg","%"+searchMap.get("faceImg")+"%");
            }
            // 支付宝账号
            if(searchMap.get("alipayAccount")!=null && !"".equals(searchMap.get("alipayAccount"))){
                criteria.andLike("alipayAccount","%"+searchMap.get("alipayAccount")+"%");
            }
            // 手机号码
            if(searchMap.get("phoneNumber")!=null && !"".equals(searchMap.get("phoneNumber"))){
                criteria.andLike("phoneNumber","%"+searchMap.get("phoneNumber")+"%");
            }
            // 银行卡号
            if(searchMap.get("bankAccount")!=null && !"".equals(searchMap.get("bankAccount"))){
                criteria.andLike("bankAccount","%"+searchMap.get("bankAccount")+"%");
            }

            // 年龄
            if(searchMap.get("age")!=null ){
                criteria.andEqualTo("age",searchMap.get("age"));
            }

        }
        return example;
    }

}
