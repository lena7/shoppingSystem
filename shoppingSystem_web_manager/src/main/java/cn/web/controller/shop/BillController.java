package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Bill;
import cn.web.service.shop.BillService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/bill")
public class BillController {

    @Reference
    private BillService billService;

    @GetMapping("/findAll")
    public List<Bill> findAll(){
        return billService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Bill> findPage(int page, int size){
        return billService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Bill> findList(@RequestBody Map<String,Object> searchMap){
        return billService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Bill> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  billService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Bill findById(String id){
        return billService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Bill bill){
        billService.add(bill);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Bill bill){
        billService.update(bill);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        billService.delete(id);
        return new Result();
    }

}
