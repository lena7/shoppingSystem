package cn.web.controller.shop;

import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.BusinessBill;
import cn.web.service.shop.BusinessBillService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/businessBill")
public class BusinessBillController {

	@Reference
	private BusinessBillService businessBillService;

	@GetMapping("/findAll")
	public List<BusinessBill> findAll(){
		return businessBillService.findAll();
	}

	@GetMapping("/findPage")
	public PageResult<BusinessBill> findPage(int page, int size){
		return businessBillService.findPage(page, size);
	}

	@PostMapping("/findList")
	public List<BusinessBill> findList(@RequestBody Map<String,Object> searchMap){
		return businessBillService.findList(searchMap);
	}

	@PostMapping("/findPage")
	public PageResult<BusinessBill> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
		return  businessBillService.findPage(searchMap,page,size);
	}

	@GetMapping("/findById")
	public BusinessBill findById(String id){
		return businessBillService.findById(id);
	}


	@PostMapping("/add")
	public Result add(@RequestBody BusinessBill businessBill){
		businessBillService.add(businessBill);
		return new Result();
	}

	@PostMapping("/update")
	public Result update(@RequestBody BusinessBill businessBill){
		businessBillService.update(businessBill);
		return new Result();
	}

	@GetMapping("/delete")
	public Result delete(String id){
		businessBillService.delete(id);
		return new Result();
	}

}
