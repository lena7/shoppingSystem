package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Business;
import cn.web.service.shop.BusinessService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/business")
public class BusinessController {

    @Reference
    private BusinessService businessService;

    @GetMapping("/findAll")
    public List<Business> findAll(){
        return businessService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Business> findPage(int page, int size){
        return businessService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Business> findList(@RequestBody Map<String,Object> searchMap){
        return businessService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Business> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  businessService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Business findById(String id){
        return businessService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Business business){
        businessService.add(business);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Business business){
        businessService.update(business);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        businessService.delete(id);
        return new Result();
    }

}
