package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Cart;
import cn.web.service.shop.CartService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Reference
    private CartService cartService;

    @GetMapping("/findAll")
    public List<Cart> findAll(){
        return cartService.findAll();
    }

    @PostMapping("/selectList")
    public List<Cart> selectList(List<String> goodsId){
        return cartService.selectList(goodsId);
    }

    @GetMapping("/findPage")
    public PageResult<Cart> findPage(int page, int size){
        return cartService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Cart> findList(@RequestBody Map<String,Object> searchMap){
        return cartService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Cart> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  cartService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Cart findById(String id){
        return cartService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Cart cart){
        //cart.setId(UUID.randomUUID().toString());
        cart.setAddTime(new Date());
        cartService.add(cart);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Cart cart){
        cartService.update(cart);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        cartService.delete(id);
        return new Result();
    }

}
