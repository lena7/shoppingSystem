package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Catagory;
import cn.web.service.shop.CatagoryService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/catagory")
public class CatagoryController {

    @Reference
    private CatagoryService catagoryService;

    @GetMapping("/findAll")
    public List<Catagory> findAll(){
        return catagoryService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Catagory> findPage(int page, int size){
        return catagoryService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Catagory> findList(@RequestBody Map<String,Object> searchMap){
        return catagoryService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Catagory> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  catagoryService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Catagory findById(String id){
        return catagoryService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Catagory catagory){
        catagoryService.add(catagory);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Catagory catagory){
        catagoryService.update(catagory);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        catagoryService.delete(id);
        return new Result();
    }

}
