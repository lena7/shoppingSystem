package cn.web.controller.shop;

import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.CatagoryShow;
import cn.web.service.shop.CatagoryShowService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/catagoryShow")
public class CatagoryShowController {

    @Reference
    private CatagoryShowService catagoryShowService;

    @GetMapping("/findAll")
    public List<CatagoryShow> findAll(){
        return catagoryShowService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<CatagoryShow> findPage(int page, int size){
        return catagoryShowService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<CatagoryShow> findList(@RequestBody Map<String,Object> searchMap){
        return catagoryShowService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<CatagoryShow> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  catagoryShowService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public CatagoryShow findById(String id){
        return catagoryShowService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody CatagoryShow catagoryShow){
        catagoryShowService.add(catagoryShow);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody CatagoryShow catagoryShow){
        catagoryShowService.update(catagoryShow);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        catagoryShowService.delete(id);
        return new Result();
    }

}
