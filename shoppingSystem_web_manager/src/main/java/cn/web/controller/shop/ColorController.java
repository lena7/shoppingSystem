package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Color;
import cn.web.service.shop.ColorService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/color")
public class ColorController {

    @Reference
    private ColorService colorService;

    @GetMapping("/findAll")
    public List<Color> findAll(){
        return colorService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Color> findPage(int page, int size){
        return colorService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Color> findList(@RequestBody Map<String,Object> searchMap){
        return colorService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Color> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  colorService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Color findById(String id){
        return colorService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Color color){
        colorService.add(color);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Color color){
        colorService.update(color);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        colorService.delete(id);
        return new Result();
    }

}
