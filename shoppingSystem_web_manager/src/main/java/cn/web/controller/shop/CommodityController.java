package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Commodity;
import cn.web.service.shop.CommodityService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/commodity")
public class CommodityController {

    @Reference
    private CommodityService commodityService;

    @GetMapping("/findAll")
    public List<Commodity> findAll(){
        return commodityService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Commodity> findPage(int page, int size){
        return commodityService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Commodity> findList(@RequestBody Map<String,Object> searchMap){
        return commodityService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Commodity> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  commodityService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Commodity findById(String id){
        return commodityService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Commodity commodity){
        commodityService.add(commodity);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Commodity commodity){
        commodityService.update(commodity);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        commodityService.delete(id);
        return new Result();
    }

}
