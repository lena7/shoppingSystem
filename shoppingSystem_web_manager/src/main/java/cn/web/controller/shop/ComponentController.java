package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Component;
import cn.web.service.shop.ComponentService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/component")
public class ComponentController {

    @Reference
    private ComponentService componentService;

    @GetMapping("/findAll")
    public List<Component> findAll(){
        return componentService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Component> findPage(int page, int size){
        return componentService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Component> findList(@RequestBody Map<String,Object> searchMap){
        return componentService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Component> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  componentService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Component findById(String id){
        return componentService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Component component){
        componentService.add(component);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Component component){
        componentService.update(component);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        componentService.delete(id);
        return new Result();
    }

}
