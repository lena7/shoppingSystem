package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Details;
import cn.web.service.shop.DetailsService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/details")
public class DetailsController {

    @Reference
    private DetailsService detailsService;

    @GetMapping("/findAll")
    public List<Details> findAll(){
        return detailsService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Details> findPage(int page, int size){
        return detailsService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Details> findList(@RequestBody Map<String,Object> searchMap){
        return detailsService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Details> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  detailsService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Details findById(String id){

        System.out.println(id);
        return detailsService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Details details){
        detailsService.add(details);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Details details){
        detailsService.update(details);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        detailsService.delete(id);
        return new Result();
    }

}
