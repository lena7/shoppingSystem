package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.DetailsImg;
import cn.web.service.shop.DetailsImgService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/detailsImg")
public class DetailsImgController {

    @Reference
    private DetailsImgService detailsImgService;

    @GetMapping("/findAll")
    public List<DetailsImg> findAll(){
        return detailsImgService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<DetailsImg> findPage(int page, int size){
        return detailsImgService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<DetailsImg> findList(@RequestBody Map<String,Object> searchMap){
        return detailsImgService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<DetailsImg> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  detailsImgService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public DetailsImg findById(String id){
        return detailsImgService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody DetailsImg detailsImg){
        detailsImgService.add(detailsImg);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody DetailsImg detailsImg){
        detailsImgService.update(detailsImg);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        detailsImgService.delete(id);
        return new Result();
    }

}
