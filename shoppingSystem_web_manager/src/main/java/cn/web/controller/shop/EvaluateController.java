package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Evaluate;
import cn.web.service.shop.EvaluateService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/evaluate")
public class EvaluateController {

    @Reference
    private EvaluateService evaluateService;

    @GetMapping("/findAll")
    public List<Evaluate> findAll(){
        return evaluateService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Evaluate> findPage(int page, int size){
        return evaluateService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Evaluate> findList(@RequestBody Map<String,Object> searchMap){
        return evaluateService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Evaluate> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  evaluateService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Evaluate findById(String id){
        return evaluateService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Evaluate evaluate){
        evaluateService.add(evaluate);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Evaluate evaluate){
        evaluateService.update(evaluate);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        evaluateService.delete(id);
        return new Result();
    }

}
