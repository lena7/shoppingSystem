package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Goods;
import cn.web.service.shop.GoodsService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @GetMapping("/findAll")
    public List<Goods> findAll(){
        return goodsService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Goods> findPage(int page, int size){
        return goodsService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Goods> findList(@RequestBody Map<String,Object> searchMap){
        return goodsService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Goods> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  goodsService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Goods findById(String id){
        return goodsService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Goods goods){
        goodsService.add(goods);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Goods goods){
        goodsService.update(goods);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        goodsService.delete(id);
        return new Result();
    }

}
