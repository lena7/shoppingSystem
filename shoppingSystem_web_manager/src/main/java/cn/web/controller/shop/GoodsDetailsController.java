package cn.web.controller.shop;

import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.GoodsDetails;
import cn.web.service.shop.GoodsDetailsService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/goodsDetails")
public class GoodsDetailsController {

	@Reference
	private GoodsDetailsService goodsDetailsService;

	@GetMapping("/findAll")
	public List<GoodsDetails> findAll(){

		return goodsDetailsService.findAll();
	}

	@GetMapping("/findPage")
	public PageResult<GoodsDetails> findPage(int page, int size){
		return goodsDetailsService.findPage(page, size);
	}

	@PostMapping("/findList")
	public List<GoodsDetails> findList(@RequestBody Map<String,Object> searchMap){
		return goodsDetailsService.findList(searchMap);
	}

	@PostMapping("/findPage")
	public PageResult<GoodsDetails> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
		return  goodsDetailsService.findPage(searchMap,page,size);
	}

	@GetMapping("/findById")
	public GoodsDetails findById(String id){
		return goodsDetailsService.findById(id);
	}


	@PostMapping("/add")
	public Result add(@RequestBody GoodsDetails goodsDetails){
		goodsDetailsService.add(goodsDetails);
		return new Result();
	}

	@PostMapping("/update")
	public Result update(@RequestBody GoodsDetails goodsDetails){
		goodsDetailsService.update(goodsDetails);
		return new Result();
	}

	@GetMapping("/delete")
	public Result delete(String id){
		goodsDetailsService.delete(id);
		return new Result();
	}

}
