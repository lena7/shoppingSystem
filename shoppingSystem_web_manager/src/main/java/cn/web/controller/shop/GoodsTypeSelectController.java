package cn.web.controller.shop;

import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.GoodsTypeSelect;
import cn.web.service.shop.GoodsTypeSelectService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/goodsTypeSelect")
public class GoodsTypeSelectController {

	@Reference
	private GoodsTypeSelectService goodsTypeSelectService;

	@GetMapping("/findAll")
	public List<GoodsTypeSelect> findAll(){
		return goodsTypeSelectService.findAll();
	}

	@GetMapping("/findPage")
	public PageResult<GoodsTypeSelect> findPage(int page, int size){
		return goodsTypeSelectService.findPage(page, size);
	}

	@PostMapping("/findList")
	public List<GoodsTypeSelect> findList(@RequestBody Map<String,Object> searchMap){
		return goodsTypeSelectService.findList(searchMap);
	}

	@PostMapping("/findPage")
	public PageResult<GoodsTypeSelect> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
		return  goodsTypeSelectService.findPage(searchMap,page,size);
	}

	@GetMapping("/findById")
	public GoodsTypeSelect findById(String id){
		return goodsTypeSelectService.findById(id);
	}


	@PostMapping("/add")
	public Result add(@RequestBody GoodsTypeSelect GoodsTypeSelect){
		goodsTypeSelectService.add(GoodsTypeSelect);
		return new Result();
	}

	@PostMapping("/update")
	public Result update(@RequestBody GoodsTypeSelect GoodsTypeSelect){
		goodsTypeSelectService.update(GoodsTypeSelect);
		return new Result();
	}

	@GetMapping("/delete")
	public Result delete(String id){
		goodsTypeSelectService.delete(id);
		return new Result();
	}
}
