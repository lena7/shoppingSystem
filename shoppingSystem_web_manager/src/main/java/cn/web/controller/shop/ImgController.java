package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Img;
import cn.web.service.shop.ImgService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/img")
public class ImgController {

    @Reference
    private ImgService imgService;

    @GetMapping("/findAll")
    public List<Img> findAll(){
        return imgService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Img> findPage(int page, int size){
        return imgService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Img> findList(@RequestBody Map<String,Object> searchMap){
        return imgService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Img> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  imgService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Img findById(String id){
        return imgService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Img img){
        imgService.add(img);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Img img){
        imgService.update(img);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        imgService.delete(id);
        return new Result();
    }

}
