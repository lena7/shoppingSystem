package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Login;
import cn.web.service.shop.LoginService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Reference
    private LoginService loginService;

/*    @PostMapping("/user")
    public int Login(String id,String password){
        return loginService.Login(id,password);
    }*/

    @GetMapping("/findAll")
    public List<Login> findAll(){
        return loginService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Login> findPage(int page, int size){
        return loginService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Login> findList(@RequestBody Map<String,Object> searchMap){
        return loginService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Login> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  loginService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Login findById(String id){
        return loginService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Login login){
        loginService.add(login);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Login login){
        loginService.update(login);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        loginService.delete(id);
        return new Result();
    }

}
