package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Logistics;
import cn.web.service.shop.LogisticsService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/logistics")
public class LogisticsController {

    @Reference
    private LogisticsService logisticsService;

    @GetMapping("/findAll")
    public List<Logistics> findAll(){
        return logisticsService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Logistics> findPage(int page, int size){
        return logisticsService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Logistics> findList(@RequestBody Map<String,Object> searchMap){
        return logisticsService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Logistics> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  logisticsService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Logistics findById(String id){
        return logisticsService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Logistics logistics){
        logistics.setSentAddress("6006");
        logistics.setReceiveAddress("6001");
        logistics.setStatus(0);
        logisticsService.add(logistics);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Logistics logistics){
        logisticsService.update(logistics);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        logisticsService.delete(id);
        return new Result();
    }

}
