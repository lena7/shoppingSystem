package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.MainImg;
import cn.web.service.shop.MainImgService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/mainImg")
public class MainImgController {

    @Reference
    private MainImgService mainImgService;

    @GetMapping("/findAll")
    public List<MainImg> findAll(){
        return mainImgService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<MainImg> findPage(int page, int size){
        return mainImgService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<MainImg> findList(@RequestBody Map<String,Object> searchMap){
        return mainImgService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<MainImg> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  mainImgService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public MainImg findById(String id){
        return mainImgService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody MainImg mainImg){
        mainImgService.add(mainImg);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody MainImg mainImg){
        mainImgService.update(mainImg);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        mainImgService.delete(id);
        return new Result();
    }

}
