package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Property;
import cn.web.service.shop.PropertyService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/property")
public class PropertyController {

    @Reference
    private PropertyService propertyService;

    @GetMapping("/findAll")
    public List<Property> findAll(){
        return propertyService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Property> findPage(int page, int size){
        return propertyService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Property> findList(@RequestBody Map<String,Object> searchMap){
        return propertyService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Property> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  propertyService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Property findById(String id){
        return propertyService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Property property){
        propertyService.add(property);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Property property){
        propertyService.update(property);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        propertyService.delete(id);
        return new Result();
    }

}
