package cn.web.controller.shop;

import cn.web.entity.Result;
import cn.web.pojo.shop.ReferalLink;
import cn.web.service.shop.ReferalLinkService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/referalLink")
public class ReferalLinkController {

	@Reference
	private ReferalLinkService referalLinkService;

	@GetMapping("/findAll")
	public List<ReferalLink> findAll(){
		return referalLinkService.findAll();
	}

	@GetMapping("/findById")
	public ReferalLink findById(String id){
		return referalLinkService.findById(id);
	}


	@PostMapping("/add")
	public Result add(@RequestBody ReferalLink ReferalLink){
		referalLinkService.add(ReferalLink);
		return new Result();
	}

	@PostMapping("/update")
	public Result update(@RequestBody ReferalLink ReferalLink){
		referalLinkService.update(ReferalLink);
		return new Result();
	}

	@GetMapping("/delete")
	public Result delete(String id){
		referalLinkService.delete(id);
		return new Result();
	}

}
