package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Shop;
import cn.web.service.shop.ShopService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/shop")
public class ShopController {

    @Reference
    private ShopService shopService;

    @GetMapping("/findAll")
    public List<Shop> findAll(){
        return shopService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Shop> findPage(int page, int size){
        return shopService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Shop> findList(@RequestBody Map<String,Object> searchMap){
        return shopService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Shop> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  shopService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Shop findById(String id){
        return shopService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Shop shop){
        shop.setOpenDate(new Date());
        shop.setQualification("已签署消保协议");
        shop.setEnterpriseQualification("营业执照");
        shop.setFeedbackRate(5.0);
        shop.setDescribeFit(5.0);
        shop.setServiceAttitude(5.0);
        shop.setLogisticsService(5.0);
        shop.setComprehensiveExperience(5.0);
        shop.setFansNumber(0);
        shop.setLevel(1);
        shopService.add(shop);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Shop shop){
        shopService.update(shop);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        shopService.delete(id);
        return new Result();
    }

}
