package cn.web.controller.shop;

import com.alibaba.dubbo.config.annotation.Reference;
import cn.web.entity.PageResult;
import cn.web.entity.Result;
import cn.web.pojo.shop.Size;
import cn.web.service.shop.SizeService;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/size")
public class SizeController {

    @Reference
    private SizeService sizeService;

    @GetMapping("/findAll")
    public List<Size> findAll(){
        return sizeService.findAll();
    }

    @GetMapping("/findPage")
    public PageResult<Size> findPage(int page, int size){
        return sizeService.findPage(page, size);
    }

    @PostMapping("/findList")
    public List<Size> findList(@RequestBody Map<String,Object> searchMap){
        return sizeService.findList(searchMap);
    }

    @PostMapping("/findPage")
    public PageResult<Size> findPage(@RequestBody Map<String,Object> searchMap,int page, int size){
        return  sizeService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Size findById(String id){
        return sizeService.findById(id);
    }


    @PostMapping("/add")
    public Result add(@RequestBody Size size){
        sizeService.add(size);
        return new Result();
    }

    @PostMapping("/update")
    public Result update(@RequestBody Size size){
        sizeService.update(size);
        return new Result();
    }

    @GetMapping("/delete")
    public Result delete(String id){
        sizeService.delete(id);
        return new Result();
    }

}
