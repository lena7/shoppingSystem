new Vue({
    el:"#app",
    data(){
        return {
            loginUser:{},
            finishOrder:[],
            applicationBack:[],
            nowOrder:{},
            bills:[],
            eacharsBill:{data:[],xData:[]},
        }
    },
    methods: {
        //t图表
        loadBillDataEcharts: function () {
            var option = {
                xAxis: {
                    type: 'category',
                  //  data: JSON.parse(sessionStorage.getItem("xData")),
                    data:JSON.parse(this.eacharsBill.xData),
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: JSON.parse(this.eacharsBill.data),
                    type: 'line',
                    smooth: true
                }]
            };
            var mycharts = echarts.init(document.getElementById("mycharts"));
            mycharts.setOption(option);
        },
        //t处理账单图表的数据
        dealBillData: function () {
            let j = this.bills.length;
            for (let i = 1; i < j; i++) {
                if (i < 11) {
                    console.log(i);
                    this.eacharsBill.xData.push(this.bills[j - i].time.substring(0, 10));
                    this.eacharsBill.data.push(this.bills[j - i].receiveAmount);
                }
            }
            this.eacharsBill.xData = JSON.stringify(this.eacharsBill.xData);
            this.eacharsBill.data = JSON.stringify(this.eacharsBill.data);
           // console.log("result:" + this.eacharsBill.xData);//t
           // console.log("xresult:" + this.eacharsBill.data);//t
            this.loadBillDataEcharts();

        },
        //t查询商家账单
        findBuinessBill: function () {
            axios.post(`/businessBill/findList.do`,{businessId: this.loginUser.id}).then(response => {
                this.bills = response.data;
                console.log(this.bills);
                this.dealBillData();    //处理数据，转化成图表所需要的格式
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询登录用户(有缓存：sessionStorage)
        findLoginUser: function () {
            let userId = sessionStorage.getItem("userId");
            console.log(userId);
            if (userId == "" || userId == null) {      //后台管理界面必须登录
                console.log("nologin");
                window.location.href = "/pages/login.html";
            } else {
                this.login = true;
                //查询用户具体信息
                axios.get(`/business/findById.do?id=${userId}`).then(response => {
                    this.loginUser = response.data;
                    // this.sumShop(); //计算购物车数量
                   // this.findUserShop();
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
    },
        created() {
            this.findLoginUser();
            console.log(this.loginUser);
            this.findBuinessBill();   //查询商家账单
            //this.InitData();
           // setTimeout(this.loadBillDataEcharts(),3000);
        },

    });