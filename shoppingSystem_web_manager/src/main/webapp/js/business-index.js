new Vue({
    el:"#app",
    data(){
        return {
            loginUser:{},
            shop:[],
            addaShop:{},
            nopay:[],
            noevaluate:[],
            sent:[],
            nosent:[],
            finishOrder:[],
            applicationBack:[],
            form:{id:'',company:''},
            dialogVisible:false,
            nowOrder:{},
            bills:[],
            eacharsBill:{data:[],xData:[]},

        }
    },
    methods:{
        //图标2
        loadBillDataEcharts2:function () {
            var option2={
                xAxis: {
                    type: 'category',
                    data: JSON.parse(sessionStorage.getItem("xData")),
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: JSON.parse(sessionStorage.getItem("data")),
                    type: 'bar',
                    showBackground: true,
                    backgroundStyle: {
                        color: 'rgba(220, 220, 220, 0.8)'
                    }
                }]
            };
            console.log(option);
            console.log("执行图表2了");
            var mycharts2=echarts.init(document.getElementById("mycharts2"));
            mycharts.setOption(option2);
        },
        //t图表
        loadBillDataEcharts:function () {
            var option = {
                xAxis: {
                    type: 'category',
                    data:JSON.parse(sessionStorage.getItem("xData")),
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data:JSON.parse(sessionStorage.getItem("data")),
                    type: 'line',
                    smooth: true
                }]
            };
            console.log(option);
            console.log("执行图表了");
            var mycharts=echarts.init(document.getElementById("mycharts"));
            mycharts.setOption(option);
        },
        //t处理账单图表的数据
        dealBillData:function () {
            let j=this.bills.length;
            for(let i=1;i<j;i++){
                if(i<11){
                    console.log(i);
                    this.eacharsBill.xData.push(this.bills[j-i].time.substring(0,10));
                    this.eacharsBill.data.push(this.bills[j-i].receiveAmount);
                }
            }
            this.eacharsBill.xData=JSON.stringify(this.eacharsBill.xData);
            this.eacharsBill.data=JSON.stringify(this.eacharsBill.data);
            console.log("result:"+this.eacharsBill.xData);//t
            console.log("xresult:"+this.eacharsBill.data);//t

        },
        //t查询商家账单
        findBuinessBill:function () {
            axios.post(`/businessBill/findList.do`,{businessId:this.loginUser.id}).then(response=>{
                this.bills=response.data;
                console.log(this.bills);
                this.dealBillData();    //处理数据，转化成图表所需要的格式
            }).catch(function (error) {
                console.log(error);
            });
        },
        //初始化函数
        InitData:function () {
            this.findLoginUser();
            console.log(this.loginUser);
            this.findNoPay();       //查询没有支付的订单
            this.findNoEvaluate();
            this.findNoSent();
            this.findBuinessBill(); //查询商家账单
            this.findSent();
            this.findApplicationBack();
            this.findFinish();
        },
        //t发货
        sentOrder:function (id) {
            this.dialogVisible = false;
            console.log(id);
            //t添加物流信息
            axios.post(`/logistics/add.do`,{id:this.form.id,expressCompany:this.form.company}).then(response=>{
                let code=response.data.code;
                if(code==0){
                    //查询出订单信息
                    axios.get(`/details/findById.do?id=${id}`).then(response=>{
                        this.nowOrder=response.data;
                      //  console.log("--"+this.nowOrder);
                        this.nowOrder.status=2;
                        this.nowOrder.logisticsId=this.form.id;
                        console.log("--"+this.nowOrder);
                        //修改订单信息
                        axios.post(`/details/update.do`,this.nowOrder).then(response=>{
                            let code1=response.data.code;
                            if (code1==0){
                                this.$notify({
                                    title: '成功',
                                    message: '添加物流信息成功！',
                                    type: 'success'
                                });
                            }
                            window.location.reload();   //刷新？           测试一下---
                        });
                    });
                }else{
                    this.$notify.error({
                        title: '异常',
                        message: '添加物流信息失败！'
                    });
                    this.findLoginUser();   //重新查询未修改的用户信息。
                }
            }).catch(function (error) {
                console.log(error);
            });


        },
        //t查询已完成订单6（商家id，没有过滤）
        findFinish:function () {
            axios.post(`/details/findList.do`,{status:6}).then(response=>{
                this.finishOrder=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询申请退款的订单7
        findApplicationBack:function () {
            axios.post(`/details/findList.do`,{status:7}).then(response=>{
                this.applicationBack=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询未发货订单1
        findNoSent:function () {
            axios.post(`/details/findList.do`,{status:1}).then(response=>{
                this.nosent=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询已发货订单2
        findSent:function () {
            axios.post(`/details/findList.do`,{status:2}).then(response=>{
                this.sent=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询没有评价的订单3
        findNoEvaluate:function () {
            axios.post(`/details/findList.do`,{status:3}).then(response=>{
                this.noevaluate=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询没有支付的订单status=0
        findNoPay:function () {
            axios.post(`/details/findList.do`,{status:0}).then(response=>{
                this.nopay=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t更新用户信息
        updateUser:function () {
            let name=document.getElementById("name").value;
            console.log(name)
            if (name!="") this.loginUser.name=name;
            name=document.getElementById("inputEmail3").value;
            if(name!="") this.loginUser.email=name;
            name=document.getElementById("brief").value;
            if(name!="") this.loginUser.brief=name;
            name=document.getElementById("phone").value;
            if(name!="") this.loginUser.phoneNumber=name;
            name=document.getElementById("living").value;
            if(name!="")  this.loginUser.living=name;
            axios.post(`/business/update.do`,this.loginUser).then(response=>{
                let code=response.data.code;
                if(code==0){
                    this.$notify({
                        title: '成功',
                        message: '修改个人信息成功！',
                        type: 'success'
                    });
                }else{
                    this.$notify.error({
                        title: '异常',
                        message: '修改个人信息失败！'
                    });
                    this.findLoginUser();   //重新查询未修改的用户信息。
                }
                this.findUserShop();
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t添加店铺
        addShop:function () {
            //获取表单
            this.addaShop.id=this.uuid();
            this.addaShop.name=document.getElementById("inputEmail1").value;
            this.addaShop.businessId=this.loginUser.id;
            //提交到数据库
            axios.post(`/shop/add.do`,this.addaShop).then(response=>{
                let code=response.data.code;
                console.log(code);
                if(code==0){
                    this.$notify({
                        title: '成功',
                        message: '添加店铺成功！',
                        type: 'success'
                    });
                }else{
                    this.$notify.error({
                        title: '异常',
                        message: '添加店铺失败！'
                    });
                }
                this.findUserShop();
            }).catch(function (error) {
                console.log(error);
            });
        },
        //跳转到店铺页面
        toShop:function (id) {
            window.location.href="/pages/shop/one.html?id="+id;
        },
        //t查询登录用户(有缓存：sessionStorage)
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
            console.log(userId);
            if (userId==""||userId==null){      //后台管理界面必须登录
                console.log("nologin");
                window.location.href="/pages/login.html";
            }else{
                this.login=true;
                //查询用户具体信息
                axios.get(`/business/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                   // this.sumShop(); //计算购物车数量
                    this.findUserShop();
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t查询该用户有的店铺,
        findUserShop:function () {
           // console.log(this.loginUser.id);
            axios.post(`/shop/findList.do`,{businessId:this.loginUser.id}).then(response=>{
                this.shop=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t随机生成id
        uuid:function() {
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 36; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
            s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
            s[8] = s[13] = s[18] = s[23] = "-";

            var uuid = s.join("");
            return uuid;
        }
    },
    created() {
        this.InitData();
    },
    mounted(){
        //window.findUserShop=this.findUserShop;
        window.loadBillDataEcharts=this.loadBillDataEcharts;
    },
    //关闭前执行的方法
    beforeDestroy(){
        sessionStorage.clear();
    }
});