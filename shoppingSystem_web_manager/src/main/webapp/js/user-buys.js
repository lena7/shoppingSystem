new Vue({
    el:"#app",
    data(){
        return {
            cart:[],
            goodsId:[],
            goodsDetail:[],
            cartId:[],
            shopNum:0,
            total:0,    //总金额
            goodsMainImg:[],
            login:true,
            loginUser:{},
            address:{},     //当前使用地址
            addressData:{},     //该用户所有地址
            addressInput:false,     //自己手动输入地址
            dialogTableVisible: false,
            selectAddress:{},       //选择的收货地址id
        }


    },
    methods:{
        //查询商品
        searchGoods:function () {
            let keyword=document.getElementById("keyword").value;    //获取输入的值
            sessionStorage.setItem("keyword",keyword);
            window.location.href="/pages/user/searchGoods.html";
        },
        //提交订单
        topay:function () {
            sessionStorage.setItem("money",this.total);
            window.location.href="/pages/user/pay.html";
        },
        //t通过cartID查询单个购物连接
        findCartById:function () {
            for(let j=0;j<this.cartId.length;j++) {
                axios.get(`/cart/findById.do?id=${this.cartId[j]}`).then(response => {
                     this.cart.splice(j,0,response.data);
                     this.goodsId.splice(j,0,response.data.goodsId);
                     this.total=this.total+response.data.money; //计算总金额
                    console.log(this.total);
                }).catch(function (error) {
                    console.log(error);
                });
            }
            setTimeout(this.findGoodsbyId,1500);
            setTimeout(this.findGoodMainImg,1500);
        },
        //t查询商品详情信息
        findGoodsbyId:function () {
            for(let i=0;i<this.goodsId.length;i++) {
               // console.log("i"+i+"==="+this.goodsId[i]);
                axios.get(`/goodsDetails/findById.do?id=${this.goodsId[i]}`).then(response => {
                    this.goodsDetail.splice(i,0,response.data);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t查询商品主图
        findGoodMainImg:function () {
            for(let k=0;k<this.goodsId.length;k++){
                axios.get(`/mainImg/findById.do?id=${this.goodsId[k]}`).then(response=>{
                //    console.log("k"+k+"==="+this.goodsId[k]);
                    this.goodsMainImg.splice(k,0,response.data.img1);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t初始化
        InitData:function () {
            this.findLoginUser();   //查询登录用户
            let carts=sessionStorage.getItem("cartId");
            this.cartId=carts.split(",");   //按“，”切割数组
            console.log("购物车id："+this.cartId);
            this.findCartById();
        },
        //t查询登录用户
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
            if (userId==""||userId==null){
                console.log("nologin");
                this.login=false;
                window.location.href="/pages/login.html";       //跳转到登陆页面
            }else{
                this.login=true;
                //查询用户具体信息
                axios.get(`/user/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                    this.findAddress(); //查询默认收货地址
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t退出登录
        logout:function () {
            sessionStorage.clear();
            this.login=false;
            this.loginUser={};
            window.location.href="/pages/login.html";
        },
        //t输入新地址
        newAdd:function () {
            this.addressInput = true;  //显示输入框
            this.dialogTableVisible = false;
        },
        /**
         * t修改收货地址
         * 根据selectaddress的值进行修改
         * 将地址的地址值赋值给address即可
         */
        changeAdd:function () {
            this.address=this.selectaddress;    //赋值用户选择的地址
            console.log("修改了地址："+this.address.id);  //Cannot read property 'id' of undefined"
            this.dialogTableVisible = false;
        },
        /**
         * t查询默认收货地址
         * 每个用户创建的第一个地址都会是默认地址。只会有该用户没有添加地址、或者存在默认地址的情况
         * 若用户没有添加地址，则将addressInput设置为true，让用户自己输入地址。
         */
        findAddress:function () {
            axios.post(`/address/findList.do`,{userId:this.loginUser.id,isDefault:1}).then(response=>{
                if (response.data.length!=0){       //存在默认地址
                    this.addressInput=false;
                    this.address=response.data[0];     //存储默认地址为当前显示地址
                }
                else{
                    this.addressInput=true; //让用户手动输入地址
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询该用户的所有收货地址
        findAddressData:function () {
            if(this.addressData.length>0){
                this.dialogTableVisible = true;
                return;
            }else{
                axios.post(`/address/findList.do`,{userId:this.loginUser.id}).then(response=>{
                    this.addressData=response.data;
                    this.dialogTableVisible = true;     //查询完毕才显示对话框
                }).catch(function (error) {
                    console.log(error);
                });
            }

        },
        //elementui自带方法
        setCurrent(row) {       //设置当前选择的行
            this.$refs.singleTable.setCurrentRow(row);
        },
        //t
        handleCurrentChange(val) {      //修改行的时候执行的方法
            this.currentRow = val;
            if(val===undefined){        //取消选择行
                this.selectaddress={};  //当前选择为空
            }else{
                this.selectaddress=val;      //将收货地址id存储起来
                //console.log("当前选择地址："+this.selectaddress.id);
            }
        }
        //根据color，size查询价格。==========================================
    },
    created() {
        this.InitData();
    },
    destroy(){
        sessionStorage.setItem("cartId","");
    }
});