new Vue({
    el:"#app",
    data(){
        return {
            login:true,
            shopNum:0,
            loginUser:{},
            carts:[],   //该用户购物车的所有商品
            goodsDetail:[],    //商品详情
            goodsMainImg:[],    //商品主图
            multipleSelection: [],
            total:0,    //当前已选商品的总金额
            checks:[],

        }
    },
    methods:{
        //计算总金额
        sumTotal:function () {
            //返回一个数组，注意第0位是全选框的值，实际对应商品要-1
          let c=sessionStorage.getItem("checks");
          let checks=c.split(",");
          this.total=0;
          for(let i=0;i<this.carts.length;i++){
              if(checks[i]=="1"){
                this.total=this.total+this.carts[i].money;      //添加金额
              }
          }
        },
        //初始化函数
        InitData:function () {
            this.findLoginUser();
            setTimeout(this.findAllCarts(),2000);        //1s后执行，避免还没查询完登录用户。
        },
        //用户选择后提交订单的时候
        submieSelect:function () {
            //首先获取选择的cartId的
            let c=sessionStorage.getItem("checks");
            let checks=c.split(",");
            let a=new Array();
            this.total=0;
            for(let i=0;i<this.carts.length;i++){
                if(checks[i]=="1"){
                    a.splice(i,1,this.carts[i].id);     //添加金额
                }
            }
            //将cartId存入Session中。
            sessionStorage.setItem("cartId",a);
            //跳转到buys.html页面。
            window.location.href="/pages/user/buy.html";
        },
        //当点击删除按钮的时候，删除该购物车记录。需要传入id值
        delCart:function (id) {
            console.log(id);
            axios.get(`/cart/delete.do?id=${id}`).then(response=>{
                let code1=response.data.code;
                if (code1==0){
                    this.$notify({
                        title: '成功',
                        message: '删除成功！',
                        type: 'success'
                    });
                    location.reload();
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        //查询商品
        searchGoods:function () {
            let keyword=document.getElementById("keyword").value;    //获取输入的值
            sessionStorage.setItem("keyword",keyword);
            window.location.href="/pages/user/searchGoods.html";
        },
        //t查询用户所有购物车商品
        findAllCarts:function () {
           // console.log(this.loginUser.id);
            axios.post(`/cart/findList.do`,{userId:this.loginUser.id}).then(response=>{
            //    console.log(this.loginUser.id);
                this.carts=response.data;   //查询出的商品只有商品id，应该根据商品id在查询商品详情.
                this.findAllGoods();    //查询商品详情
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t根据carts中的goodsId，查询商品详情
        findAllGoods:function () {
          for(let i=0;i<this.carts.length;i++){
              axios.get(`/goodsDetails/findById.do?id=${this.carts[i].goodsId}`).then(response => {
                  this.goodsDetail.splice(i,0,response.data);  //将第i个数据存入数组索引为i处。
              }).catch(function (error) {
                  console.log(error);
              });
          }
          this.findGoodMainImg();   //查询商品主图
        },
        //t查询商品主图
        findGoodMainImg:function () {
            for(let k=0;k<this.carts.length;k++){
                axios.get(`/mainImg/findById.do?id=${this.carts[k].goodsId}`).then(response=>{
                    console.log("k"+k+"==="+this.carts[k].goodsId);
                    this.goodsMainImg.splice(k,0,response.data.img1);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        /**
         * t计算购物车数量
         * 在用户登录之后才会调用该方法，如果没有登录就不计算购物车数量。
         */
        sumShop:function () {
            axios.post(`/cart/findList.do`,{userId:this.loginUser.id}).then(response=>{
                this.shopNum=response.data.length;
                 console.log("购物车："+this.shopNum+"---"+this.loginUser.id);
                console.log(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询登录用户
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
          //  let userId="1001";    //测试需要（测试成功后删除即可）==========================================================================
            if (userId==""||userId==null){
                console.log("nologin");
                this.login=false;
                window.location.href="/pages/login.html"; //由于测试需要暂不演示。
            }else{
                this.login=true;
                this.loginUser.id=userId;   //先赋值
                console.log("login");
                //查询用户具体信息
                axios.get(`/user/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                    console.log(userId);
                    this.sumShop(); //计算购物车数量
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t退出登录
        logout:function () {
            sessionStorage.clear();
            this.login=false;
            this.loginUser={};
            window.location.href="/pages/index.html";       //重新加载页面？
        },
        //elementui选择器方法
        toggleSelection(rows) {
            if (rows) {
                rows.forEach(row => {
                    this.$refs.multipleTable.toggleRowSelection(row);
                });
            } else {
                this.$refs.multipleTable.clearSelection();
            }
        },
        //修改所有状态
        toggleSelectionAll(obj) {
            console.log("全选");
            console.log(obj);
            $(".checkstatus").prop("checked", obj.checked);
        },
        handleSelectionChange(val) {
            this.multipleSelection = val;
        },
    },
    created() {
        this.InitData();
    }
});