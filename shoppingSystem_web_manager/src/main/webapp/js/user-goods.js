new Vue({
    el:"#app",
    data(){
        return {
            goods:{},
            goodsMainImg:{},
            goodsDetailImg:{},        //商品详情页图片
            evalueNum:0,    //计算累计评价(未实现)
            evalueList:[],      //评价列表
            equal:false,
            color:{},   //需要创建新的类创建映射关系
            size:{},
            cart:{id:'',userId:'',goodsId:'',color:'黑色',size:'M',number:1,addTime:''},
            message:'',
            properties:{},
            loginUser:{},
            login:false,    //是否登录
            num8: 1,    //购买数量选择
            cart:{},
            referalLink:[],
            shopNum:0,
          //  nowBigImg:'',
            //searchMap:{},
        }
    },
    methods:{
        //查询商品
        searchGoods:function () {
            let keyword=document.getElementById("keyword").value;    //获取输入的值
            sessionStorage.setItem("keyword",keyword);
            window.location.href="/pages/user/searchGoods.html";
        },
        //t退出登录
        logout:function () {
            sessionStorage.clear();
            this.login=false;
            this.loginUser={};
            window.location.href="/pages/index.html";       //重新加载页面？
        },
        InitData:function () {
            QueryString.Init();     //初始化
            let goodsId=QueryString.GetValue("goodsId");
            this.findLoginUser();  //查询当前登录用户信息
            this.findGoodsById(goodsId);    //查询产品详情信息。
            this.findGoodMainImg(goodsId);  //查询主页图片
            this.findEvaluateAll(goodsId);     //查询评价
            this.findGoodsDetailImg(goodsId);      //查询详情页图片
            this.findPropertiesValue(goodsId);  //查询参数值
            this.findLink();        //头部显示连接
        },
        //t头部显示连接
        findLink:function () {
            axios.get(`/referalLink/findAll.do`).then(response=>{
                this.referalLink=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        /**
         * t计算购物车数量
         * 在用户登录之后才会调用该方法，如果没有登录就不计算购物车数量。
         */
        sumShop:function () {
            axios.post(`/cart/findList.do`,{userId:this.loginUser.id}).then(response=>{
                this.shopNum=response.data.length;
                // console.log("购物车："+this.shopNum+"---"+this.loginUser);
                console.log(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询登录用户
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
            if (userId==""||userId==null){
                console.log("nologin");
                this.login=false;
            }else{
                this.login=true;
                //查询用户具体信息
                axios.get(`/user/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                    this.sumShop();
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t根据商品ID查询商品详情
        findGoodsById:function (goodsId) {
            //console.log(goodsId);
            axios.get(`/goodsDetails/findById.do?id=${goodsId}`).then(response=>{
                this.goods=response.data;
              //  console.log(this.goods);
                if (this.goods.lowPrice==this.goods.highPrice){
                    this.equal=true;
                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询商品评价
        findEvaluateAll:function (goodsId) {
            //this.searchMap.goodsId=goodsId;
            axios.post(`/evaluate/findList.do`,{goodsId:goodsId}).then(response=>{
                this.evalueList=response.data;
                this.evalueNum=response.data.length;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询商品主图
        findGoodMainImg:function (id) {
            axios.get(`/mainImg/findById.do?id=${id}`).then(response=>{
                this.goodsMainImg=response.data;
               // this.nowBigImg=this.goodsMainImg.img1;
               // console.log(this.goodsMainImg);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询商品详情页图片
        findGoodsDetailImg:function (goodsId) {
            axios.get(`/detailsImg/findById.do?id=${goodsId}`).then(response=>{
                this.goodsDetailImg=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //查询参数
        findProperties:function () {
            axios.get(`/property/findAll.do`).then(response=>{
                this.properties.name=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询参数值
        findPropertiesValue:function (goodsId) {
            axios.get(`/component/findById.do?id=${goodsId}`).then(response=>{
                this.properties=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t修改主图
        changeBigImg:function (a) {
            switch(a){
                //修改url  $("bigImgMain").attr("src",this.goodsMainImg.img1)
                case 1:$("#bigImgMain").attr("src",this.goodsMainImg.img1);break;
                case 2:$("#bigImgMain").attr("src",this.goodsMainImg.img2);break;
                case 3:$("#bigImgMain").attr("src",this.goodsMainImg.img3);break;
                case 4:$("#bigImgMain").attr("src",this.goodsMainImg.img4);break;
                case 5:$("#bigImgMain").attr("src",this.goodsMainImg.img5);break;
            }
        },
        //t立即购买
        buyLink:function () {
            if (!this.login){       //当前浏览者没有登录
                this.$alert('登录成功才能进行购买，点击确定可跳转登录界面。', '请先登录', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'error'   //异常标志
                }).then(() => {
                    window.location.href="/pages/login.html";
                });
            }else{
                this.cart.color=1;          //通过获取元素的选择==========================================
                this.cart.size=1;
                this.cart.id=this.genID(12);
                this.cart.userId=this.loginUser.id;
                this.cart.goodsId=this.goods.id;
                this.cart.number=this.num8;
                this.cart.money=(this.num8)*(this.goods.lowPrice);
                //先将想购买的参数添加进购物车中，然后再根据购物车ID跳转到购买页面
                axios.post(`/cart/add.do`,this.cart).then(response=>{
                    let result=response.data.code;   //添加
                    console.log(result);
                    if (result==0){     //成功执行
                        sessionStorage.setItem("cartId",this.cart.id);
                        window.location.href="/pages/user/buy.html";
                    }else{
                        alert("操作失败，请重新尝试！")
                    }
                }).catch(function (error) {
                    console.log(error);
                });

            }
        },
        //添加到购物车
        addToCart:function () {
            if (!this.login){       //当前浏览者没有登录
                this.$alert('登录成功才能添加到购物车，点击确定可跳转登录界面。', '请先登录', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'error'   //异常标志
                }).then(() => {
                    window.location.href="/pages/login.html";  //上一级？=================================
                });
            }else{
                this.cart.userId=this.loginUser.id;
                this.cart.goodsId=this.goods.id;
                this.cart.color=1;      //========================================================
                this.cart.size=1;
                this.cart.number=this.num8;
                axios.post(`/cart/add.do`,this.cart).then(response=>{
                    let code=response.data.code;
                    if(code==0){
                        this.$notify({
                            title: '成功',
                            message: '添加商品到购物车成功！',
                            type: 'success'
                        });
                    }else{
                        this.$notify.error({
                            title: '异常',
                            message: '添加商品到购物车失败！'
                        });
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //数量选择器数据的变化
        handleChange(value) {
            console.log(value);
        },
        //t随机生成id
        genID(length){
            return Number(Math.random().toString().substr(3,length) + Date.now()).toString(36);
        }
    },
    created() {
        this.InitData();
    }
});