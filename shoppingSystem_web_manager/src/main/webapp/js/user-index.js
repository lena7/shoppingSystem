new Vue({
    el:"#app",
    data(){
        return {
            login:true,
            referalLink:[],
            shopNum:4,
            loginUser:{},
            catagoryshow:[],
            goods5:[],
        }
    },
    methods:{
        //查询商品
        searchGoods:function () {
            let keyword=document.getElementById("keyword").value;    //获取输入的值
            sessionStorage.setItem("keyword",keyword);
            window.location.href="/pages/user/searchGoods.html";
        },
        //初始化
        InitData:function () {
            this.findLink();
            this.findLoginUser();
            this.findCatagoryShow();
            this.findGoodsTop5();   //查找前五个商品
        },
        //t查找前五个商品
        findGoodsTop5:function () {
            axios.get(`/goodsDetails/findPage.do?page=1&size=5`).then(response=>{
                this.goods5=response.data.rows;
             //   console.log(this.goods5);
               // this.findGoodsTop5Img();
            }).catch(function (error) {
                console.log(error);
            });
        },
        //跳转到商品界面
        toGoods:function (id) {
            window.location.href="/pages/user/goods.html?goodsId="+id;
        },
        //t头部显示连接
        findLink:function () {
            axios.get(`/referalLink/findAll.do`).then(response=>{
                this.referalLink=response.data;
               // console.log(this.referalLink);
            }).catch(function (error) {
                console.log(error);
            });
        },
        /**
         * t计算购物车数量
         * 在用户登录之后才会调用该方法，如果没有登录就不计算购物车数量。
         */
        sumShop:function () {
            axios.post(`/cart/findList.do`,{userId:this.loginUser.id}).then(response=>{
                this.shopNum=response.data.length;
               // console.log("购物车："+this.shopNum+"---"+this.loginUser);
                console.log(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询分类名
        findCatagoryShow:function () {
            axios.get(`/catagoryShow/findAll.do`).then(response=>{
                this.catagoryshow=response.data;
                //console.log(this.catagoryshow);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询登录用户
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
            if (userId==""||userId==null){
                console.log("nologin");
                this.login=false;
            }else{
                this.login=true;
                //查询用户具体信息
                axios.get(`/user/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                    this.sumShop(); //计算购物车数量
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t主页分类详情显示 鼠标移入触发事件
        showProductsByCategoryId:function(category_id) {
            //console.log("show:"+category_id);
            $("div.hot-word-con[category_id=" + category_id + "]").show();
        },
        //t主页分类详情显示 鼠标移出触发事件
        hideProductsByCategoryId:function(category_id) {
            //console.log("hide:" + category_id);
            $("div.hot-word-con[category_id=" + category_id + "]").hide();
        },
        //退出登录
        logout:function () {
            sessionStorage.clear();
            this.login=false;
            this.loginUser={};
            window.location.href="/pages/login.html";
        },

    },
    created(){
        this.InitData();
    }
});