new Vue({
    el:"#app",
    data(){
        return {
            login:true,
            shopNum:0,
            loginUser:{},
            activeName: 'fourth',    //当前页面是全部订单
            // status:-1,
            orderData:[],   //当前订单数据
            goodsDetail:[],      //商品详情
            //  goodsId:[],     //商品id
            goodsMainImg:[],    //商品主图
        }
    },
    methods:{
        handleClick(tab, event) {
            console.log("label:"+tab.label);    //可以获取到label的标签
            let label=tab.label;
            if(label=="待付款"){
                window.location.href="order-nopay.html";
            }else if(label=="待发货"){
                window.location.href="order-nosent.html";
            }else if(label=="待收货"){
                window.location.href="order-norecieve.html";
            }else if(label=="待评价"){
                window.location.href="order-noevaluate.html";
            }else{
                window.location.href="order.html"; //显示全部订单
            }
            // setTimeout(this.findOrderByStatus(),1000);      //查询订单
        },
        //根据status查询订单（待付款）
        findOrderByStatus:function () {
            axios.post(`/details/findList.do`,{userId:this.loginUser.id,status:2}).then(response=>{
                this.orderData=response.data;   //获得订单数据
                console.log(this.orderData);
                this.findAllGoods();    //查询商品详情
            }).catch(function (error) {
                console.log(error);
            });
        },
        //根据商品id查询商品details
        findAllGoods:function () {
            for(let i=0;i<this.orderData.length;i++){
                axios.get(`/goodsDetails/findById.do?id=${this.orderData[i].goodsId}`).then(response => {
                    //    console.log("i"+i+"==="+this.orderData[i].goodsId);
                    this.goodsDetail.splice(i,0,response.data);  //将第i个数据存入数组索引为i处。
                })/*.catch(function (error) {
                    console.log(error);
                });*/
            }
            this.findGoodMainImg();   //查询商品主图
        },
        //t查询商品主图
        findGoodMainImg:function () {
            for(let k=0;k<this.orderData.length;k++){
                axios.get(`/mainImg/findById.do?id=${this.orderData[k].goodsId}`).then(response=>{
                    //     console.log("k"+k+"==="+this.orderData[k].goodsId);
                    this.goodsMainImg.splice(k,0,response.data.img1);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //查询商品
        searchGoods:function () {
            let keyword=document.getElementById("keyword").value;    //获取输入的值
            sessionStorage.setItem("keyword",keyword);
            window.location.href="/pages/user/searchGoods.html";
        },
        InitData:function () {
            this.findLoginUser();
            // this.status=0;
            setTimeout(this.findOrderByStatus(),2500);
        },
        /**
         * t计算购物车数量
         * 在用户登录之后才会调用该方法，如果没有登录就不计算购物车数量。
         */
        sumShop:function () {
            axios.post(`/cart/findList.do`,{userId:this.loginUser.id}).then(response=>{
                this.shopNum=response.data.length;
                // console.log("购物车："+this.shopNum+"---"+this.loginUser);
                console.log(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询登录用户
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
            if (userId==""||userId==null){
                console.log("nologin");
                this.login=false;
            }else{
                this.login=true;
                //查询用户具体信息
                axios.get(`/user/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                    this.sumShop(); //计算购物车数量
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t退出登录
        logout:function () {
            sessionStorage.clear();
            this.login=false;
            this.loginUser={};
            window.location.href="/pages/login.html";
        },
    },
    created() {
        this.InitData();
    }
});