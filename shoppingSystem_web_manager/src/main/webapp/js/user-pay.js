new Vue({
    el:"#app",
    data(){
        return {
            login:true,
            referalLink:[],
            shopNum:0,
            loginUser:{},
            money:0,
        }
    },
    methods:{
        InitData:function () {
            this.findLoginUser();
            this.money=sessionStorage.getItem("money");
        },
        //查询商品
        searchGoods:function () {
            let keyword=document.getElementById("keyword").value;    //获取输入的值
            sessionStorage.setItem("keyword",keyword);
            window.location.href="/pages/user/searchGoods.html";
        },
        /**
         * t计算购物车数量
         * 在用户登录之后才会调用该方法，如果没有登录就不计算购物车数量。
         */
        sumShop:function () {
            axios.post(`/cart/findList.do`,{userId:this.loginUser.id}).then(response=>{
                this.shopNum=response.data.length;
                // console.log("购物车："+this.shopNum+"---"+this.loginUser);
                console.log(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询登录用户
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
            if (userId==""||userId==null){
                console.log("nologin");
                this.login=false;
            }else{
                this.login=true;
                //查询用户具体信息
                axios.get(`/user/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                    this.sumShop(); //计算购物车数量
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //t退出登录
        logout:function () {
            sessionStorage.clear();
            this.login=false;
            this.loginUser={};
            window.location.href="/pages/login.html";
        },
    },
    created() {
        this.InitData();
    }
});