new Vue({
    el:"#app",
    data(){
        return {
            user:{
                realName:'',
                name:'',    //昵称
                phoneNumber:'',
                sex:"男",
                email:'',
                brief:'',       //个人签名
            },
            login:{
                id:'',      //唯一表示ID，使用昵称或者手机号都可
                password:'',   //密码
                status:1
            }

        }
    },
    methods:{
        registUser:function () {
            this.user.sex=$("input[name='sex']:checked").val(); //获取性别的取值

        },
        checkRealName:function () {
            let realnamevalue=document.getElementById("realname").value;
            if (realnamevalue==""){
                $("#form1").html("真实姓名不能为空");
                return false;
            }
            let reg=/^[\xa0-\xff]{2,4}$/;
            if(reg.test(realnamevalue)){
                $("#form1").html("");
                //window.sessionStorage.setItem("id",id);
                this.user.realName=realnamevalue;
                return true;
            }else{
                //显示提示信息
                $("#form1").html("请输入6位纯数字");
                return false;
            }
        },
        checkName:function () {
            let namevalue=document.getElementById("realname").value;
            if (namevalue==""){
                $("#form2").html("昵称不能为空");
                return false;
            }
            let reg=/^\w{0,20}$/;
            if(reg.test(namevalue)){
                $("#form1").html("");
                //window.sessionStorage.setItem("id",id);
                this.user.name=namevalue;
                return true;
            }else{
                //显示提示信息
                $("#form1").html("请输入20字符以内的昵称");
                return false;
            }
        },
        checkPassword:function () {
            let passwordvalue=document.getElementById("password").value;
            if (passwordvalue==""){
                $("#form3").html("密码不能为空");
                return false;
            }
            let reg=/^\d{6}$/;
            if(reg.test(passwordvalue)){
                $("#form3").html("");
                this.login.password=passwordvalue;
                return true;
            }else{
                //显示提示信息
                $("#form3").html("密码格式不正确，请输入6位纯数字的密码");
                return false;
            }
        },
        checkRepassword:function () {
            let rpasswordvalue=document.getElementById("rpassword").value;
            if (rpasswordvalue==""){
                $("#form4").html("请再次输入密码");
                return false;
            }
            let password=document.getElementById("password").value;
            if(password==rpasswordvalue){
                $("#form4").html("");
                //window.sessionStorage.setItem("rpassword",rpassword);
                this.login.password=rpasswordvalue;
                return true;
            }else{
                //显示提示信息
                $("#form4").html("两次输入密码不一致，请重新输入");
                return false;
            }
        },
        checkPhone:function () {
            let phonevalue=document.getElementById("phone").value;
            if (phonevalue==""){
                $("#form5").html("支付宝账号不能为空");
               // $("#form5").html("");
                return false;
            }
            let reg= /^[1][3,4,5,7,8][0-9]{9}$/;
            if(reg.test(phonevalue)){
                $("#form5").html("");
                this.user.phoneNumber=phonevalue;
                return true;
            }else{
                //显示提示信息
                $("#form5").html("输入支付宝账号的格式不正确");
                return false;
            }
        },
        checkEmail:function () {
            let emailvalue=document.getElementById("email").value;
            if (emailvalue==""){
                $("#form6").html("");
                this.user.email='';
                return true;
            }
            let reg=/^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
            if(reg.test(emailvalue)){
                $("#form6").html("");
                this.user.email=emailvalue;
                return true;
            }else{
                //显示提示信息
                $("#form6").html("请输入正确的邮箱格式");
                return false;
            }
        },
        checkBrief:function () {
            let briefvalue=document.getElementById("brief").value;
            if (briefvalue==""){
                $("#form7").html("");
                this.user.brief='';
                return true;
            }
            let reg=/^\w{0,50}$/;
            if(reg.test(briefvalue)){
                $("#form6").html("");
                this.user.brief=briefvalue;
                return true;
            }else{
                //显示提示信息
                $("#form6").html("请不要超过50个字符");
                return false;
            }
        }
    },
    created() {

    },
    mounted(){

    },
    //关闭前执行的方法
    beforeDestroy(){

    }
});