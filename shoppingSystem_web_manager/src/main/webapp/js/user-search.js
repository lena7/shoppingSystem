new Vue({
    el:"#app",
    data(){
        return {
            login:true,
            referalLink:[],
            shopNum:0,
            loginUser:{},
            keyword:"搜天猫",
            isEmply:false,
            goodsMainImg:[],
            goodsData:[],
        }
    },
    methods:{
        //查询商品
        searchGoods:function () {
            //获取符合条件的产品
            axios.post(`/goodsDetails/findList.do`,{title:this.keyword}).then(response=>{
                console.log(this.keyword);
                this.goodsData=response.data;
                if(this.goodsData.length==0){
                    this.isEmply=true;
                }else{
                    this.isEmply=false;
                }
                console.log(this.goodsData);
                this.findGoodMainImg();
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询商品主图
        findGoodMainImg:function () {
            for(let k=0;k<this.goodsData.length;k++){
                axios.get(`/mainImg/findById.do?id=${this.goodsData[k].id}`).then(response=>{
                    this.goodsMainImg.splice(k,0,response.data.img1);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        //初始化
        InitData:function () {
            this.findLoginUser();
          //  this.keyword= sessionStorage.getItem("keyword");
            this.keyword="鞋";
            setTimeout(this.searchGoods(),1500);
        },
        //t头部显示连接
/*        findLink:function () {
            axios.get(`/referalLink/findAll.do`).then(response=>{
                this.referalLink=response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },*/

        /**
         * t计算购物车数量
         * 在用户登录之后才会调用该方法，如果没有登录就不计算购物车数量。
         */
        sumShop:function () {
            axios.post(`/cart/findList.do`,{userId:this.loginUser.id}).then(response=>{
                this.shopNum=response.data.length;
                // console.log("购物车："+this.shopNum+"---"+this.loginUser);
                console.log(response.data);
            }).catch(function (error) {
                console.log(error);
            });
        },
        //t查询登录用户
        findLoginUser:function () {
            let userId=sessionStorage.getItem("userId");
            if (userId==""||userId==null){
                console.log("nologin");
                this.login=false;
            }else{
                this.login=true;
                //查询用户具体信息
                axios.get(`/user/findById.do?id=${userId}`).then(response=>{
                    this.loginUser=response.data;
                    this.sumShop(); //计算购物车数量
                }).catch(function (error) {
                    console.log(error);
                });
                console.log(this.loginUser.id);
            }
        },
        //t退出登录
        logout:function () {
            sessionStorage.clear();
            this.login=false;
            this.loginUser={};
            window.location.href="/pages/login.html";
        },
    },
    created() {
        this.InitData();
    }
});