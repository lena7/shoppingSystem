/*
 * 日期格式化
 * 使用：new Date().formate(参数)
 * 参数：可选格式1==>yyyy-MM-dd hh:mm:ss 格式2==>yyyy-MM-dd
*/
/*Date.prototype.format = function(fmt) {
    var o = {
        "M+" : this.getMonth()+1,                 //月份
        "d+" : this.getDate(),                    //日
        "h+" : this.getHours(),                   //小时
        "m+" : this.getMinutes(),                 //分
        "s+" : this.getSeconds(),                 //秒
        "q+" : Math.floor((this.getMonth()+3)/3), //季度
        "S"  : this.getMilliseconds()             //毫秒
    };
    if(/(y+)/.test(fmt)) {
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    }
    for(var k in o) {
        if(new RegExp("("+ k +")").test(fmt)){
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        }
    }
    return fmt;
};*/
QueryString=
    {
        data   :{},
        Init:function()
        {
            var   aPairs,   aTmp;
            var   queryString   =   new String(window.location.search);
            queryString   =   queryString.substr(1,   queryString.length);   //remove   "?"
            aPairs   =   queryString.split("&");
            for   (var   i=0   ;   i<aPairs.length;   i++)
            {
                aTmp   =   aPairs[i].split("=");
                this.data[aTmp[0]]   =   aTmp[1];
            }
           // console.log(this.data);
        },
        GetValue:function(key)
        {
            return   this.data[key];
        },
        SetValue:function(   key,   value   )
        {
            if   (value   ==   null)
                delete   this.data[key];
            else
                this.data[key]   =   value;
        },
        ToString:function()
        {
            var   queryString   =   new   String("");

            for   (var   key   in   this.data)
            {
                if   (queryString   !=   "")
                    queryString   +=   "&";
                if   (this.data[key])
                    queryString   +=   key   +   "="   +   this.data[key];
            }
            if   (queryString.length   >   0)
                return   "?"   +   queryString;
            else
                return   queryString;
        },
        Clear:function()
        {
            delete   this.data;
            this.data   =   [];
        }
    };